/**
 *********************************************************************************************************
 *
 * @file    balancing_strategy.c
 * @author  Arne Meeuw
 * @version V0.0.1
 * @date    22-January-2015
 * @brief   Functions for decisions needed for balancing strategies
 * 
 *********************************************************************************************************
 */ 

#include <balancing_strategy.h>

extern nodeInfoType self;                       ///< Cell information
extern packInfoType packInfo;                   ///< Battery pack information
extern Int8U NUMOFNODES;                        ///< Number of nodes in the system

/**
 * @brief Function gets called when cell is initialized and has ID
 * 
 */
void smartCellStart(void)
{  
  
  ///- Set initial balancing settings
  self.balancingSettings.balancingStrategy = BAL_BELOW_AVERAGE;
  self.balancingSettings.monitorEnabled  = false;
  self.balancingSettings.startDiff       = 1.0;
  self.balancingSettings.stopDiff        = 0.5;
  self.balancingSettings.OCVbalancing    = false;
  self.balancingSettings.OCVlatency      = 5000;
  self.balancingSettings.lastTransaction = 0;
  
  ///- Set initial PWM timing settings
  self.pwmSettings.frequency          = 3000;
  self.pwmSettings.numberOfPulses     = 30000;
  self.pwmSettings.duration           = 30000 / 3000 * 1000;
  self.pwmSettings.chargePercent      = 35;
  self.pwmSettings.deadtimePercent    = 5;
  self.pwmSettings.dischargePercent   = 25;
  
  ///- Set initial broadcasting settings
  self.broadcastSettings.allBroadcast              = true;
  self.broadcastSettings.maxBroadcast              = false;
  self.broadcastSettings.minBroadcast              = false;
  self.broadcastSettings.broadcastPeriod           = 0.25;
  self.broadcastSettings.broadcastAfterTransaction = false;
  self.broadcastSettings.limitBroadcast            = false;
  self.broadcastSettings.limitBroadcastBound       = 0.0;
  self.broadcastSettings.broadcastDebugStatus      = true;
  
  ///- Set initial measurement timer settings
  self.timerSettings.uTimerEnabled  = true;
  self.timerSettings.uFrequency     = 600;
  self.timerSettings.uFilter        = 100;
  self.timerSettings.iTimerEnabled  = false;
  self.timerSettings.iFrequency     = 6000;
  self.timerSettings.iFilter        = 1;
  self.timerSettings.kfTimerEnabled = false;
  self.timerSettings.kfFrequency    = 10;
  self.timerSettings.kfFilter       = 1;
  
  ///- Initialize and enable measurement timers
  initUTimer();
  initITimer();
  
  enableUTimer();
  enableITimer();
//  enableKFtimer();
  
}

/**
 * @brief Determination of being in need of requesting charge
 * 
 * @param[out]  bool    Resulting decision [true / false]
 * 
 */
bool requestChargeCondition(void)
{
  OS_ERR err;
  OS_TICK now = OSTimeGet(&err);
  bool result = false;
  
  if ( (self.balancingSettings.balancingStrategy == BAL_MINIMUM) || (self.balancingSettings.balancingStrategy == BAL_MINMAX) )
    result = ( (packInfo.status[self.id] == OFF) && isMin(0.01) );
  else
  if (self.balancingSettings.balancingStrategy == BAL_BELOW_AVERAGE)
    result = ( (packInfo.status[self.id] == OFF) && (packInfo.voltage[self.id] < avgVoltage(0, NUMOFNODES)) );
  
  
  if ( self.balancingSettings.OCVbalancing )
  {
    if ( now  < (self.balancingSettings.lastTransaction + self.pwmSettings.duration + self.balancingSettings.OCVlatency) )
      result = false;
    else
      result = result;
  }
  
  return result;
}

/**
 * @brief Determination of sender for charge request
 * 
 * @param[in/out]  *transactionRequest    Pointer containing information of proposed transaction
 * 
 */
void determineSender(transactionInfoType *transactionRequest)
{
  if (moreVoltageOnLeft())
  {
    transactionRequest->senderID = self.id - 1;
  }
  else
  {
    transactionRequest->senderID = self.id + 1;
  }
}

/**
 * @brief Determination of being in need of sending charge
 * 
 * @param[out]  bool    Resulting decision [true / false]
 * 
 */
bool sendChargeCondition(void)
{
  bool result = false;
  
  if ( (self.balancingSettings.balancingStrategy == BAL_MAXIMUM) || (self.balancingSettings.balancingStrategy == BAL_MINMAX) )
    result = ( (packInfo.status[self.id] == OFF) && isMax(0.01) );
  
  return result;
}

/**
 * @brief Determination of receiver for charge request
 * 
 * @param[in/out]  *transactionRequest    Pointer containing information of proposed transaction
 * 
 */
void determineReceiver(transactionInfoType *transactionRequest)
{
  if (lessVoltageOnLeft())
  {
    transactionRequest->receiverID = self.id - 1;
  }
  else
  {
    transactionRequest->receiverID = self.id + 1;
  }
}

/**
 * @brief Determination of being able to accept a charge request
 * 
 * @param[out]  bool    Resulting decision [true / false]
 * 
 */
bool acknowledgeSendCondition(void)
{
  OS_ERR err;
  OS_TICK now = OSTimeGet(&err);
  bool result = false;
  
  if ( (self.balancingSettings.balancingStrategy == BAL_MINIMUM) || (self.balancingSettings.balancingStrategy == BAL_MAXIMUM) || (self.balancingSettings.balancingStrategy == BAL_MINMAX) )
    result = (packInfo.status[self.id] == OFF);
  else
  if (self.balancingSettings.balancingStrategy == BAL_BELOW_AVERAGE)
    result = ( (packInfo.status[self.id] == OFF) && (packInfo.voltage[self.id] > avgVoltage(0, NUMOFNODES)) );
  
  
  if ( self.balancingSettings.OCVbalancing )
  {
    if ( now  < (self.balancingSettings.lastTransaction + self.pwmSettings.duration + self.balancingSettings.OCVlatency) )
      result = false;
    else
      result = result;
  }  
  
  // BAL_NONE is overriding strategy -> every request will be accepted
  if (self.balancingSettings.balancingStrategy == BAL_NONE)
    result = true;
  
  return result;
}

bool sendBroadcastCondition(void)
{
  OS_ERR err;
  OS_TICK now = OSTimeGet(&err);
  bool result = false;
  bool bound  = (fabs(packInfo.voltage[self.id]-self.voltage) > self.broadcastSettings.limitBroadcastBound);  
  
  // allBroadcast option enabled
  if ( self.broadcastSettings.allBroadcast )
  {
    // limitBroadcast option enabled and cell voltage change is larger than limitBound
    if ( self.broadcastSettings.limitBroadcast )
    {
      if ( bound )
        result = true;
    }
    else
      result = true;
  }
    
  // minBroadcast option enabled and cell is minimum in pack
  if ( self.broadcastSettings.minBroadcast && isMin(0.01) )
  {
    // limitBroadcast option enabled and cell voltage change is larger than limitBound
    if ( self.broadcastSettings.limitBroadcast )
    {
      if ( bound )
        result = true;
    }
    else
      result = true;
  }
  
  // maxBroadcast option enabled and cell is maximum in pack
  if ( self.broadcastSettings.maxBroadcast && isMax(0.01) )
  {
    // limitBroadcast option enabled and cell voltage change is larger than limitBound
    if ( self.broadcastSettings.limitBroadcast )
    {
      if ( bound )
        result = true;
    }
    else
      result = true;
  }
  
  if ( self.balancingSettings.OCVbalancing )
  {
    if ( ( now  <= (self.balancingSettings.lastTransaction + self.pwmSettings.duration + self.balancingSettings.OCVlatency)  ) || packInfo.status[self.id] != OFF )
      result = false;
    else
      result = result;
  }
  
  return result;
}

/**
 * @brief Function is called before cell is sending charge to another cell
 * 
 */
void beforeSend(void)
{
  OS_ERR err;
  
  ///- Set last time of transaction
  OS_TICK now = OSTimeGet(&err);
  self.balancingSettings.lastTransaction = now; 
}

/**
 * @brief Function is called after cell sent charge to another cell
 * 
 */
void afterSend(void)
{  
  OS_ERR err;
  ///- Wait 5 seconds for voltage to gain normal level
  OSTimeDlyHMSM(0, 
                0,
                5,
                0,
                OS_OPT_TIME_HMSM_STRICT, 
                &err);

  ///- Send VoltageBroadcast after balancing transaction
  if ( self.broadcastSettings.broadcastAfterTransaction )
    SendVoltageMessage();
}

/**
 * @brief Function is called before cell is receiving charge from another cell
 * 
 */
void beforeReceive(void)
{
  OS_ERR err;
  
  ///- Set last time of transaction
  OS_TICK now = OSTimeGet(&err);
  self.balancingSettings.lastTransaction = now;
}

/**
 * @brief Function is called after cell received charge from another cell
 * 
 */
void afterReceive(void)
{  
  OS_ERR err;
  ///- Wait 5 seconds for voltage to gain normal level
  OSTimeDlyHMSM(0, 
                0,
                5,
                0,
                OS_OPT_TIME_HMSM_STRICT, 
                &err);
  
  ///- Send VoltageBroadcast after balancing transaction
  if ( self.broadcastSettings.broadcastAfterTransaction )
    SendVoltageMessage();
}

/**
 * @brief Function for enabling active balancing in cell
 * 
 */
void enableActiveBalancing(void)
{
  self.balancingSettings.balancingEnabled = true;
  
  // switch on LED   
#ifndef BOARD_TUM
  GPIOC->BSRRH |= GPIO_Pin_13;
#else
  GPIOB->BSRRH |= GPIO_Pin_7;
#endif
}

/**
 * @brief Function for disabling active balancing in cell
 * 
 */
void disableActiveBalancing(void)
{
  self.balancingSettings.balancingEnabled = false;

  // switch off LED
#ifndef BOARD_TUM
  GPIOC->BSRRL |= GPIO_Pin_13;
#else
  GPIOB->BSRRL |= GPIO_Pin_7;
#endif
}