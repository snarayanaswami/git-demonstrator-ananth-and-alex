#include "BufLCD6610driver.h"
#include "glcd_ll.h"

Int8U ToDrawScreen[26136];
Int8U MirrorScreen[26136]; // 132 * 132 * 3/2 (2 pixels = 3 byte)


// Font bitmaps
// Created by Thomas Jespersen, July 2009
Int16U ColorLCD_font[] = {
  0x00 , 0x00 , 0x00 , 0x00 , 0x00 ,
  0x00 , 0x06 , 0x5F , 0x06 , 0x00 ,                      // !
  0x07 , 0x03 , 0x00 , 0x07 , 0x03 ,                      // ,
  0x24 , 0x7E , 0x24 , 0x7E , 0x24 ,                      // #
  0x24 , 0x2B , 0x6A , 0x12 , 0x00 ,                      // $
  0x63 , 0x13 , 0x08 , 0x64 , 0x63 ,                      // %
  0x36 , 0x49 , 0x56 , 0x20 , 0x50 ,                      // &
  0x00 , 0x07 , 0x03 , 0x00 , 0x00 ,                      // //
  0x00 , 0x3E , 0x41 , 0x00 , 0x00 ,                      // (
  0x00 , 0x41 , 0x3E , 0x00 , 0x00 ,                      // )
  0x08 , 0x3E , 0x1C , 0x3E , 0x08 ,                      // *
  0x08 , 0x08 , 0x3E , 0x08 , 0x08 ,                      // +
  0x00 , 0xE0 , 0x60 , 0x00 , 0x00 ,                      // ,
  0x08 , 0x08 , 0x08 , 0x08 , 0x08 ,                      // -
  0x00 , 0x60 , 0x60 , 0x00 , 0x00 ,                      // .
  0x20 , 0x10 , 0x08 , 0x04 , 0x02 ,                      // /
  0x3E , 0x51 , 0x49 , 0x45 , 0x3E ,                      // 0
  0x00 , 0x42 , 0x7F , 0x40 , 0x00 ,                      // 1
  0x62 , 0x51 , 0x49 , 0x49 , 0x46 ,                      // 2
  0x22 , 0x49 , 0x49 , 0x49 , 0x36 ,                      // 3
  0x18 , 0x14 , 0x12 , 0x7F , 0x10 ,                      // 4
  0x2F , 0x49 , 0x49 , 0x49 , 0x31 ,                      // 5
  0x3C , 0x4A , 0x49 , 0x49 , 0x30 ,                      // 6
  0x01 , 0x71 , 0x09 , 0x05 , 0x03 ,                      // 7
  0x36 , 0x49 , 0x49 , 0x49 , 0x36 ,                      // 8
  0x06 , 0x49 , 0x49 , 0x29 , 0x1E ,                      // 9
  0x00 , 0x6C , 0x6C , 0x00 , 0x00 ,                      // :
  0x00 , 0xEC , 0x6C , 0x00 , 0x00 ,                      // ;
  0x08 , 0x14 , 0x22 , 0x41 , 0x00 ,                      // <
  0x24 , 0x24 , 0x24 , 0x24 , 0x24 ,                      // =
  0x00 , 0x41 , 0x22 , 0x14 , 0x08 ,                      // >
  0x02 , 0x01 , 0x59 , 0x09 , 0x06 ,                      // ?
  0x3E , 0x41 , 0x5D , 0x55 , 0x1E ,                      // @
  0x7E , 0x09 , 0x09 , 0x09 , 0x7E ,                      // A
  0x7F , 0x49 , 0x49 , 0x49 , 0x36 ,                      // B
  0x3E , 0x41 , 0x41 , 0x41 , 0x22 ,                      // C
  0x7F , 0x41 , 0x41 , 0x41 , 0x3E ,                      // D
  0x7F , 0x49 , 0x49 , 0x49 , 0x41 ,                      // E
  0x7F , 0x09 , 0x09 , 0x09 , 0x01 ,                      // F
  0x3E , 0x41 , 0x49 , 0x49 , 0x7A ,                      // G
  0x7F , 0x08 , 0x08 , 0x08 , 0x7F ,                      // H
  0x00 , 0x41 , 0x7F , 0x41 , 0x00 ,                      // I
  0x30 , 0x40 , 0x40 , 0x40 , 0x3F ,                      // J
  0x7F , 0x08 , 0x14 , 0x22 , 0x41 ,                      // K
  0x7F , 0x40 , 0x40 , 0x40 , 0x40 ,                      // L
  0x7F , 0x02 , 0x04 , 0x02 , 0x7F ,                      // M
  0x7F , 0x02 , 0x04 , 0x08 , 0x7F ,                      // N
  0x3E , 0x41 , 0x41 , 0x41 , 0x3E ,                      // O
  0x7F , 0x09 , 0x09 , 0x09 , 0x06 ,                      // P
  0x3E , 0x41 , 0x51 , 0x21 , 0x5E ,                      // Q
  0x7F , 0x09 , 0x09 , 0x19 , 0x66 ,                      // R
  0x26 , 0x49 , 0x49 , 0x49 , 0x32 ,                      // S
  0x01 , 0x01 , 0x7F , 0x01 , 0x01 ,                      // T
  0x3F , 0x40 , 0x40 , 0x40 , 0x3F ,                      // U
  0x1F , 0x20 , 0x40 , 0x20 , 0x1F ,                      // V
  0x3F , 0x40 , 0x3C , 0x40 , 0x3F ,                      // W
  0x63 , 0x14 , 0x08 , 0x14 , 0x63 ,                      // X
  0x07 , 0x08 , 0x70 , 0x08 , 0x07 ,                      // Y
  0x71 , 0x49 , 0x45 , 0x43 , 0x00 ,                      // Z
  0x00 , 0x7F , 0x41 , 0x41 , 0x00 ,                      // [
  0x02 , 0x04 , 0x08 , 0x10 , 0x20 ,                      // Back slash
  0x00 , 0x41 , 0x41 , 0x7F , 0x00 ,                      // ]
  0x04 , 0x02 , 0x01 , 0x02 , 0x04 ,                      // ^
  0x80 , 0x80 , 0x80 , 0x80 , 0x80 ,                      // _
  0x00 , 0x03 , 0x07 , 0x00 , 0x00 ,                      // `
  0x20 , 0x54 , 0x54 , 0x54 , 0x78 ,                      // a
  0x7F , 0x44 , 0x44 , 0x44 , 0x38 ,                      // b
  0x38 , 0x44 , 0x44 , 0x44 , 0x28 ,                      // c
  0x38 , 0x44 , 0x44 , 0x44 , 0x7F ,                      // d
  0x38 , 0x54 , 0x54 , 0x54 , 0x18 ,                      // e
  0x08 , 0x7E , 0x09 , 0x09 , 0x00 ,                      // f
  0x18 , 0xA4 , 0xA4 , 0xA4 , 0x7C ,                      // g
  0x7F , 0x04 , 0x04 , 0x78 , 0x00 ,                      // h
  0x00 , 0x00 , 0x7D , 0x00 , 0x00 ,                      // i
  0x40 , 0x80 , 0x84 , 0x7D , 0x00 ,                      // j
  0x7F , 0x10 , 0x28 , 0x44 , 0x00 ,                      // k
  0x00 , 0x00 , 0x7F , 0x40 , 0x00 ,                      // l
  0x7C , 0x04 , 0x18 , 0x04 , 0x78 ,                      // m
  0x7C , 0x04 , 0x04 , 0x78 , 0x00 ,                      // n
  0x38 , 0x44 , 0x44 , 0x44 , 0x38 ,                      // o
  0xFC , 0x44 , 0x44 , 0x44 , 0x38 ,                      // p
  0x38 , 0x44 , 0x44 , 0x44 , 0xFC ,                      // q
  0x44 , 0x78 , 0x44 , 0x04 , 0x08 ,                      // r
  0x08 , 0x54 , 0x54 , 0x54 , 0x20 ,                      // s
  0x04 , 0x3E , 0x44 , 0x24 , 0x00 ,                      // t
  0x3C , 0x40 , 0x20 , 0x7C , 0x00 ,                      // u
  0x1C , 0x20 , 0x40 , 0x20 , 0x1C ,                      // v
  0x3C , 0x60 , 0x30 , 0x60 , 0x3C ,                      // w
  0x6C , 0x10 , 0x10 , 0x6C , 0x00 ,                      // x
  0x9C , 0xA0 , 0x60 , 0x3C , 0x00 ,                      // y
  0x64 , 0x54 , 0x54 , 0x4C , 0x00 ,                      // z
  0x08 , 0x3E , 0x41 , 0x41 , 0x00 ,                      // {
  0x00 , 0x00 , 0x7F , 0x00 , 0x00 ,                      // |
  0x00 , 0x41 , 0x41 , 0x3E , 0x08 ,                      // }
  0x02 , 0x01 , 0x02 , 0x01 , 0x00                        // ~
};


// Selects an area of the screen
void LCDsetBox(Int16U x, Int16U y, Int16U width, Int16U height)
{  
  Int32U k; 
  // Select the columns
  k = x | ((x+width-1) <<8);
  GLCD_SendCmd(CASET,(pInt8U)&k,0);
  // Select the rows
  k = y | ((y+height-1)<<8);
  GLCD_SendCmd(RASET,(pInt8U)&k,0);
}


// Checks whether a line has been changes using the efficient memcmp function
// 2013-10-17 Sebastian Steinhorst
byte BufLCDlineChanged(Int16U line, byte (* ToDrawScreen)[], byte (*MirrorScreen)[])
{
  byte changed=0;
  changed=memcmp(ToDrawScreen, MirrorScreen, 198);
  return changed;
}


//  Draw a line on ToDrawScreen to the LCD, first taking a copy to MirrorScreen, drawing from there
// 2013-10-17 Sebastian Steinhorst
void BufLCDDrawScreenLine(Int16U line, byte (* ToDrawScreen)[], byte (*MirrorScreen)[])
{
  // Select a screen area to draw to
  LCDsetBox(0, line, 132, 1);
  
  // Prepare writing to memory
  Int16U offset = line*198;
  
  // make a copy of the line to MirrorScreen and draw from there, so changes
  // to ToDrawScreen will not be lost
  memcpy(&(*MirrorScreen)[offset],&(*ToDrawScreen)[offset],198);
  
  // write line to LCD
  GLCD_SPI_ChipSelect(1);
  GLCD_SendCmd(RAMWR,0,0);  
  GLCD_SPI_SendBlock((pInt8U)&(*MirrorScreen)[offset],198);
  GLCD_SPI_ChipSelect(0);
  
  return;
}


// Draws LCD Screen from ToDrawScreen array, checking for changes in MirrorScreen
// 2013-10-17 Sebastian Steinhorst
void BufLCDUpdateScreen(byte (* ToDrawScreen)[], byte (* MirrorScreen)[])
{
  for (Int16U line=0;line<132;line++)
  {
    if (BufLCDlineChanged(line, ToDrawScreen, MirrorScreen) >0);
    {   
      BufLCDDrawScreenLine(line, ToDrawScreen, MirrorScreen);
    }
  }
  return;
}


// Fill an area of ToDrawScreen with a color
// 2013-10-16 Sebastian Steinhorst
void BufLCDcolorFill(byte (* ToDrawScreen)[], Int16U x, Int16U y, Int16U width, Int16U height, Int16U color)
{
  if( x > 132 || y > 132 ) 
    return; 
  if( ( x + width  ) > 132 ) width  = 132 - x;
  if( ( y + height ) > 132 ) height = 132 - y;
  
  // Write Pixels for area  
  for (Int16U row=x; row < x+width; row++)
    for (Int16U column=y; column < y+height; column++)
      BufLCDsetPixel(ToDrawScreen, row, column, color);
}


// Fill ToDrawScreen with 12 bit 132 x 132 bitmap in (0x03) enconding (3 bytes are 2 pixels (RRGG BBRR GGBB))
// images can be generated using the Code Image Generator from http://www.random-i.com/NokiaLCD/
// 2013-10-16 Sebastian Steinhorst
void BufLCDshowBMP12bit(byte (* ToDrawScreen)[], byte (* bmp)[])
{
  // Compute the dimensions
  Int16U numberOfBytes = (132 * 132 *3)/2; // 3 bytes are 2 pixels (RRGG BBRR GGBB)
  
  // Copy from bitmap to ToDrawScreen
  memcpy(&(*ToDrawScreen)[0],&(*bmp)[5],numberOfBytes);
  return;
}


// Sets a pixel to a defined color on ToDrawScreen
// 2013-10-16 Sebastian Steinhorst
void BufLCDsetPixel(byte (* ToDrawScreen)[], Int16U x, Int16U y, Int16U color)
{
  if( x > 132 || y > 132 ) 
  return; 
  
  Int16U pixelOffset = x + (y * 132);
  Int16U screenOffset = (pixelOffset*3)/2;
    
  if (pixelOffset % 2 == 0) //take care of the 12bit pixel alignment
  {
    (* ToDrawScreen)[screenOffset]=color >> 4;
    (* ToDrawScreen)[screenOffset+1]=(( color << 4 ) & 0xf0) | ((* ToDrawScreen)[screenOffset+1]&0xf);
  }
  else
  {
    (* ToDrawScreen)[screenOffset]=((* ToDrawScreen)[screenOffset]&0xf0) | ( (color >> 8) & 0xf);
    (* ToDrawScreen)[screenOffset+1]=(color & 0xff);
  }  
}


// Show a sprite on the screen at screenX, screenY, using the part of bmp starting at sourceX, sourceY with width, height
// The actual dimensions of the source file can be other than 132x132, as long this is encoded in the first 5 bytes
// The bitmap is in (0x03) enconding (3 bytes are 2 pixels (RRGG BBRR GGBB))
// images can be generated using the Code Image Generator from http://www.random-i.com/NokiaLCD/
// 2013-10-17 Sebastian Steinhorst
void BufLCDshowSpriteBMP12bit(byte (* ToDrawScreen)[], Int16U screenX, Int16U screenY, Int16U sourceX, Int16U sourceY, Int16U width, Int16U height, byte (* bmp)[])
{
  Int16U sourceWidth = (*bmp)[0]+1;
  Int16S correctionOffset1, correctionOffset2;
 
  // Write the pixels, considering the 12bit pixel layout of the LCD
  if (((screenX % 2) + (sourceX % 2))%2 == 0) // If both sourceX and screenX are either even or uneven, we have no pixel alignment problem.
                                              // We can then use the memcpy function, which is very efficient
  {
    for (Int16U yOffset=0;yOffset<height;yOffset++)
      memcpy(&(*ToDrawScreen)[(((((screenY+yOffset)*132)+screenX)*3)/2)],&(*bmp)[5+(((((sourceY+yOffset)*sourceWidth)+sourceX)*3)/2)],(width*3)/2);
        
  }
  else // we have to fix the pixel alignment
  {
    if (((screenX % 2)==0) && ((sourceX % 2) == 1))
    {
      correctionOffset1=0; //prepare correction offsets
      correctionOffset2=1;
    }
    else
    {
      correctionOffset1=-1;
      correctionOffset2=0;
    }  
    
    for (Int16U yOffset=0;yOffset<height;yOffset++)
    {
      Int16U screenPos=(((((screenY+yOffset)*132)+screenX)*3)/2);
      Int16U sourcePos=5+(((((sourceY+yOffset)*sourceWidth)+sourceX)*3)/2);
      byte firstScreenByte = (* ToDrawScreen)[screenPos];
      byte firstSourceByte = (* bmp)[sourcePos];
      (* ToDrawScreen)[screenPos]=(firstScreenByte&0xf0) + ((firstSourceByte>>4)&0xf); //fix and draw first pixel in line
      for (Int16U xOffset=1;xOffset<((width*3)/2);xOffset++) //draw the rest
      {
        byte sourceByte1=(* bmp)[5+(((((sourceY+yOffset)*sourceWidth)+sourceX)*3)/2)+xOffset+correctionOffset1];
        byte sourceByte2=(* bmp)[5+(((((sourceY+yOffset)*sourceWidth)+sourceX)*3)/2)+xOffset+correctionOffset2];
        (* ToDrawScreen)[(((((screenY+yOffset)*132)+screenX)*3)/2)+xOffset]=((sourceByte1<<4)&0xf0)+((sourceByte2>>4)&0xf);
      }
    }
  }
  return;
}


// Function used to print a char with a different color from the foreground color
void BufLCDprintString(byte (* ToDrawScreen)[], char *string, Int16U x, Int16U y, Int16U color)
{
  Int16U bgColor=0x000;
  while ( *string )
  {
      BufLCDprintFontChar(ToDrawScreen, *string++, x, y, 5, 8, color, bgColor, 1 );
      x +=( 6 );
  }
}



// Function used to print a char with a different color from the foreground color
void BufLCDprintStringBG(byte (* ToDrawScreen)[], char *string, Int16U x, Int16U y, Int16U fgColor, Int16U bgColor)
{
  while ( *string )
  {
      BufLCDprintFontChar(ToDrawScreen, *string++, x, y, 5, 8, fgColor, bgColor, 1 );
      x +=( 6 );
  }
}

// Function used to print a char with a different color from the foreground color
void BufLCDprintStringBGZoom(byte (* ToDrawScreen)[], char *string, Int16U x, Int16U y, Int16U fgColor, Int16U bgColor)
{
  while ( *string )
  {
      BufLCDprintFontChar(ToDrawScreen, *string++, x, y, 5, 8, fgColor, bgColor, 2 );
      x +=( 13 );
  }
}

// Prints a char at position (x, y) with dimensions width and height and color code color
void BufLCDprintFontChar(byte (* ToDrawScreen)[], char c, Int16U x, Int16U y, Int16U width, Int16U height, Int16U color, Int16U bgColor, Int16U zoom )
{
  Int16U column, column2, row, row0, bF, bb;
  Int16U* pFont = ColorLCD_font + (c - 32) * width;
  
  for ( column = 0; column < width; column++ )
  {
    bF = pFont[ column ];
    bb = bF & 1;
    row0 = 0;
    column2 = x + column * zoom;
    
    for ( row = 1; row < height; row++ )
    {
      bF >>= 1;
      if( ( bF & 1 ) != bb )
      {
        BufLCDcolorFill(ToDrawScreen, column2, y + row0 * zoom, zoom, ( row - row0 ) * zoom, bb ? color : bgColor );
        row0 = row;
        bb = bF & 1;
      }
    }
    BufLCDcolorFill(ToDrawScreen, column2, y + row0 * zoom, zoom, ( row - row0 ) * zoom, bb ? color : bgColor );
  }
  
  column2 = x + width * zoom;
  BufLCDcolorFill(ToDrawScreen, column2, y, zoom, height * zoom, bgColor );
}


void LCDSetBrightness(Int16U brightness)
{
  GLCD_Backlight( brightness );
}


void LCDSetContrast( Int16U Contrast)
{
  #if defined(BOARD_E407) || defined(BOARD_IAR) || defined(BOARD_TUM)
    Contrast |= (0x0UL << 8);
    GLCD_SendCmd(VOLCTR,(pInt8U)&Contrast,0);
  #endif 
  #ifdef BOARD_P407  
    GLCD_SendCmd(SETCON,(pInt8U)&Contrast,0);
  #endif
}


void BufLCDInit()
{
   GLCD_PowerUpInit();
   GLCD_Backlight(BACKLIGHT_ON);  
}