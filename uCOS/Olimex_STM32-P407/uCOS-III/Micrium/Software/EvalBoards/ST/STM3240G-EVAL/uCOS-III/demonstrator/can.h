#ifndef can_h
#define can_h


#include <includes.h>

void CAN1_RX0_IRQHandler(void);
void CAN1_Init(Int16U kbps);
void CAN1_Set_Filter(Int8U ownID);
void sendCANMessage(Int16U type, idType target, idType origin, Int8U DLC, Int8U data[]);
#endif