#ifndef BufLCD6610driver_h
#define BufLCD6610driver_h
#include <arm_comm.h>
#include <string.h>
#include <drv_glcd.h>

typedef unsigned char byte; // Unsigned  8 bit quantity

extern Int8U ToDrawScreen[];
extern Int8U MirrorScreen[];

// Frontend functions
void BufLCDprintString(byte (* ToDrawScreen)[], char *string, Int16U x, Int16U y, Int16U color);
void BufLCDprintStringBG(byte (* ToDrawScreen)[], char *string, Int16U x, Int16U y, Int16U fgColor, Int16U bgColor);
void BufLCDprintStringBGZoom(byte (* ToDrawScreen)[], char *string, Int16U x, Int16U y, Int16U fgColor, Int16U bgColor);
void BufLCDshowBMP12bit(byte (* ToDrawScreen)[], byte (* bmp)[]);
void BufLCDshowSpriteBMP12bit( byte (* ToDrawScreen)[], Int16U screenX, Int16U screenY, Int16U sourceX, Int16U sourceY, Int16U width, Int16U height, byte (* bmp)[]);
void BufLCDcolorFill(byte (* ToDrawScreen)[], Int16U x, Int16U y, Int16U width, Int16U height, Int16U color);
void BufLCDsetPixel(byte (* ToDrawScreen)[], Int16U x, Int16U y, Int16U color);

// Redraw management
void BufLCDUpdateScreen(byte (* ToDrawScreen)[], byte (* MirrorScreen)[]);
void BufLCDDrawScreenLine(Int16U line, byte (* ToDrawScreen)[], byte (*MirrorScreen)[]);
byte BufLCDlineChanged(Int16U line, byte (* ToDrawScreen)[], byte (*MirrorScreen)[]);

// Frontend helper for text output
void BufLCDprintFontChar(byte (* ToDrawScreen)[], char c, Int16U x, Int16U y, Int16U width, Int16U height, Int16U color, Int16U bgColor, Int16U zoom );

// Backend functions
void BufLCDInit();
void LCDSetContrast(Int16U Contrast);
void LCDSetBrightness(Int16U Contrast);
void LCDsetBox(Int16U x, Int16U y, Int16U width, Int16U height);

#endif
