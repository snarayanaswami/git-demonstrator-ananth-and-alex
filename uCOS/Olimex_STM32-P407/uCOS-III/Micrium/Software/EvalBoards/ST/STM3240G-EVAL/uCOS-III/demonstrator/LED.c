/**
 *********************************************************************************************************
 *
 * @file    LED.c
 * @author  Arne Meeuw
 * @version V0.0.1
 * @date    2-January-2015
 * @brief   Initialization and setting of GPIO pins used for LEDs
 *  
 *********************************************************************************************************
 */ 

#include <LED.h>

void BoardLEDInit(void)
{
  // initialize GPIO pins for all four LEDs  
  
#ifdef LED_RED
  // RED
  GPIO_InitTypeDef GPIO_InitStruct;
  RCC_AHB1PeriphClockCmd(LED_RED_Clock, ENABLE);
  GPIO_InitStruct.GPIO_Pin = LED_RED_Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(LED_RED_Port, &GPIO_InitStruct);    
#endif
  
#ifdef LED_GREEN
  // GREEN
  GPIO_InitTypeDef GPIO_InitStruct1;
  RCC_AHB1PeriphClockCmd(LED_GREEN_Clock, ENABLE);
  GPIO_InitStruct1.GPIO_Pin = LED_GREEN_Pin;
  GPIO_InitStruct1.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct1.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct1.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct1.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(LED_GREEN_Port, &GPIO_InitStruct1);    
#endif
  
#ifdef LED_YELLOW
  // YELLOW
  GPIO_InitTypeDef GPIO_InitStruct2;
  RCC_AHB1PeriphClockCmd(LED_YELLOW_Clock, ENABLE);
  GPIO_InitStruct2.GPIO_Pin = LED_YELLOW_Pin;
  GPIO_InitStruct2.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct2.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct2.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct2.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(LED_YELLOW_Port, &GPIO_InitStruct2);    
#endif
  
#ifdef LED_ORANGE
  // ORANGE
  GPIO_InitTypeDef GPIO_InitStruct3;
  RCC_AHB1PeriphClockCmd(LED_ORANGE_Clock, ENABLE);
  GPIO_InitStruct3.GPIO_Pin = LED_ORANGE_Pin;
  GPIO_InitStruct3.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct3.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct3.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct3.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(LED_ORANGE_Port, &GPIO_InitStruct3);    
#endif
}

void BoardLED(Int8U ledColor, bool isOn)
{
  switch(ledColor)
  {
  case RED:
    if (isOn)
    {
      // Switch LED on
      LED_RED_Port->BSRRL |= LED_RED_Pin; 
    }
    else
    {
      // Switch LED off
      LED_RED_Port->BSRRH |= LED_RED_Pin; 
    }  
    break;
  case GREEN:
    if (isOn)
    {
#ifndef BOARD_TUM
      // Switch LED on
      LED_GREEN_Port->BSRRL |= LED_GREEN_Pin; 
#else
      LED_GREEN_Port->BSRRH |= LED_GREEN_Pin; 
#endif
    }
    else
    {
#ifndef BOARD_TUM
      // Switch LED off
      LED_GREEN_Port->BSRRH |= LED_GREEN_Pin; 
#else
      LED_GREEN_Port->BSRRL |= LED_GREEN_Pin; 
#endif
    }  
    break;
  case YELLOW:
    if (isOn)
    {
      // Switch LED on
      LED_YELLOW_Port->BSRRL |= LED_YELLOW_Pin; 
    }
    else
    {
      // Switch LED off
      LED_YELLOW_Port->BSRRH |= LED_YELLOW_Pin; 
    }  
    break;
  case ORANGE:
    if (isOn)
    {
      // Switch LED on
      LED_ORANGE_Port->BSRRL |= LED_ORANGE_Pin; 
    }
    else
    {
      // Switch LED off
      LED_ORANGE_Port->BSRRH |= LED_ORANGE_Pin; 
    }  
    break;
  }
}