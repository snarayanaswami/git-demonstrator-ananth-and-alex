#include <dogm162.h>

void Delay(uint32_t nTime)
{
  while(nTime--);
}

void dogm_set_cs()
{
	 GPIOE->BSRRH  = GPIO_Pin_12;
}

void dogm_unset_cs()
{
	GPIOE->BSRRL  = GPIO_Pin_12;
}

void dogm_data_mode()
{
	GPIOE->BSRRL  = GPIO_Pin_2;
}

void dogm_command_mode()
{
	GPIOE->BSRRH  = GPIO_Pin_2;
}

void SPI_send(Int8U data)
{
uint8_t i = 8;

do
{
    Delay(10000);
    if (data & 0x80)
        //GPIOD->BSRRL = GPIO_Pin_7;  //If data_bit is 1 then MOSI is 1 for olimex
        GPIOD->BSRRL = GPIO_Pin_15;  //If data_bit is 1 then MOSI is 1 for TUM
    else
        //GPIOD->BSRRH = GPIO_Pin_7; //If data_bit is 0 then MOSI is 0 for olimex
        GPIOD->BSRRH = GPIO_Pin_15; //If data_bit is 0 then MOSI is 0 for TUM

    //GPIOD->BSRRH = GPIO_Pin_6;      // Pull Clock Low for olimex
    GPIOD->BSRRH = GPIO_Pin_8;      // Pull Clock Low for TUM
    Delay(10000);
    data <<=1;  // Shift data one bit
    //GPIOD->BSRRL = GPIO_Pin_6;      // Pull Clock High for olimex
    GPIOD->BSRRL = GPIO_Pin_8;      // Pull Clock High for TUM
    Delay(10000);
}while(--i);

}

void command(uint8_t dat)
{
  dogm_command_mode();
  Delay(2000); // 20us Delay
  SPI_send(dat); // Send the command byte
  Delay(2000); // 20us Delay
}


void Clear_display(void)
{
  dogm_set_cs();
  command(0x01); //clear display and return home
  dogm_unset_cs();
}

void Displ_String(char *str)
{
  int i=0;
  dogm_set_cs();
  dogm_data_mode();

  while (str[i] != 0)
  {
          SPI_send(str[i]);
          i++;
  }

  dogm_unset_cs();
}

void Displ_Char(Int8U data)
{
  dogm_set_cs();
  dogm_data_mode();
  SPI_send(data);
  dogm_unset_cs();
}

void Position(uint8_t column, uint8_t line)
{

dogm_set_cs();
	uint8_t cmd = 0;
    uint8_t Placement = 0;

	if(column == 0) column = 1; //minimum column 1
	if(column > 16) column = 16;//maximum column 16

	if(line == 2) //second line address
        {
        cmd = 0x40;
        }
    Placement = 0x80 + cmd + (column-1);
	command(Placement);
	//command(0x80 + cmd + column-1); //DOG display starts with column 0 --> decrement
dogm_unset_cs();
}

void LCD_INITSPI_EADOGM162(void)
{

dogm_set_cs();
command(0x39); //Function set: 0x39 is for 2 lines, Instruction table 1
Delay(150);  //30 us Delay

command(0x14); //Bias set: 1/5, 2 line LCD
Delay(150);  //30 us Delay

command(0x55); //Power: booster On, contrast: clear C5, set C4
Delay(150);  //30 us Delay

command(0x6D); //Follower control
Delay(150);  //30 us Delay

command(0x78);	// set contrast
Delay(150);  //30 us Delay


command(0x38); //Function set: 8 bit Instruction table 0
Delay(150);  //30 us Delay

command(0x0C); //display ON, cursor ON, cursor blink
Delay(150);  //30 us Delay

command(0x01); //clear display cursor Home
Delay(20000); // Delay of 4 ms

command(0x06); // cursor auto increment
Delay(150);  //30 us Delay

dogm_unset_cs();
Delay(500000); // Delay of 30ms

}

void initialize_IO(void)
{

RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG, ENABLE);

//RCC_APB1PeriphClockCmd(RCC_APB1Periph_GPIOD, ENABLE);
//RCC_APB1PeriphClockCmd(RCC_APB1Periph_GPIOE, ENABLE);
GPIO_InitTypeDef GPIO_InitStructure;


	/* Configure the chip select pin
	   in this case we will use PE12 for TUM board */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOE, &GPIO_InitStructure);

	GPIOE->BSRRL = GPIO_Pin_12; // set PE7 high

// RS PIN for differentiating command and data for olimex board

//  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
//  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
//  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
//  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
//  GPIO_Init(GPIOD, &GPIO_InitStructure);



// RS PIN for differentiating command and data for TUM board
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOE, &GPIO_InitStructure);

// PSB PIN for differentiating SPI and 8-bit mode for TUM board
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOE, &GPIO_InitStructure);
  GPIOE->BSRRH = GPIO_Pin_0; // Low for SPI mode

// RESET PIN for TUM board
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOE, &GPIO_InitStructure);
  GPIOE->BSRRL = GPIO_Pin_1; // High to be ON

// //Clock Pin PD6 for Olimex board
//  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
//  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
//  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
//  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
//  GPIO_Init(GPIOD, &GPIO_InitStructure);
//  GPIOD->BSRRL = GPIO_Pin_6; // Initial Clock state High

// Clock Pin PD8 for TUM board
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOD, &GPIO_InitStructure);
  GPIOD->BSRRL = GPIO_Pin_8; // Initial Clock state High

//// MOSI Pin PD7 for olimex board
//  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
//  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
//  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
//  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
//  GPIO_Init(GPIOD, &GPIO_InitStructure);
//  GPIOD->BSRRH = GPIO_Pin_7; // Initial data pin pulled low

// MOSI Pin PD15 for TUM board
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOD, &GPIO_InitStructure);
  GPIOD->BSRRH = GPIO_Pin_15; // Initial data pin pulled low

/*Data Pins D0---5 for TUM board to be connected to 3.3 V*/
//D0
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOD, &GPIO_InitStructure);
  GPIOD->BSRRL = GPIO_Pin_2; // Pulled High to connect to 3.3V
//D1
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOD, &GPIO_InitStructure);
  GPIOD->BSRRL = GPIO_Pin_3; // Pulled High to connect to 3.3V
//D2
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOD, &GPIO_InitStructure);
  GPIOD->BSRRL = GPIO_Pin_4; // Pulled High to connect to 3.3V
//D3
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOD, &GPIO_InitStructure);
  GPIOD->BSRRL = GPIO_Pin_5; // Pulled High to connect to 3.3V
//D4
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOD, &GPIO_InitStructure);
  GPIOD->BSRRL = GPIO_Pin_6; // Pulled High to connect to 3.3V
//D5
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOD, &GPIO_InitStructure);
  GPIOD->BSRRL = GPIO_Pin_7; // Pulled High to connect to 3.3V
// Backlight PG9
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOG, &GPIO_InitStructure);
  GPIOG->BSRRL = GPIO_Pin_9; // Pulled High to ON backlight

}

