/*************************************************************************
 *
 *    Used with ICCARM and AARM.
 *
 *    (c) Copyright IAR Systems 2006
 *
 *    File name   : glcd_ll.h
 *    Description : GLCD low level include file
 *
 *    History :
 *    1. Date        : December 2, 2006
 *       Author      : Stanimir Bonev
 *       Description : Create
 *
 *    $Revision: 60339 $
 *
 *    Modified by Sebastian Steinhorst 2013-11-04 to work with BufLCD6610driver
 *   
 **************************************************************************/   
#ifndef __GLCD_LL_H
#define __GLCD_LL_H

#define BACKLIGHT_OFF     0x00
#define BACKLIGHT_ON      0x80
#define SSP_FIFO_SIZE     8

#define GLCD_SPI_CLK      1000    // [kHz]

//BOARD_P407
#ifdef  BOARD_P407
// LCD controller reset pin - PD3
#define LCD_RST_PORT        GPIOD
#define LCD_RST_MASK        GPIO_Pin_3
#define LCD_RST_CLK         RCC_AHB1Periph_GPIOD

// LCD controller CS pin - PD6
#define LCD_CS_PORT         GPIOD
#define LCD_CS_MASK         GPIO_Pin_6
#define LCD_CS_CLK          RCC_AHB1Periph_GPIOD

// LCD controller BACK LIGHT pin - PB0
#define LCD_BL_PORT         GPIOB
#define LCD_BL_MASK         GPIO_Pin_0
#define LCD_BL_CLK          RCC_AHB1Periph_GPIOB
#define LCD_BL_PIN_SOURCE   GPIO_PinSource0
#define LCD_BL_PIN_AF       GPIO_AF_TIM3

// LCD controller SPI pins - PA5, PC3, PB4
#define LCD_SPI_SCLK_PORT   GPIOA
#define LCD_SPI_SCLK_MASK   GPIO_Pin_5
#define LCD_SPI_SCLK_CLK    RCC_AHB1Periph_GPIOA

/* multiplexed with JTAG do not use */
#define LCD_SPI_MISO_PORT   GPIOB
#define LCD_SPI_MISO_MASK   GPIO_Pin_4
#define LCD_SPI_MISO_CLK    RCC_AHB1Periph_GPIOB


#define LCD_SPI_MOSI_PORT   GPIOC
#define LCD_SPI_MOSI_MASK   GPIO_Pin_3
#define LCD_SPI_MOSI_CLK    RCC_AHB1Periph_GPIOC

#endif

//BOARD_E407
#ifdef  BOARD_E407

// LCD controller reset pin - PC6
#define LCD_RST_PORT        GPIOC
#define LCD_RST_MASK        GPIO_Pin_6
#define LCD_RST_CLK         RCC_AHB1Periph_GPIOC

// LCD controller CS pin - PG10
#define LCD_CS_PORT         GPIOG
#define LCD_CS_MASK         GPIO_Pin_10
#define LCD_CS_CLK          RCC_AHB1Periph_GPIOG

// LCD controller BACK LIGHT pin - PC7
#define LCD_BL_PORT         GPIOC
#define LCD_BL_MASK         GPIO_Pin_7
#define LCD_BL_CLK          RCC_AHB1Periph_GPIOC
#define LCD_BL_PIN_SOURCE   GPIO_PinSource7
#define LCD_BL_PIN_AF       GPIO_AF_TIM3

// LCD controller SPI pins - PA5, PC3, PB4
#define LCD_SPI_SCLK_PORT   GPIOB
#define LCD_SPI_SCLK_MASK   GPIO_Pin_10
#define LCD_SPI_SCLK_CLK    RCC_AHB1Periph_GPIOB

#define LCD_SPI_MOSI_PORT   GPIOC
#define LCD_SPI_MOSI_MASK   GPIO_Pin_3
#define LCD_SPI_MOSI_CLK    RCC_AHB1Periph_GPIOC

#define LCD_SPI_MISO_PORT   GPIOC
#define LCD_SPI_MISO_MASK   GPIO_Pin_2
#define LCD_SPI_MISO_CLK    RCC_AHB1Periph_GPIOC
#endif


// NEW CONTROL BOARD
#ifdef  BOARD_TUM
#ifdef  UEXT2
// LCD controller reset pin - PC2
#define LCD_RST_PORT        GPIOC
#define LCD_RST_MASK        GPIO_Pin_2
#define LCD_RST_CLK         RCC_AHB1Periph_GPIOC

// LCD controller CS pin - PB12
#define LCD_CS_PORT         GPIOB
#define LCD_CS_MASK         GPIO_Pin_12
#define LCD_CS_CLK          RCC_AHB1Periph_GPIOB

// LCD controller BACK LIGHT pin - PC7
#define LCD_BL_PORT         GPIOC
#define LCD_BL_MASK         GPIO_Pin_7
#define LCD_BL_CLK          RCC_AHB1Periph_GPIOC
#define LCD_BL_PIN_SOURCE   GPIO_PinSource7
#define LCD_BL_PIN_AF       GPIO_AF_TIM3

// LCD controller SPI pins - PB13, PB15, PB14
#define LCD_SPI_SCLK_PORT   GPIOB
#define LCD_SPI_SCLK_MASK   GPIO_Pin_13
#define LCD_SPI_SCLK_CLK    RCC_AHB1Periph_GPIOB

#define LCD_SPI_MOSI_PORT   GPIOB
#define LCD_SPI_MOSI_MASK   GPIO_Pin_15
#define LCD_SPI_MOSI_CLK    RCC_AHB1Periph_GPIOB

#define LCD_SPI_MISO_PORT   GPIOB
#define LCD_SPI_MISO_MASK   GPIO_Pin_14
#define LCD_SPI_MISO_CLK    RCC_AHB1Periph_GPIOB

#else
// LCD controller reset pin - PA2
#define LCD_RST_PORT        GPIOA
#define LCD_RST_MASK        GPIO_Pin_2
#define LCD_RST_CLK         RCC_AHB1Periph_GPIOA

// LCD controller CS pin - PC9
#define LCD_CS_PORT         GPIOC
#define LCD_CS_MASK         GPIO_Pin_9
#define LCD_CS_CLK          RCC_AHB1Periph_GPIOC

// LCD controller BACK LIGHT pin - PA3
#define LCD_BL_PORT         GPIOA
#define LCD_BL_MASK         GPIO_Pin_3
#define LCD_BL_CLK          RCC_AHB1Periph_GPIOA
#define LCD_BL_PIN_SOURCE   GPIO_PinSource3
#define LCD_BL_PIN_AF       GPIO_AF_TIM3

// LCD controller SPI pins - PD8, PD15, PC11
#define LCD_SPI_SCLK_PORT   GPIOD
#define LCD_SPI_SCLK_MASK   GPIO_Pin_8
#define LCD_SPI_SCLK_CLK    RCC_AHB1Periph_GPIOC

#define LCD_SPI_MOSI_PORT   GPIOD
#define LCD_SPI_MOSI_MASK   GPIO_Pin_15
#define LCD_SPI_MOSI_CLK    RCC_AHB1Periph_GPIOC

#define LCD_SPI_MISO_PORT   GPIOC
#define LCD_SPI_MISO_MASK   GPIO_Pin_11
#define LCD_SPI_MISO_CLK    RCC_AHB1Periph_GPIOC
#endif
#endif


//BOARD_IAR
#ifdef BOARD_IAR

// LCD controller reset pin - PE5
#define LCD_RST_PORT        GPIOE
#define LCD_RST_MASK        GPIO_Pin_5
#define LCD_RST_CLK         RCC_AHB1Periph_GPIOE  

// LCD controller CS pin - PB1
#define LCD_CS_PORT         GPIOB
#define LCD_CS_MASK         GPIO_Pin_1
#define LCD_CS_CLK          RCC_AHB1Periph_GPIOB

// LCD controller BACK LIGHT pin 
#define LCD_BL_PORT         GPIOB
#define LCD_BL_MASK         GPIO_Pin_0
#define LCD_BL_CLK          RCC_AHB1Periph_GPIOB
#define LCD_BL_PIN_SOURCE   GPIO_PinSource0
#define LCD_BL_PIN_AF       GPIO_AF_TIM3

// LCD controller SPI pins - PC10, PA6, PA7
#define LCD_SPI_SCLK_PORT   GPIOA
#define LCD_SPI_SCLK_MASK   GPIO_Pin_5
#define LCD_SPI_SCLK_CLK    RCC_AHB1Periph_GPIOA
#define LCD_SPI_MISO_PORT   GPIOA
#define LCD_SPI_MISO_MASK   GPIO_Pin_6
#define LCD_SPI_MISO_CLK    RCC_AHB1Periph_GPIOA

#define LCD_SPI_MOSI_PORT   GPIOA
#define LCD_SPI_MOSI_MASK   GPIO_Pin_4
#define LCD_SPI_MOSI_CLK    RCC_AHB1Periph_GPIOA
#endif

#define GLCD_SPI_MOSI_H() LCD_SPI_MOSI_PORT->BSRRL = LCD_SPI_MOSI_MASK
#define GLCD_SPI_MOSI_L() LCD_SPI_MOSI_PORT->BSRRH  = LCD_SPI_MOSI_MASK
#define GLCD_SPI_CLK_H()  LCD_SPI_SCLK_PORT->BSRRL = LCD_SPI_SCLK_MASK
#define GLCD_SPI_CLK_L()  LCD_SPI_SCLK_PORT->BSRRH  = LCD_SPI_SCLK_MASK
#define GLCD_SPI_MISO()   (0 != (LCD_SPI_MISO_PORT->IDR & LCD_SPI_MISO_MASK))

/*************************************************************************
 * Function Name: GLCD_SetReset
 * Parameters: Boolean State
 * Return: none
 *
 * Description: Set reset pin state
 *
 *************************************************************************/
void GLCD_SetReset (Boolean State);

/*************************************************************************
 * Function Name: GLCD_SetBacklight
 * Parameters: Int8U Light
 * Return: none
 *
 * Description: Set backlight pin state
 *
 *************************************************************************/
void GLCD_Backlight (Int8U Light);

/*************************************************************************
 * Function Name: GLCD_LLInit
 * Parameters: none
 * Return: none
 *
 * Description: Init Reset and Backlight control outputs
 *
 *************************************************************************/
void GLCD_LLInit (void);

/*************************************************************************
 * Function Name: LcdSpiChipSelect
 * Parameters: Boolean Select
 * Return: none
 *
 * Description: SPI Chip select control
 * Select = true  - Chip is enable
 * Select = false - Chip is disable
 *
 *************************************************************************/
void GLCD_SPI_ChipSelect (Boolean Select);

/*************************************************************************
 * Function Name: LcdSpiSetWordWidth
 * Parameters: Int32U Width
 * Return: Boolean
 *
 * Description: Set SPI word width
 *
 *************************************************************************/
Boolean GLCD_SPI_SetWordWidth (Int32U Width);

/*************************************************************************
 * Function Name: LcdSpiSetClockFreq
 * Parameters: Int32U Frequency
 * Return: Int32U
 *
 * Description: Set SPI clock
 *
 *************************************************************************/
Int32U GLCD_SPI_SetClockFreq (Int32U Frequency);

/*************************************************************************
 * Function Name: GLCD_SPI_Init
 * Parameters: Int32U Clk, Int32U Width
 * Return: none
 *
 * Description: Init SPI
 *
 *************************************************************************/
void GLCD_SPI_Init(Int32U Clk, Int32U Width);

/*************************************************************************
 * Function Name: GLCD_SPI_TranserByte
 * Parameters: Int32U Data
 * Return: Int32U
 *
 * Description: Transfer byte from SPI
 *
 *************************************************************************/
Int32U GLCD_SPI_TranserByte (Int32U Data);

/*************************************************************************
 * Function Name: GLCD_SPI_SendBlock
 * Parameters: pInt8U pData, Int32U Size
 *
 * Return: void
 *
 * Description: Write block of data to SPI
 *
 *************************************************************************/
void GLCD_SPI_SendBlock (pInt8U pData, Int32U Size);

/*************************************************************************
 * Function Name: GLCD_SPI_ReceiveBlock
 * Parameters: pInt8U pData, Int32U Size
 *
 * Return: void
 *
 * Description: Read block of data from SPI
 *
 *************************************************************************/
void GLCD_SPI_ReceiveBlock (pInt8U pData, Int32U Size);

#endif // __GLCD_LL_H
