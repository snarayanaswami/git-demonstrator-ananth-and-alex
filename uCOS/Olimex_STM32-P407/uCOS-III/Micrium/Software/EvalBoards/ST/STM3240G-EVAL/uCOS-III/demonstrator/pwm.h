#ifndef pwm_h
#define pwm_h

#include <includes.h>

void OutputPWMPulses(Int32U Freq, Int16U NumberOfPulses, Int8U DutyCyclePercentPWM1, Int8U DutyCyclePercentWaitBetweenPWM1and2, Int8U DutyCyclePercentPWM2, Int8U TimerChannel);
void OutputInitPWMPulses(Int32U Freq, Int16U NumberOfPulses, Int8U DutyCyclePercentPWM1, Int8U TimerChannel);
void PWM_Timer9_C1_E5_Init(Int16U freq, Int8U duty, Int8U pwmMode);
void PWM_Timer4_C1_D12_Init(Int16U freq, Int8U duty, Int8U pwmMode);
void TIM9_IRQHandler(void);
void TIM4_IRQHandler(void);

#endif