#ifndef soc_kalman_h
#define soc_kalman_h

#include <includes.h>

void iterate_KF(kalmanType * KalmanState, matrixType * yk, matrixType * uk);

#endif