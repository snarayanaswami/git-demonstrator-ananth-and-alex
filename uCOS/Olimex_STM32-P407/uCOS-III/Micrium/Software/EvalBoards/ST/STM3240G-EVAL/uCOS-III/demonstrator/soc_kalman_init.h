#ifndef soc_kalman_init_h
#define soc_kalman_init_h

#include <includes.h>

void kalman_matrix_init(kalmanType * KFM);

#endif