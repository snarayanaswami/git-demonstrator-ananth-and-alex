#include <includes.h>

extern Int8U SystemID;
extern Int32U LastButtonTS;
extern nodeInfoType self;
extern packInfoType packInfo;
extern Int8U ScreenToShow;

void HWButtonInit(void)
{
  EXTILine0_Config();
}


void StartStopButtonInit(void)
{
  EXTILine2_Config();
}


void EXTILine0_Config(void)
{
  
  GPIO_InitTypeDef   GPIO_InitStructure;
  NVIC_InitTypeDef   NVIC_InitStructure;
  EXTI_InitTypeDef   EXTI_InitStructure;
  
  /* Enable GPIOA clock */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  /* Enable SYSCFG clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
  
  /* Configure PA0 pin as input floating */
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  
  /* Connect EXTI Line0 to PA0 pin */
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);
  
  /* Configure EXTI Line0 */
  EXTI_InitStructure.EXTI_Line = EXTI_Line0;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);
  
  /* Enable and set EXTI Line0 Interrupt to the lowest priority */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x01;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  
  BSP_IntVectSet(BSP_INT_ID_EXTI0, &EXTI0_IRQHandler);
  BSP_IntEn(BSP_INT_ID_EXTI0);
}


void EXTI0_IRQHandler(void)
{  
  if(EXTI_GetITStatus(EXTI_Line0) != RESET)
  {
    OS_ERR err;
    Int32U now=OSTimeGet(&err);
    // check that some time has passed
    if ((now - LastButtonTS) > 200)
    {
      if (packInfo.status[self.id]==UNDEFINED) SystemID++; // increase SystemID if still undefined
      
      if (packInfo.status[self.id]!=UNDEFINED) // switch between debug screen and standard screen if already initialized
      {
        ScreenToShow++;
        if (ScreenToShow>1)ScreenToShow=0;
      }
    }
    
    LastButtonTS=now;
    
    /* Clear the EXTI line pending bit */
    EXTI_ClearITPendingBit(EXTI_Line0);
    
  }
}

void EXTILine2_Config(void)
{
  GPIO_InitTypeDef   GPIO_InitStructure;
  NVIC_InitTypeDef   NVIC_InitStructure;
  EXTI_InitTypeDef   EXTI_InitStructure;
      
#ifndef BOARD_TUM
      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
      GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
      GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
      GPIO_Init(GPIOC, &GPIO_InitStructure);
      GPIOC->BSRRL |= GPIO_Pin_13;
#else
      RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
      GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
      GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
      GPIO_Init(GPIOB, &GPIO_InitStructure);
      GPIOB->BSRRL |= GPIO_Pin_7;
#endif
  
#ifndef BOARD_TUM
  /* Enable GPIOA clock */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE);
#else
  /* Enable GPIOB clock */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
#endif
  /* Enable SYSCFG clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
  
#ifndef BOARD_TUM
  /* Configure PF2 pin as input floating */
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
  GPIO_Init(GPIOF, &GPIO_InitStructure);
  /* Connect EXTI Line2 to PF2 pin */
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOF, EXTI_PinSource2);
#else
  /* Configure PB6 pin as input floating */
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  /* Connect EXTI Line2 to PB6 pin */
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, EXTI_PinSource6);
#endif
  
#ifndef BOARD_TUM
  /* Configure EXTI Line2 */
  EXTI_InitStructure.EXTI_Line = EXTI_Line2;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Enable and set EXTI Line2 Interrupt to the lowest priority */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI2_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x01;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  
  BSP_IntVectSet(BSP_INT_ID_EXTI2, &EXTI2_IRQHandler);
  BSP_IntEn(BSP_INT_ID_EXTI2);
#else
  /* Configure EXTI Line6 */
  EXTI_InitStructure.EXTI_Line = EXTI_Line6;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Enable and set EXTI Line6 Interrupt to the lowest priority */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x01;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  
  BSP_IntVectSet(BSP_INT_ID_EXTI9_5, &EXTI2_IRQHandler);
  BSP_IntEn(BSP_INT_ID_EXTI9_5);
#endif
  
}


void EXTI2_IRQHandler(void)
{
#ifndef BOARD_TUM
  if(EXTI_GetITStatus(EXTI_Line2) != RESET)
  {
    /* Clear the EXTI line pending bit */
    EXTI_ClearITPendingBit(EXTI_Line2);
#else
  if(EXTI_GetITStatus(EXTI_Line6) != RESET)
  {
    /* Clear the EXTI line pending bit */
    EXTI_ClearITPendingBit(EXTI_Line6);
#endif
    OS_ERR err;
    Int32U now=OSTimeGet(&err);
    // check that some time has passed
    if ((now - LastButtonTS) > 200)
    {
      self.balancingSettings.balancingEnabled=!self.balancingSettings.balancingEnabled;
      
      if (self.balancingSettings.balancingEnabled)
        enableActiveBalancing();
      else
        disableActiveBalancing();
      
      Int8U data[1] = {self.balancingSettings.balancingEnabled};
      sendCANMessage(MSG_TYPE_SET_BALANCING, 0xFF, self.id, 1, data);
      
    }
    LastButtonTS=now;
    
    
    
  }
}
