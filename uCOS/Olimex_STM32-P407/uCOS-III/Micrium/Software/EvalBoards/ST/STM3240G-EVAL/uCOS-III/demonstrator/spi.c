/**
 *********************************************************************************************************
 *
 * @file    spi.c
 * @author  Arne Meeuw
 * @version V0.0.1
 * @date    2-January-2015
 * @brief   Initialization and communication over Serial Peripheral Interface
 *  
 *********************************************************************************************************
 */ 

#include  <spi.h>

/**
 * @brief Initializes the SPI1 peripheral
 * 
 */
void SPI1_Init(void){
  
  GPIO_InitTypeDef GPIO_InitStruct;
  SPI_InitTypeDef SPI_InitStruct;
  
  // enable clock for used IO pins
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
  
  /* configure pins used by SPI1
   * PA5 = SCK   --> NEW BOARD NO CHANGE
   * PA6 = MISO  --> NEW BOARD NO CHANGE
   * PB5 = MOSI  --> NEW BOARD PA7
   */
  GPIO_InitStruct.GPIO_Pin =  GPIO_Pin_6 | GPIO_Pin_5;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStruct);
  
#ifndef BOARD_TUM
  GPIO_InitStruct.GPIO_Pin = GPIO_Pin_5;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOB, &GPIO_InitStruct);
#else
  GPIO_InitStruct.GPIO_Pin = GPIO_Pin_7;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStruct);
#endif  
  
  // connect SPI1 pins to SPI alternate function
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_SPI1);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_SPI1);
#ifndef BOARD_TUM
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource5, GPIO_AF_SPI1);
#else
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_SPI1);
#endif
  
  
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);                 // enable clock for used IO pins
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);                 // enable clock for used IO pins
  
  /* Configure the chip select pin
  in this case we will use PA4 */
  GPIO_InitStruct.GPIO_Pin = GPIO_Pin_4;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &GPIO_InitStruct);
  
  GPIOA->BSRRL |= GPIO_Pin_4; // set PA4 high
    
  // enable peripheral clock
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
  
  /* configure SPI1 in Mode 0 
   * CPOL = 0 --> clock is low when idle
   * CPHA = 0 --> data is sampled at the first edge
   */
  SPI_InitStruct.SPI_Direction = SPI_Direction_2Lines_FullDuplex;       // set to full duplex mode, seperate MOSI and MISO lines
  SPI_InitStruct.SPI_Mode = SPI_Mode_Master;                            // transmit in master mode, NSS pin has to be always high
  SPI_InitStruct.SPI_DataSize = SPI_DataSize_8b;                        // one packet of data is 8 bits wide
  SPI_InitStruct.SPI_CPOL = SPI_CPOL_Low;                               // clock is low when idle
  SPI_InitStruct.SPI_CPHA = SPI_CPHA_1Edge;                             // data sampled at first edge
  SPI_InitStruct.SPI_NSS = SPI_NSS_Soft | SPI_NSSInternalSoft_Set;      // set the NSS management to internal and pull internal NSS high
  SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_32;      // SPI frequency is APB2 frequency / 4
  SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB;                       // data is transmitted MSB first
  SPI_Init(SPI1, &SPI_InitStruct); 
  
  SPI_Cmd(SPI1, ENABLE); // enable SPI1
}


/** 
 * @brief Transmit and receive data with SPI1
 * 
 * @param[in]   data    data to be transmitted
 * @return	received value
 *
 */
Int8U SPI1_send(Int8U data)
{  
  SPI1->DR = data;                              // write data to be transmitted to the SPI data register
  while( !(SPI1->SR & SPI_I2S_FLAG_TXE) );      // wait until transmit complete
  while( !(SPI1->SR & SPI_I2S_FLAG_RXNE) );     // wait until receive complete
  while( SPI1->SR & SPI_I2S_FLAG_BSY );         // wait until SPI is not busy anymore
  return SPI1->DR;                              // return received data from SPI data register
}

/**
 * @brief Sets CS pin to low
 * 
 */
void SPI1_CS_low(void)
{
  GPIOA->BSRRH |= GPIO_Pin_4; // set PA4 (CS) low
}

/**
 * @brief Sets CS pin to high
 * 
 */
void SPI1_CS_high(void)
{
  GPIOA->BSRRL |= GPIO_Pin_4; // set PE7 (CS) high
}
#ifndef BOARD_TUM
/**
 * @brief Read voltage on MCP 3204 DAC 
 * 
 */
float SPI_read_voltage(void)
{
  Int8U data1 = 0;
  Int8U data2 = 0;
  Int16U data = 0;
  float referenceVoltage = 4.964;
  float voltage = 0;
  
  SPI1_CS_low();              // set PA4 (CS) low
  SPI1_send(0x06);            // transmit first byte
  data1 = SPI1_send(0x00);    // transmit second byte and receive data
  data2 = SPI1_send(0x00);    // transmit dummy byte and receive data
  SPI1_CS_high();             // set PE7 (CS) high
  
  data1 &= 0x0F;
  data = (data1<<8) | data2;
  
  voltage = ((float) data * referenceVoltage) / 4096;
 
  return voltage;
}
#else
/**
 * @brief Read voltage on TI ADS8341
 * 
 */
float SPI_read_voltage(void)
{
  Int8U data1 = 0;
  Int8U data2 = 0;
  Int8U data3 = 0;
  Int16U data = 0;
  float referenceVoltage = 4.5;
  float voltage = 0;
  
  SPI1_CS_low();              // set PA4 (CS) low
  SPI1_send(0x97);            // transmit first byte
  data1 = SPI1_send(0x00);    // transmit dummy byte and receive data
  data2 = SPI1_send(0x00);    // transmit dummy byte and receive data
  data3 = SPI1_send(0x00);    // transmit dummy byte and receive data
  SPI1_CS_high();             // set PE4 (CS) high
  
  data = (data1<<8)&0xFFFF;
  data = data | data2;
  data = data<<1;
  data3 = data3>>7;
  data = data | data3;
  
  voltage = (float)data/65535 * referenceVoltage;

  return voltage;
}
#endif

/**
 * @brief Read current on MCP 3204 DAC 
 * 
 */
float read_current(void)
{
#ifdef ONE
  Int8U data1 = 0;
  Int8U data2 = 0;
  Int16U data = 0;
  float referenceVoltage = 4.964;
  float voltage = 0;
  float current = 0;
  
  SPI1_CS_low();              // set PA4 (CS) low
  SPI1_send(0x06);            // transmit first byte
  data1 = SPI1_send(0x40);    // transmit second byte and receive data
  data2 = SPI1_send(0x00);    // transmit dummy byte and receive data
  SPI1_CS_high();             // set PE7 (CS) high
  
  data1 &= 0x0F;
  data = (data1<<8) | data2;
  
  voltage = ((float) data * referenceVoltage) / 4096;
  current = voltage / 1; // 0.3R (=V/A) * 20 (OpAmp) = 6 V/A
  return current;
  
#else
  
  Int16U data1;
  Int16U data2;
  Int16U datacomb;
  float voltage;
  float referenceVoltage=4.964;
  Int8U repetition=10;
  float addVoltage=0;
  float avgVoltage=0;
  float avgCurrent=0;
  
  for (Int8U counter=0;counter<repetition;counter++)
  {
    datacomb=0;
    data1=0;
    data2=0;
    
    SPI1_CS_low();              // set PA4 (CS) low
    SPI1_send(0x06);            // transmit first byte
    data1 = SPI1_send(0x40);    // transmit second byte and receive data
    data1 &= 0x0f;              // 
    data2 = SPI1_send(0x00);    // transmit dummy byte and receive data
    SPI1_CS_high();             // set PE7 (CS) high
    
    datacomb=(data1<<8) + data2;
    voltage = ((float) datacomb * referenceVoltage) / 4096;
    
    addVoltage +=voltage;
   
  }
  avgVoltage = addVoltage / repetition;
  avgCurrent = avgVoltage / 3;  // was 6
  return avgCurrent;
#endif
}

/**
 * @brief Read current on MSP DAC 
 * 
 */
float SPI_read_current(void)
{
  Int16U data1;
  Int16U data2;
  Int16U datacomb;
  float voltage;
  float referenceVoltage=4.964;
  Int8U repetition=100;
  float addVoltage=0;
  float avgVoltage=0;
  float avgCurrent=0;
  
  for (Int8U counter=0;counter<repetition;counter++)
  {
    datacomb=0;
    data1=0;
    data2=0;
    
    SPI1_CS_low();              // set PA4 (CS) low
    SPI1_send(0x06);            // transmit first byte
    data1 = SPI1_send(0x40);    // transmit second byte and receive data
    data1 &= 0x0f;              // 
    data2 = SPI1_send(0x00);    // transmit dummy byte and receive data
    SPI1_CS_high();             // set PE7 (CS) high
    
    datacomb=(data1<<8) + data2;
    voltage = ((float) datacomb * referenceVoltage) / 4096;
    
    addVoltage +=voltage;
   
  }
  avgVoltage = addVoltage / repetition;
  avgCurrent = avgVoltage / 3;  // was 6
  return avgCurrent;
}