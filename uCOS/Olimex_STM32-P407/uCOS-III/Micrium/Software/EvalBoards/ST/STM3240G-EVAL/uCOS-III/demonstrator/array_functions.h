#ifndef array_functions_h
#define array_functions_h


#include <includes.h>


bool isInList(Int8U ID, Int8U *blockedList, Int8U length);
bool isInParticipantList(Int8U ownID, Int8U senderID, Int8U receiverID);
Int8U getListLength(Int8U senderID, Int8U receiverID);
void getParticipantList(Int8U senderID, Int8U receiverID, Int8U * participantList);

float avgVoltage(Int8U fromCell, Int8U toCell);
bool moreVoltageOnLeft(void);
bool lessVoltageOnLeft(void);
float avgSOC(Int8U fromCell, Int8U toCell);
bool moreChargeOnLeft(void);
bool lessChargeOnLeft(void);

float min(float * array, Int8U length);
float max(float * array, Int8U length);

bool isMin(float bound);
bool isMax(float bound);

bool appendBlocker(idType blockerID);
bool removeBlocker(idType blockerID);
bool flushBlockers(void);

#endif