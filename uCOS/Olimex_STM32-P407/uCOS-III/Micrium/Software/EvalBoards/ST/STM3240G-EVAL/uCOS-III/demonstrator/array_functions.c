/**
 *********************************************************************************************************
 *
 * @file    array_functions.c
 * @author  Arne Meeuw
 * @version V0.0.1
 * @date    2-January-2015
 * @brief   Functions for managing, sorting and evaluating arrays
 * 
 *********************************************************************************************************
 */ 

#include <array_functions.h>

extern packInfoType packInfo;   ///< Battery pack information
extern nodeInfoType self;       ///< Cell information
extern Int8U NUMOFNODES;        ///< 



/**
 * @brief Get amount of cells participating in transaction
 * 
 * @param[in]   senderID        ID of cell who is sending charge in the proposed balancing transaction
 * @param[in]   receiverID      ID of cell who is receiving charge in the proposed balancing transaction
 * @return      Int8U           amount of cells participating in transaction from senderID to receiverID
 * 
 */
Int8U getListLength(Int8U senderID, Int8U receiverID)
{
  Int8U startID = 0;
  Int8U endID = 0;
  Int8U length = 0;

  if (senderID > receiverID)
  /// Direction of transaction is UP (senderID > receiverID)
  {   
      if (receiverID == 0)
        /// If receiver is outer-most cell, list starts at 0
        startID = 0;
      else 
#ifdef NOBLOCKERS
        /// if not, list starts at receiver cell
        startID = receiverID;
#else
        /// if not, list starts at cell under receiver
        startID = receiverID-1;
#endif
      
      if (senderID == NUMOFNODES-1)
        /// If sender is outer-most cell, lists ends at last cell
        endID = NUMOFNODES-1;
      else
#ifdef NOBLOCKERS
        /// if not, list ends at sender cell
        endID = senderID;
#else
        /// if not, list ends at cell above sender
        endID = senderID + 1;
#endif
  }
  else
  /// Direction of transaction is DOWN (senderID < receiverID)
  {
      if (senderID == 0)
        /// If sender is outer-most cell, list starts at 0
        startID = 0;
      else
#ifdef NOBLOCKERS
        /// if not, list starts at sender cell
        startID = senderID;
#else
        /// if not, list starts at cell under sender
        startID = senderID-1;
#endif
      
      if (receiverID == NUMOFNODES-1)
        /// If receiver is outer-most cell, lists ends at last cell
        endID = NUMOFNODES-1;
      else
#ifdef NOBLOCKERS
        /// if not, list ends at receiver cell
        endID = receiverID;
#else
        /// if not, list ends at cell above receiver
        endID = receiverID + 1;
#endif
  }
  
  /// Length is distance between start and end cell
  length = abs(startID-endID)+1;
  return length;
}

/**
 * @brief Compile list of participants of proposed transaction
 * 
 * @param[in]           senderID                ID of cell who is sending charge in the proposed balancing transaction
 * @param[in]           receiverID              ID of cell who is receiving charge in the proposed balancing transaction
 * @param[in,out]       *participantList        Pointer to array containing the participants in transaction
 * 
 */
void getParticipantList(Int8U senderID, Int8U receiverID, Int8U * participantList)
{
  Int8U startID = 0;
  Int8U endID = 0;
  Int8U length = 0;
  
  if (senderID > receiverID)
  /// Direction of transaction is UP (senderID > receiverID)
  {   
      if (receiverID == 0)
        /// If receiver is outer-most cell, list starts at 0
        startID = 0;
      else 
#ifdef NOBLOCKERS
        /// if not, list starts at receiver cell
        startID = receiverID;
#else
        /// if not, list starts at cell under receiver
        startID = receiverID-1;
#endif
      
      if (senderID == NUMOFNODES-1)
        /// If sender is outer-most cell, lists ends at last cell
        endID = NUMOFNODES-1;
      else
#ifdef NOBLOCKERS
        /// if not, list ends at sender cell
        endID = senderID;
#else
        /// if not, list ends at cell above sender
        endID = senderID + 1;
#endif
  }
  else
  /// Direction of transaction is DOWN (senderID < receiverID)
  {
      if (senderID == 0)
        /// If sender is outer-most cell, list starts at 0
        startID = 0;
      else
#ifdef NOBLOCKERS
        /// if not, list starts at sender cell
        startID = senderID;
#else
        /// if not, list starts at cell under sender
        startID = senderID-1;
#endif
      
      if (receiverID == NUMOFNODES-1)
        /// If receiver is outer-most cell, lists ends at last cell
        endID = NUMOFNODES-1;
      else
#ifdef NOBLOCKERS
        /// if not, list ends at receiver cell
        endID = receiverID;
#else
        /// if not, list ends at cell above receiver
        endID = receiverID + 1;
#endif
  }
  
  /// Length is distance between start and end cell
  length = abs(startID-endID)+1;
  
  /// participantList starts at *startID* and conists of *length*-elements
  for (Int8U i=0; i<length; i++)
    participantList[i] = startID + i;
}

/**
 * @brief Check if given ID is part of proposed balancing transaction
 * 
 * @param[in]   ownID           ID to be checked
 * @param[in]   senderID        ID of cell who is sending charge in the proposed balancing transaction
 * @param[in]   receiverID      ID of cell who is receiving charge in the proposed balancing transaction
 * @return      bool            True/False whether ownID is part of the balancing transaction
 * 
 */
bool isInParticipantList(Int8U ownID, Int8U senderID, Int8U receiverID)
{
  bool result = 0;
  /// Get length of list for memory allocation
  Int8U length = getListLength(senderID, receiverID);
  /// Declarate pointer to allocated space for list
  Int8U * participantList = (Int8U*) malloc(length);
  /// Get participantList
  getParticipantList(senderID, receiverID, participantList);
  /// Check if ownID is in participantList
  result = isInList(ownID, participantList, length);
  /// Free allocated memory in heap
  free(participantList);
  return result;
}

/**
 * @brief Check if given ID is in array
 * 
 * @param[in]   ID              ID to be checked
 * @param[in]   *blockedList    Pointer to array
 * @param[in]   length          Length of array
 * @return      bool            True/False whether ID is in given List
 * 
 */
bool isInList(Int8U ID, Int8U *blockedList, Int8U length)
{
  /// Iterate over every element of given array
  for (Int8U i=0; i<length; i++)
  {
    if (blockedList[i] == ID)
      /// return **true** if matching element is found
      return true;
  }
  /// If no element matched, return **false**
  return false;
}

/**
 * @brief Calculate average voltage in pack
 * 
 * @param[in]   fromCell        ID of cell from which to start averaging
 * @param[in]   toCell          ID of cell to where averaging ends
 * @return      float           avgVoltage between fromCell and toCell
 * 
 */
float avgVoltage(Int8U fromCell, Int8U toCell)
{       
  float sumVoltage = 0;
  
  if (fromCell < toCell)
  {
    /// (fromCell < toCell) -> Iterate up
    for (Int8U counter = fromCell; counter < toCell; counter++)
    {
      sumVoltage+=packInfo.voltage[counter];
    }
  }
  else
  {
    /// (fromCell > toCell) -> Iterate down
    for (Int8U counter = fromCell; counter > toCell; counter--)
    {
      sumVoltage+=packInfo.voltage[counter];
    }
  }
  /// Return voltage sum divided by number of elements
  return sumVoltage/abs(toCell-fromCell);
}

/**
 * @brief Calculate if average voltage is higher on left side
 * 
 * @return      bool            True/False
 * 
 */
bool moreVoltageOnLeft(void)
{
  if (self.id == 0)
    /// If self is on left border return **false**
    return false;
          
  if (self.id == NUMOFNODES-1)
    /// If self is on right border return **true**
    return true;
  
  /// Calculate average SOC from cell 0 to self
  float meanValLeft = avgVoltage(0,self.id);
  /// Calculate average SOC from self+1 to outer most cell
  float meanValRight = avgVoltage(self.id+1,NUMOFNODES);
  
  if (meanValLeft > meanValRight)
    /// Left average is larger -> return **true**
    return true;
  else
    /// Right average is larger -> return **false**
    return false;
}

/**
 * @brief Calculate if average voltage is lower on left side
 * 
 * @return      bool            True/False
 * 
 */
bool lessVoltageOnLeft(void)
{
  if (self.id == 0)
    /// If self is on left border return **false**
    return false;
          
  if (self.id == NUMOFNODES-1)
    /// If self is on right border return **true**
    return true;
  
  /// Calculate average SOC from cell 0 to self
  float meanValLeft = avgVoltage(0,self.id);
  /// Calculate average SOC from self+1 to outer most cell
  float meanValRight = avgVoltage(self.id+1,NUMOFNODES);
  
  if (meanValLeft < meanValRight)
    /// Left average is smaller -> return **true**
    return true;
  else
    /// Right average is smaller -> return **false**
    return false;
}

/**
 * @brief Calculate average SOC in pack
 * 
 * @param[in]   fromCell        ID of cell from which to start averaging
 * @param[in]   toCell          ID of cell to where averaging ends
 * @return      float           average SOC between fromCell and toCell
 * 
 */
float avgSOC(Int8U fromCell, Int8U toCell)
{       
  float sumSOC = 0;
  if (fromCell < toCell)
  {
    /// (fromCell > toCell) -> Iterate down
    for (Int8U counter = fromCell; counter < toCell; counter++)
      sumSOC+=packInfo.soc[counter];
  }
  else
  {
    /// (fromCell > toCell) -> Iterate down
    for (Int8U counter = fromCell; counter > toCell; counter--)
    {
      sumSOC+=packInfo.soc[counter];
    }
  }
  /// Return SOC sum divided by number of elements
  return sumSOC/abs(toCell-fromCell);
}

/**
 * @brief Calculate if average SOC is higher on left side
 * 
 * @return      bool            True/False
 * 
 */
bool moreChargeOnLeft(void)
{
  if (self.id == 0)
    /// If self is on left border return **false**
    return false;
          
  if (self.id == NUMOFNODES-1)
    /// If self is on right border return **true**
    return true;
  
  /// Calculate average SOC from cell 0 to self
  float meanValLeft = avgSOC(0,self.id);
  /// Calculate average SOC from self+1 to outer most cell
  float meanValRight = avgSOC(self.id+1,NUMOFNODES);
  
  if (meanValLeft > meanValRight)
    /// Left average is larger -> return **true**
    return true;
  else
    /// Right average is larger -> return **false**
    return false;
}

/**
 * @brief Calculate if average SOC is lower on left side
 * 
 * @return      bool            True/False
 * 
 */
bool lessChargeOnLeft(void)
{
  if (self.id == 0)
    /// If self is on left border return **false**
    return false;
          
  if (self.id == NUMOFNODES-1)
    /// If self is on right border return **true**
    return true;
  
  /// Calculate average SOC from cell 0 to self
  float meanValLeft = avgSOC(0,self.id);
  /// Calculate average SOC from self+1 to outer most cell
  float meanValRight = avgSOC(self.id+1,NUMOFNODES);
  
  if (meanValLeft < meanValRight)
    /// Left average is smaller -> return **true**
    return true;
  else
    /// Right average is smaller -> return **false**
    return false;
}

/**
 * @brief Calculate minimum value in array
 * 
 * @param[in]   array           Array to be checked
 * @param[in]   length          Length of array
 * @return      float           minimum in array
 * 
 */
float min(float * array, Int8U length)
{
  /// Set compare element to first element of array
  float min = array[0];
  /// Iterate over every element of array
  for(Int8U i = 0; i<length; i++)
    if(min > array[i])
      /// Element is smaller than compare element, assign new compare element
      min = array[i];
  /// Return smallest element
  return min;
}

/**
 * @brief Calculate maximum value in array
 * 
 * @param[in]   array           Array to be checked
 * @param[in]   length          Length of array
 * @return      float           maximum in array
 * 
 */
float max(float * array, Int8U length)
{
  /// Set compare element to first element of array
  float max = array[0];
  /// Iterate over every element of array
  for(Int8U i = 0; i<length; i++)
    if(max < array[i])
      /// Element is larger than compare element, assign new compare element
      max = array[i];
  /// Return largest element
  return max;
}

/**
 * @brief Return if cell is minimum in pack with given bound
 * 
 * @param[in]   bound           bound
 * @return      bool            cell is minimum in pack
 * 
 */
bool isMin(float bound)
{
  bool result = false;

  result = ( self.voltage <= (min(packInfo.voltage, NUMOFNODES)+bound) );
    
  return result;
}

/**
 * @brief Return if cell is maximum in pack with given bound
 * 
 * @param[in]   bound           bound
 * @return      bool            cell is maximum in pack
 * 
 */
bool isMax(float bound)
{
  bool result = false;

  result = ( self.voltage >= (max(packInfo.voltage, NUMOFNODES)-bound) );
  
  return result;
}

/**
 * @brief Append blockerID to list of blockerIDs
 * 
 * @param[in]   blockerID       blockerID to append
 * @return      bool            status of operation
 * 
 */
bool appendBlocker(idType blockerID)
{
  for (Int8U i = 0; i<2; i++)
  {
    if (self.blockerIDs[i] == 0xFF)
    {
      self.blockerIDs[i] = blockerID;
      self.blockers++;
      return true;
    }
  }
  return false;
}

/**
 * @brief Remove blockerID from list of blockerIDs
 * 
 * @param[in]   blockerID       blockerID to remove
 * @return      bool            status of operation
 * 
 */
bool removeBlocker(idType blockerID)
{
  for (Int8U i = 0; i<2; i++)
  {
    if (self.blockerIDs[i] == blockerID)
    {
      self.blockerIDs[i] = 0xFF;
      self.blockers--;
      return true;
    }
  }
  return false;
}

/**
 * @brief Remove all blockerIDs from list of blockerIDs
 * 
 * @return      bool            status of operation
 * 
 */
bool flushBlockers(void)
{
  self.blockerIDs[0] = 0xFF;
  self.blockerIDs[1] = 0xFF;
  self.blockers      = 0;
  return true;
}