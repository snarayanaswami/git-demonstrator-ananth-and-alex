#ifndef spi_h
#define spi_h

#include <includes.h>

void SPI1_Init(void);
Int8U SPI1_send(Int8U data);
void SPI1_CS_low(void);
void SPI1_CS_high(void);
float SPI_read_voltage(void);
float read_current(void);
float SPI_read_current(void);

float SPI_read_voltage_8341(void);
#endif