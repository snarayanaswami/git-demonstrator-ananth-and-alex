#ifndef switch_h
#define switch_h


#include <includes.h>

//#define bin(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p) (a<<15)|(b<<14)|(c<<13)|(d<<12)|(e<<11)|(f<<10)|(g<<9)|(h<<8)|(i<<7)|(j<<6)|(k<<5)|(l<<4)|(m<<3)|(n<<2)|(o<<1)|(p<<0) 
#ifndef BOARD_TUM
#define bin(Sw0,Sw1,Sw2,Sw3,Sw4,Sw5,Sw6,Sw7,Sw8,Sw9,Sw10,Sw11) (Sw0<<11)|(Sw1<<10)|(Sw2<<9)|(Sw3<<8)|(Sw4<<7)|(Sw5<<6)|(Sw6<<5)|(Sw7<<4)|(Sw8<<3)|(Sw9<<2)|(Sw10<<1)|(Sw11<<0) 
#else
#define bin(Sw0,Sw1,Sw2,Sw3,Sw4,Sw5,Sw6,Sw7) (Sw0<<11)|(Sw1<<10)|(Sw2<<9)|(Sw3<<8)|(Sw4<<7)|(Sw5<<6)|(Sw6<<5)|(Sw7<<4) 
#endif
#ifndef BOARD_TUM

/* Switch 0 */
#define Switch00
#define Switch00Port         GPIOD
#define Switch00Pin          GPIO_Pin_9
#define Switch00Clock        RCC_AHB1Periph_GPIOD

/* Switch 1 */
#define Switch01
#define Switch01Port         GPIOD
#define Switch01Pin          GPIO_Pin_12
#define Switch01Clock        RCC_AHB1Periph_GPIOD

/* Switch 2 */
#define Switch02
#define Switch02Port         GPIOD
#define Switch02Pin          GPIO_Pin_10
#define Switch02Clock        RCC_AHB1Periph_GPIOD

/* Switch 3 */
#define Switch03
#define Switch03Port         GPIOD
#define Switch03Pin          GPIO_Pin_11
#define Switch03Clock        RCC_AHB1Periph_GPIOD

/* Switch 4 */
#define Switch04
#define Switch04Port         GPIOD
#define Switch04Pin          GPIO_Pin_13
#define Switch04Clock        RCC_AHB1Periph_GPIOD

/* Switch 5 */
#define Switch05
#define Switch05Port         GPIOD
#define Switch05Pin          GPIO_Pin_14
#define Switch05Clock        RCC_AHB1Periph_GPIOD

/* Switch 6 */
#define Switch06
#define Switch06Port         GPIOE
#define Switch06Pin          GPIO_Pin_3
#define Switch06Clock        RCC_AHB1Periph_GPIOE

/* Switch 7 */
#define Switch07
#define Switch07Port         GPIOE
#define Switch07Pin          GPIO_Pin_4
#define Switch07Clock        RCC_AHB1Periph_GPIOE

#else

/* T1 - Switch 0 */
#define Switch00
#define Switch00Port         GPIOD
#define Switch00Pin          GPIO_Pin_10
#define Switch00Clock        RCC_AHB1Periph_GPIOD

/* T2 - Switch 1 */
#define Switch01
#define Switch01Port         GPIOD
#define Switch01Pin          GPIO_Pin_11
#define Switch01Clock        RCC_AHB1Periph_GPIOD

/* B1 - Switch 2 */
#define Switch02
#define Switch02Port         GPIOD
#define Switch02Pin          GPIO_Pin_13
#define Switch02Clock        RCC_AHB1Periph_GPIOD

/* B2 - Switch 3 */
#define Switch03
#define Switch03Port         GPIOD
#define Switch03Pin          GPIO_Pin_14
#define Switch03Clock        RCC_AHB1Periph_GPIOD

/* P - Switch 4 */
#define Switch04
#define Switch04Port         GPIOD
#define Switch04Pin          GPIO_Pin_12
#define Switch04Clock        RCC_AHB1Periph_GPIOD

/* S - Switch 5 */
#define Switch05
#define Switch05Port         GPIOE
#define Switch05Pin          GPIO_Pin_5
#define Switch05Clock        RCC_AHB1Periph_GPIOE

/* BB1 - Switch 6 */
#define Switch06
#define Switch06Port         GPIOE
#define Switch06Pin          GPIO_Pin_3
#define Switch06Clock        RCC_AHB1Periph_GPIOE

/* BB2 - Switch 7 */
#define Switch07
#define Switch07Port         GPIOE
#define Switch07Pin          GPIO_Pin_4
#define Switch07Clock        RCC_AHB1Periph_GPIOE

#endif

/* Switch 8 */
#define Switch08
#define Switch08Port         GPIOE
#define Switch08Pin          GPIO_Pin_5
#define Switch08Clock        RCC_AHB1Periph_GPIOE

/* Switch 9 */
#define Switch09
#define Switch09Port         GPIOE
#define Switch09Pin          GPIO_Pin_6
#define Switch09Clock        RCC_AHB1Periph_GPIOE

/* Switch 10 */
#define Switch10
#define Switch10Port         GPIOE
#define Switch10Pin          GPIO_Pin_7
#define Switch10Clock        RCC_AHB1Periph_GPIOE

/* Switch 11 */
#define Switch11
#define Switch11Port         GPIOE
#define Switch11Pin          GPIO_Pin_8
#define Switch11Clock        RCC_AHB1Periph_GPIOE

void SwitchConfig(Int8U mode);
void SwitchInit(void);
void Switch(Int16U switchConfig);


#endif