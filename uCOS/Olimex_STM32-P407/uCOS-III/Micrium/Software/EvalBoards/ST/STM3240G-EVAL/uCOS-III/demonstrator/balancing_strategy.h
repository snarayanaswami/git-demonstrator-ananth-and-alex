#ifndef balancing_strategy_h
#define balancing_strategy_h

#include <includes.h>

void smartCellStart(void);

bool requestChargeCondition(void);
void determineSender(transactionInfoType *transactionRequest);

bool sendChargeCondition(void);
void determineReceiver(transactionInfoType *transactionRequest);

bool acknowledgeSendCondition(void);

bool sendBroadcastCondition(void);

void beforeSend(void);
void afterSend(void);
void beforeReceive(void);
void afterReceive(void);

void enableActiveBalancing(void);
void disableActiveBalancing(void);

#endif