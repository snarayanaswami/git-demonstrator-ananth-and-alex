/*! \mainpage Documentation for TUM CREATE SmartCell-Platform
 *
 * \section intro_sec Introduction
 * 
 *  The TUM CREATE SmartCell Platform is the codebase meant for a selection of hardware meant to provide battery management functions.
 *  Battery management is done decentralized based on node processing power and aims for concurrent cell balancing spaning multiple cells.
 * 
 * \section hardware_sec Hardware
 *
 *  This software is meant to run on a STM32F04 ARM Cortex-M4 chip-family:
 *  - Olimex E407 Evaluation Board (<code>#define BOARD_E407</code>)
 *  - Olimex P407 Evaluation Board (<code>#define BOARD_P407</code>)
 *  - IAR Evaluation Board (<code>#define BOARD_IAR</code>)
 *  - TUM CREATE Control Board (<code>#define BOARD_TUM</code>)
 * 
 * \section toolchain_sec Toolchain information
 *  
 *  IAR Embedded Workbench is used as IDE for writing, compiling and deploying code to hardware.
 *  
 * \section disclaimer_sec Research disclaimer
 * 
 *  This platform is being developed as part of a master's thesis at TUM CREATE ltd. Singapore.
 *  The software provided is strictly for research purposes.
 *
 */

/**
 *********************************************************************************************************
 *                                     TUM CREATE SmartCell Platform
 *
 * @file    app.c
 * @author  Arne Meeuw
 * @version V0.0.1
 * @date    2-January-2015
 * @brief   Main application source file responsible for task-management
 *  
 *********************************************************************************************************
 * @attention
 * 
 * PRELIMINARY RELEASE
 *
 * <h2><center>&copy; COPYRIGHT 2015 TUM CREATE Limited</center></h2>
 *********************************************************************************************************
 */ 


/*
 *********************************************************************************************************
 *                                             INCLUDE FILES
 *********************************************************************************************************
 */

#include  <app_cfg.h>
#include  <includes.h>

/*
 *********************************************************************************************************
 *                                            LOCAL DEFINES
 *********************************************************************************************************
 */

#define DLY_100US  1000

/*
 *********************************************************************************************************
 *                                       LOCAL GLOBAL VARIABLES
 *********************************************************************************************************
 */

/* ----------------- APPLICATION GLOBALS ------------------ */
static  OS_TCB   AppTaskStartTCB;
static  CPU_STK  AppTaskStartStk[APP_CFG_TASK_START_STK_SIZE];

/// Miscellaneous system tasks & stacks
OS_TCB ScreenRefreshTaskTCB,                    ///< Task declaration for ScreenRefresh
       StatusTask;                              ///< Task declaration for StatusMessageTask 
static CPU_STK ScreenRefreshTaskStk[512],       ///< Stack declaration for ScreenRefresh
               TaskBStk[128];                   ///< Stack declaration for 


/// Cell parameter tasks & stacks
OS_TCB VoltageAvgTask,                          ///< Task declaration for Voltage Averaging Task
       CurrentSmplTask;                         ///< Task declaration for Current Sampling Task
static CPU_STK VoltageAverageTaskStk[128],      ///< Stack declaration for Voltage Averaging Task
       CurrentSamplingTaskStk[64];             ///< Stack declaration for Current Sampling Task

/// Balancingstrategy related tasks & stacks
OS_TCB RequestTask,                             ///< Task declaration for Request Strategy
       AcknowledgeTask,                         ///< Task declaration for Acknowledge Strategy
       BlockCellsTask,                          ///< Task declaration for Cell Blocking
       MonitorPackTask;                         ///< Task declaration for monitoring of pack
static CPU_STK RequestTaskStack[128],           ///< Stack declaration for Request Strategy
               AcknowledgeTaskStack[128],       ///< Stack declaration for Acknowledge Strategy
               BlockCellsTaskStk[128],          ///< Stack declaration for Cell Blocking
               MonitorPackTaskStk[64];          ///< Stack declaration for Pack Monitoring

/// CAN processing tasks & stacks
OS_TCB ExtendedCANTask;                         ///< Task declaration for Process CAN Messages
static CPU_STK ExtendedCANMsgTaskStk[128];      ///< Stack declaration for Process CAN Messages


OS_Q BlockResponseQ;                            ///< message queue for block responses

OS_Q LeftUARTQ;                                 ///< message queue for block responses
OS_Q RightUARTQ;                                ///< message queue for block responses

UARTRxMsg RxUARTRMessage;
UARTRxMsg RxUARTLMessage;

transactionInfoType rxTransaction;       
transactionInfoType txTransaction;   

CanRxMsg RxMessage;

OS_SEM blockCellsSemaphore;                     ///< semaphore for waiting on blocking cells to finish
OS_SEM sendChargeSemaphore;                     ///< semaphore for waiting on PWM pulses to finish

Int16U PWMPulseCounter;                         ///< 
Int16U PWMPulseCounter2;                        ///< 
Int16U PWMCount;                                ///< 
Int16U PWMWaitPercent;                          ///< 
Int8U  PWMTimerChannel;                         ///< 

Int8U SystemID;                                 ///< 
Int32U LastButtonTS;                            ///< Timestamp of last button-press
packInfoType packInfo;                          ///< Battery pack information
nodeInfoType self;                              ///< Cell information

Int8U ScreenToShow;                             ///< 
Int16U spriteCounterX=0;                        ///< 
Int16U spriteCounterY=2;                        ///< 
Int16U walkCounter=0;                           ///< 

#define VHISTITEMS 132                          ///< Amount of voltage history items
Int16U voltageTimeScaler = 50;                 ///< 
float voltageHistory[VHISTITEMS];               ///< voltage history array
Int16S voltageHistoryOffset;                    ///< display offset

#define CHISTITEMS 132                          ///< Amount of current history items
Int16U currentTimeScaler = 1;                 ///< 
float currentHistory[CHISTITEMS];               ///< current history array
Int16S currentHistoryOffset;                    ///< display offset

#define SHISTITEMS 132                          ///< Amount of SOC history items
Int16U socTimeScaler = 2;                       ///< 
float socHistory[SHISTITEMS];                   ///< SOC history array
Int16S socHistoryOffset;                        ///< display offset

float currentSum;                               ///< 

float globDiffVoltage=0.5;                      ///< 

Int8U ID_AUTODETECT = 1;                        ///< 
Int8U autoInitState = 0;                        ///< 
Int8U NUMOFNODES;                               ///< 
Int8U neighborTrigger=0;                        ///< 
Int8U enableCurrent=true;                       ///< 

struct listNode *leftRoot;                      ///< 
struct listNode *rightRoot;                     ///< 
struct listNode *leftEnd;                       ///< 
struct listNode *rightEnd;                      ///< 
Int8U secWaitAfterNTrigger=2;                   ///< 


/*
 *********************************************************************************************************
 *                                         FUNCTION PROTOTYPES
 *********************************************************************************************************
 */


static  void  AppTaskStart          (void     *p_arg);
static  void  AppTaskCreate         (void);
static  void  AppObjCreate          (void);

static  void  ScreenRefreshTask     (void     *p_arg);
static  void  StatusMessageTask     (void     *p_arg);

static  void  ProcessExtendedCANMsg (void     *p_arg);

static  void  VoltageAverageTask    (void     *p_arg);
static  void  CurrentSampler        (void     *p_arg);

static  void  BlockCells            (void     *p_arg);
static  void  RequestStrategy       (void     *p_arg);
static  void  AcknowledgeStrategy   (void     *p_arg);
static  void  MonitorPack           (void     *p_arg);

int main(void)
{
  OS_ERR  err;
  BSP_IntDisAll();                                            /* Disable all interrupts.                              */
  
  OSInit(&err);                                               /* Init uC/OS-III.                                      */
  
  
  OSTaskCreate((OS_TCB       *)&AppTaskStartTCB,              /* Create the start task                                */
               (CPU_CHAR     *)"App Task Start",
               (OS_TASK_PTR   )AppTaskStart, 
               (void         *)0,
               (OS_PRIO       )APP_CFG_TASK_START_PRIO,
               (CPU_STK      *)&AppTaskStartStk[0],
               (CPU_STK_SIZE  )AppTaskStartStk[APP_CFG_TASK_START_STK_SIZE / 10],
               (CPU_STK_SIZE  )APP_CFG_TASK_START_STK_SIZE,
               (OS_MSG_QTY    )0,
               (OS_TICK       )0,
               (void         *)0,
               (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
               (OS_ERR       *)&err);
  
  OSQCreate(&BlockResponseQ,
            "Block Response Queue",
            32,
            &err);
  OSQCreate(&LeftUARTQ,
            "Left UART Queue",
            32,
            &err);
  OSQCreate(&RightUARTQ,
            "Right UART Queue",
            32,
            &err);
  
OSSemCreate(&blockCellsSemaphore,
            "Wait blocking of cells",
            0,
            &err);
  
OSSemCreate(&sendChargeSemaphore,
            "Wait to finish sending charge",
            0,
            &err);


  OSStart(&err);                                              /* Start multitasking (i.e. give control to uC/OS-III). */
  
  (void)&err;
  
  return (0);
}

/*************************************************************************
* Function Name: DelayResolution100us
* Parameters: Int32U Dly
*
* Return: none
*
* Description: Delay ~ (arg * 100us)
*
*************************************************************************/
void DelayResolution100us(Int32U Dly)
{
  for(; Dly; Dly--)
  {
    for(volatile Int32U j = DLY_100US; j; j--)
    {
    }
  }
}

/*
*********************************************************************************************************
*                                          STARTUP TASK
*
* Description : This is an example of a startup task.  As mentioned in the book's text, you MUST
*               initialize the ticker only once multitasking has started.
*
* Arguments   : p_arg   is the argument passed to 'AppTaskStart()' by 'OSTaskCreate()'.
*
* Returns     : none
*
* Notes       : 1) The first line of code is used to prevent a compiler warning because 'p_arg' is not
*                  used.  The compiler should not generate any code for this statement.
*********************************************************************************************************
*/

static  void  AppTaskStart (void *p_arg)
{
  OS_ERR      err;
  
  /* Init global variables */
  
  SystemID = 0;
  PWMTimerChannel=1;
  packInfo.voltage[self.id] = 0;
  self.blockerIDs[0] = 0xFF;
  self.blockerIDs[1] = 0xFF;
  self.receiveActive = false;
  self.balancingSettings.balancingEnabled = false;
  self.balancingSettings.monitorEnabled = false;
  
  globDiffVoltage=0;
  RNG_Init();
 
  voltageHistoryOffset=VHISTITEMS-1;
  for (Int8U counter=0;counter<VHISTITEMS;counter++)
    voltageHistory[counter]=-100;
  
  socHistoryOffset=SHISTITEMS-1;
  for (Int8U counter=0;counter<SHISTITEMS;counter++)
    socHistory[counter]=-100;
    
  
  LastButtonTS=OSTimeGet(&err);
  
  // create root nodes for neighbor identification
  leftRoot= (struct listNode *) malloc( sizeof(struct listNode) ); 
  rightRoot= (struct listNode *) malloc( sizeof(struct listNode) ); 
  leftEnd=leftRoot;
  rightEnd=rightRoot;
  leftRoot->next=0;
  rightRoot->next=0;
  leftRoot->ID=0;
  rightRoot->ID=0;
  // create list end nodes for neighbor identification
  //leftEnd= (struct listNode *) malloc( sizeof(struct listNode) ); 
  //rightEnd= (struct listNode *) malloc( sizeof(struct listNode) ); 
  
  
  (void)p_arg;
  
  BSP_Init();                                                 /* Initialize BSP functions                             */
  CPU_Init();                                                 /* Initialize the uC/CPU services                       */
  
  BSP_Tick_Init();                                            /* Start Tick Initialization                            */
  
  Mem_Init();                                                 /* Initialize Memory Management Module                  */
  Math_Init();                                                /* Initialize Mathematical Module                       */
  
  
#if OS_CFG_STAT_TASK_EN > 0u
  OSStatTaskCPUUsageInit(&err);                               /* Compute CPU capacity with no task running            */
#endif
  
#ifdef CPU_CFG_INT_DIS_MEAS_EN
  CPU_IntDisMeasMaxCurReset();
#endif
  
#if (APP_CFG_SERIAL_EN == DEF_ENABLED)    
  App_SerialInit();                                           /* Initialize Serial Communication                      */
#endif
  
#ifndef BOARD_TUM  
  BufLCDInit();                                               /* Initialize LCD                                       */
#else
  initialize_IO();
  LCD_INITSPI_EADOGM162();
#endif
  
  BoardLEDInit();
  
  CAN1_Init(1000);
  
  SPI1_Init();  
  
  HWButtonInit();
  StartStopButtonInit();
  
  SwitchInit();
  
  ControlInit();
  
  APP_TRACE("SmartCell Balancing Platform\n");
  APP_TRACE_DBG(("Creating Application kernel objects\n\r"));
  
  AppObjCreate();                                             /* Create Applicaiton kernel objects                    */
  
  APP_TRACE_DBG(("Creating Application Tasks\n\r"));
  AppTaskCreate();                                            /* Create Application tasks                             */
  
  while (DEF_TRUE) {                                          /* Task body, always written as an infinite loop.       */
    // end task
    OSTaskDel((OS_TCB *) &AppTaskStartTCB, &err);
  }
}


// Create application tasks. Caller(s)   : AppTaskStart()
static  void  AppTaskCreate (void)
{
  OS_ERR      err;
  OSTaskCreate((OS_TCB       *)&ScreenRefreshTaskTCB,  // Create user task A
               (CPU_CHAR     *)"ScreenRefreshTask",
               (OS_TASK_PTR   )ScreenRefreshTask, 
               (void         *)0,
               (OS_PRIO       )6,
               (CPU_STK      *)&ScreenRefreshTaskStk[0],
               (CPU_STK_SIZE  )0,
               (CPU_STK_SIZE  )512,
               (OS_MSG_QTY    )0,
               (OS_TICK       )0,
               (void         *)0,
               (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
               (OS_ERR       *)&err);
  
  /*#################################################################################*/
  /*                      CAN Processing Tasks                                       */
  /*#################################################################################*/
 
  OSTaskCreate((OS_TCB       *)&ExtendedCANTask,              // Create ExtendedCANTask
               (CPU_CHAR     *)"ProcessExtendedCANMsg",
               (OS_TASK_PTR   )ProcessExtendedCANMsg, 
               (void         *)0,
               (OS_PRIO       )1,
               (CPU_STK      *)&ExtendedCANMsgTaskStk[0],
               (CPU_STK_SIZE  )0,
               (CPU_STK_SIZE  )128,
               (OS_MSG_QTY    )32,
               (OS_TICK       )0,
               (void         *)0,
               (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
               (OS_ERR       *)&err);
  
  /*#################################################################################*/
  /*                            OTHER TASKS                                          */
  /*#################################################################################*/  

  OSTaskCreate((OS_TCB       *)&StatusTask,                    // Create StatusMessageTask
               (CPU_CHAR     *)"StatusMessageTask",
               (OS_TASK_PTR   )StatusMessageTask, 
               (void         *)0,
               (OS_PRIO       )5,
               (CPU_STK      *)&TaskBStk[0],
               (CPU_STK_SIZE  )0,
               (CPU_STK_SIZE  )128,
               (OS_MSG_QTY    )0,
               (OS_TICK       )0,
               (void         *)0,
               (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
               (OS_ERR       *)&err);
    
  /*#################################################################################*/
  /*                      BALANCING STRATEGY TASKS                                   */
  /*#################################################################################*/
    
  OSTaskCreate((OS_TCB       *)&BlockCellsTask,              // Create BlockCellsTask
               (CPU_CHAR     *)"BlockCells Task",
               (OS_TASK_PTR   )BlockCells, 
               (void         *)0,
               (OS_PRIO       )2,
               (CPU_STK      *)&BlockCellsTaskStk[0],
               (CPU_STK_SIZE  )0,
               (CPU_STK_SIZE  )128,
               (OS_MSG_QTY    )32,
               (OS_TICK       )0,
               (void         *)0,
               (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
               (OS_ERR       *)&err);
  
  OSTaskCreate((OS_TCB       *)&RequestTask,                    // Create RequestTask
               (CPU_CHAR     *)"Charge Requesting Task",
               (OS_TASK_PTR   )RequestStrategy, 
               (void         *)0,
               (OS_PRIO       )3,
               (CPU_STK      *)&RequestTaskStack[0],
               (CPU_STK_SIZE  )0,
               (CPU_STK_SIZE  )128,
               (OS_MSG_QTY    )0,
               (OS_TICK       )0,
               (void         *)0,
               (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
               (OS_ERR       *)&err);

  OSTaskCreate((OS_TCB       *)&AcknowledgeTask,                // Create AcknowledgeTask
               (CPU_CHAR     *)"Charge Acknowledging Task",
               (OS_TASK_PTR   )AcknowledgeStrategy, 
               (void         *)0,
               (OS_PRIO       )3,
               (CPU_STK      *)&AcknowledgeTaskStack[0],
               (CPU_STK_SIZE  )0,
               (CPU_STK_SIZE  )128,
               (OS_MSG_QTY    )32,
               (OS_TICK       )0,
               (void         *)0,
               (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
               (OS_ERR       *)&err);
  
  OSTaskCreate((OS_TCB       *)&MonitorPackTask,                // Create MonitorPackTask
               (CPU_CHAR     *)"Pack monitoring task",
               (OS_TASK_PTR   )MonitorPack, 
               (void         *)0,
               (OS_PRIO       )3,
               (CPU_STK      *)&MonitorPackTaskStk[0],
               (CPU_STK_SIZE  )0,
               (CPU_STK_SIZE  )64,
               (OS_MSG_QTY    )0,
               (OS_TICK       )0,
               (void         *)0,
               (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
               (OS_ERR       *)&err);
  
  
  /*#################################################################################*/
  /*                      CELL PARAMETER RELATED TASKS                               */
  /*#################################################################################*/
  
  OSTaskCreate((OS_TCB       *)&VoltageAvgTask,              // Create VoltageAvgTask
               (CPU_CHAR     *)"VoltageAverageTask",
               (OS_TASK_PTR   )VoltageAverageTask, 
               (void         *)0,
               (OS_PRIO       )5,
               (CPU_STK      *)&VoltageAverageTaskStk[0],
               (CPU_STK_SIZE  )0,
               (CPU_STK_SIZE  )128,
               (OS_MSG_QTY    )0,
               (OS_TICK       )0,
               (void         *)0,
               (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
               (OS_ERR       *)&err);
  
  OSTaskCreate((OS_TCB       *)&CurrentSmplTask,              // Create CurrentSmplTask
               (CPU_CHAR     *)"Current Sampling Task",
               (OS_TASK_PTR   )CurrentSampler, 
               (void         *)0,
               (OS_PRIO       )5,
               (CPU_STK      *)&CurrentSamplingTaskStk[0],
               (CPU_STK_SIZE  )0,
               (CPU_STK_SIZE  )64,
               (OS_MSG_QTY    )0,
               (OS_TICK       )0,
               (void         *)0,
               (OS_OPT        )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
               (OS_ERR       *)&err);
  
}


// Create application kernel objects tasks (called by AppTaskStart())
static  void  AppObjCreate (void)
{
  
}

/**
 * @brief Task in which cell decides whether to make a balancing request
 * 
 *
 */
void RequestStrategy(void *p_arg)
{
  OS_ERR err;
  OS_TICK timeout_s  = 1;  
  OS_TICK timeout_ms = 0; 
  
  while (DEF_TRUE)
  {
    timeout_s  = random_int()%1;
    timeout_ms = random_int()%1000;
    
    // Delay the task for specified timeout
    OSTimeDlyHMSM(0, 
                  0,
                  timeout_s,
                  timeout_ms,
                  OS_OPT_TIME_HMSM_STRICT, 
                  &err);
    
    if (self.balancingSettings.balancingEnabled)
    {
      ///-- Check for condition of being in need to receive charge (i.e. Cell minimum in voltage list)
      if ( requestChargeCondition() )
      {
        ///- Determine sender
        determineSender(&txTransaction);
        
        ///- In a charge request cell is always receiver
        txTransaction.receiverID = self.id;
        
        // TODO -> Check how much charge is needed and available at sender
        txTransaction.transferRate = 0.2;
        txTransaction.transferTime = 10.0;
        
        ///- Send out request message to determined sender
        SendSendRequest(&txTransaction);
      }
      else
      ///-- Check for condition of being in need to send charge (i.e. Cell maximum in voltage list)
      if ( sendChargeCondition() )
      {        
        ///- Determine receiver
        determineReceiver(&txTransaction);
        
        ///- In a send charge request cell is always sender
        txTransaction.senderID = self.id;
        
        // TODO -> Check how much charge is needed and available at receiver
        txTransaction.transferRate = 0.2;
        txTransaction.transferTime = 10.0;
        
        ///- Send out request to send to acknowledgeTask
        OSTaskQPost(&AcknowledgeTask,
              (void*) &txTransaction,
              sizeof (void*),
              OS_OPT_POST_FIFO,
              &err);
      }
    }
  }
}

/**
 * @brief Task in which cell decides whether to acknowledge a charge request
 * 
 */
void AcknowledgeStrategy(void *p_arg)
{
  transactionInfoType *requestedTransaction;
  OS_MSG_SIZE size;
  CPU_TS ts;
  OS_ERR err;
  while (DEF_TRUE) 
  {
    ///-- Wait for incoming transaction request
    requestedTransaction = OSTaskQPend (0,
                                        OS_OPT_PEND_BLOCKING,
                                        &size,
                                        &ts,
                                        &err);
    {
      ///-- Check for condition of being able to send (i.e. Cell is not blocked)
      if ( acknowledgeSendCondition() )
      {
        ///- Set status to PROCESSING self to prevent interference
        packInfo.status[self.id] = PROCESSING; 
        
        requestedTransaction->transferRate = 0.2;
        requestedTransaction->transferTime = 10;
        
        ///- Forward transaction to BlockCellsTask
        OSTaskQPost(&BlockCellsTask,
                    (void*) requestedTransaction,
                    sizeof (void*),
                    OS_OPT_POST_FIFO,
                    &err);
        
        ///- Wait on BlockCells task to finish...
        OSSemPend(&blockCellsSemaphore,
                  0,
                  OS_OPT_PEND_BLOCKING,
                  &ts,
                  &err);
        
        ///-- Check block status of requestedTransaction:
        if ( (requestedTransaction->blockSuccessful == true) && (self.id == requestedTransaction->senderID) )
        {
          ///-- Blocking successful -> start balancing
          
          ///- cell is about to send -> execute beforeSend() function
          beforeSend();
          
          ///- execute SendSendAcknowledge()
          SendSendAcknowledge(requestedTransaction);
          
          ///- Set SwitchConfig() for sending
          SwitchConfig(packInfo.status[self.id]);
          
          ///- SendCharge() (i.e. PWM pulses)
          SendCharge(packInfo.status[self.id]);
          
          ///- Wait on finishing sending the charge
          OSSemPend(&sendChargeSemaphore,
                    0,
                    OS_OPT_PEND_BLOCKING,
                    &ts,
                    &err);
          
          ///- SendUnblockRequest() to cells participating in the transaction
          SendUnblockRequest(requestedTransaction->senderID, requestedTransaction->receiverID);
          
          ///- Send charge done -> execute afterSend() function
          afterSend();
        }
        else
        {
          ///-- Blocking unsuccessful -> SendUnblockRequest()
          SendUnblockRequest(requestedTransaction->senderID, requestedTransaction->receiverID);
        }
          
        ///- Empty task-queue
        OSTaskQFlush(&AcknowledgeTask, &err);
        if (self.balancingSettings.balancingEnabled)
          
        ///- Empty blockCell queue
        OSTaskQFlush(&BlockCellsTask,
                     &err);
      }
    }
  }
}

/**
 * @brief Task for blocking cells
 * 
 * This task waits for a message in its own queue and executes when a proposed
 * balancing transaction contained in <code>transactionInfo</code> is put into queue.
 * The task includes another queue used for managing the block-responses from cells.
 * This queue is used to check whether the correct cells answered if the block-request
 * to them was successful or not.
 * 
 */
static void BlockCells(void *p_arg)
{ 
  transactionInfoType *requestedTransaction;
  BlockResponseType *BlockResponse;
  OS_MSG_SIZE size;
  OS_ERR err;
  CPU_TS ts;
  OS_TICK timeout = 100;
  Int8U senderID, receiverID;
  
  while (DEF_ON)
  {
    ///-- Pend on task-message-queue
    requestedTransaction = OSTaskQPend (0,
                                        OS_OPT_PEND_BLOCKING,
                                        &size,
                                        &ts,
                                        &err);
    
    ///- Assign senderID and receiverID to requestedTransaction
    senderID   = requestedTransaction->senderID;
    receiverID = requestedTransaction->receiverID;
    
    ///- Get amount of expected responses (i.e. participants in balancing transaction)
    Int8U ResponseAmount = getListLength(senderID, receiverID);
    
    ///- Define blockedList
    Int8U * blockedList;
    ///- Allocate space needed for blocked list
    blockedList = (Int8U*) malloc (ResponseAmount);
    ///- Initialize blockedList with 0xFF as values
    for (Int8U i=0; i<ResponseAmount;i++)
      blockedList[i] = 0xFF;
    
    ///- Declarate and initialize response- and fail counts
    Int8U ResponseCount = 0;
    Int8U FailCount = 0;
    ///- Set starttime needed for timing out
    OS_TICK starttime = OSTimeGet(&err);
    
    ///- As a request for blocking cells comes in, send out a block request
    SendBlockRequest(senderID, receiverID);
    
    /** --While the amount of expected positive blocking responses isn't met and the operation 
        has not timed out, wait for incoming responses on the message queue */
    while ( (ResponseCount < ResponseAmount) && (OSTimeGet(&err)<(starttime+timeout)) )
    {
      BlockResponse = OSQPend(&BlockResponseQ,
                              10,
                              OS_OPT_PEND_BLOCKING,
                              &size,
                              &ts,
                              &err);
      
      if (err == OS_ERR_NONE)
      {
        ///- Check if the response we got is positive and unique (i.e. not in blockedList)
        if ( ///-- Response's origin has to be participating in transaction
            (isInParticipantList(BlockResponse->origin, senderID, receiverID))
             ///-- Response's origin must not be in valid responses list
           &&(!isInList(BlockResponse->origin, blockedList, ResponseAmount))
             ///-- Response's status must match corresponding role in transaction
           &&(BlockResponse->status == getRole(BlockResponse->origin, requestedTransaction))
           )
        {
          blockedList[ResponseCount] = BlockResponse->origin;
          packInfo.status[BlockResponse->origin] = BlockResponse->status;
          ResponseCount++;
        }
      }
      else
      {
        FailCount++;
      }
      free(BlockResponse);
    }
    
    ///- If checking is done and the right amount of nodes responded positively blocking was successful
    if (ResponseCount == ResponseAmount)
    {
      requestedTransaction->blockSuccessful = true;
    }
    else
    {
      requestedTransaction->blockSuccessful = false;
    }
    
    ///-- Flush BlockResponse Message Queue
    OSQFlush(&BlockResponseQ, 
             &err);
    ///-- Free memory allocated in heap
    free(blockedList);
    
    ///-- Post semaphore for AcknowledgeTask to go on
    OSSemPost(&blockCellsSemaphore,
              OS_OPT_POST_1,
              &err);
  }
}


/**
 * @brief Task for processing incoming CAN-messages
 * 
 * This task checks its message queue for CAN-messages and launches the
 * corresponding action to process its type.
 * 
 */
static void  ProcessExtendedCANMsg (void *p_arg)
{
  CanRxMsg *canMsg;
  OS_ERR      err;
  OS_MSG_SIZE size;
  CPU_TS ts;
  
  while (DEF_TRUE) 
  {
    ///--Pend on task-message-queue
    canMsg = OSTaskQPend (0,
                          OS_OPT_PEND_BLOCKING,
                          &size,
                          &ts,
                          &err);
    
    // Message type is in first 2 bytes of extended CANid
    Int16U type = (canMsg->ExtId>>16)&0xFFFF;
    // Message target is 3rd byte in ID
    Int8U target = (canMsg->ExtId>>8)&0xFF;
    // Message origin is 4th byte in ID
    Int8U origin = (canMsg->ExtId)&0xFF;
    
    union floatUnion_t
    {
      float f;
      Int8U bytes[sizeof(float)];
    } floatBytes;

    union longUnion_t
    {
      long L;
      Int8U bytes[sizeof(long)];
    } longBytes;
    
    union shortUnion_t
    {
      Int16U s;
      Int8U bytes[sizeof(short)];
    } shortBytes;
    
    /** -Switch action according to received message_type
        (see [ENUM]<code>MSG_TYPE</code> for details) */
    switch (type)
    {
      case MSG_TYPE_SOC:
        ProcessSOCMessage(canMsg);
      break;
      
      case MSG_TYPE_VOLTAGE:
        ProcessVoltageMessage(canMsg);
      break;
      
      case MSG_TYPE_BLOCK:
        ProcessBlockRequest(canMsg);
      break;
      
      case MSG_TYPE_UNBLOCK:
        ProcessUnblockRequest(canMsg);
      break;
      
      case MSG_TYPE_STATUS_RESPONSE:
        ProcessStatusResponse(canMsg);
      break;
      
      case MSG_TYPE_SEND_REQ:
        ProcessSendRequest(canMsg);
      break;
      
      case MSG_TYPE_SEND_ACK:
        ProcessSendAcknowledge(canMsg);
      break;
      
      case MSG_TYPE_SET_BALANCING:
        if (canMsg->Data[0])
          enableActiveBalancing();
        else
          disableActiveBalancing();
      break;
      
      case MSG_TYPE_SET_MONITOR:
        self.balancingSettings.monitorEnabled = canMsg->Data[0];
      break;
      
      case MSG_TYPE_SET_MONITOR_BOUND:
        for (Int8U counter = 0; counter < 4; counter++)
                floatBytes.bytes[counter] = canMsg->Data[counter];
        self.balancingSettings.startDiff = floatBytes.f;
        for (Int8U counter = 0; counter < 4; counter++)
                floatBytes.bytes[counter] = canMsg->Data[counter+4];
        self.balancingSettings.stopDiff = floatBytes.f;
      break;
      
      case MSG_TYPE_SET_STRATEGY:
        self.balancingSettings.balancingStrategy = canMsg->Data[0];
      break;
      
      case MSG_TYPE_SET_BC_SETTINGS:
        self.broadcastSettings.minBroadcast              = canMsg->Data[0];
        self.broadcastSettings.maxBroadcast              = canMsg->Data[1];
        self.broadcastSettings.allBroadcast              = canMsg->Data[2];
        self.broadcastSettings.broadcastAfterTransaction = canMsg->Data[3];
        self.broadcastSettings.limitBroadcast            = canMsg->Data[4];
        self.broadcastSettings.broadcastDebugStatus      = canMsg->Data[5];
      break;
      
      case MSG_TYPE_SET_BC_PERIOD:
        for (Int8U counter = 0; counter < 4; counter++)
                floatBytes.bytes[counter] = canMsg->Data[counter];
        self.broadcastSettings.broadcastPeriod = floatBytes.f;
      break;
      
      case MSG_TYPE_SET_BC_BOUND:
        for (Int8U counter = 0; counter < 4; counter++)
                floatBytes.bytes[counter] = canMsg->Data[counter];
        self.broadcastSettings.limitBroadcastBound = floatBytes.f;
      break;
      
      case MSG_TYPE_SET_OCV_BALANCING:
        self.balancingSettings.OCVbalancing = canMsg->Data[0];
      break;
        
      case MSG_TYPE_SET_OCV_LATENCY:
        for (Int8U counter = 0; counter < 4; counter++)
                floatBytes.bytes[counter] = canMsg->Data[counter];
        self.balancingSettings.OCVlatency = floatBytes.f;
      break;
      
      case MSG_TYPE_SET_U_TIMER:
        self.timerSettings.uTimerEnabled = canMsg->Data[0];
        if ( self.timerSettings.uTimerEnabled )
          enableUTimer();
        else
          disableUTimer();
      break;
      
      case MSG_TYPE_SET_U_TIMER_VALUES:
        for (Int8U counter = 0; counter < 4; counter++)
                longBytes.bytes[counter] = canMsg->Data[counter];
        self.timerSettings.uFrequency = longBytes.L;
        for (Int8U counter = 0; counter < 4; counter++)
                longBytes.bytes[counter] = canMsg->Data[counter+4];
        self.timerSettings.uFilter = longBytes.L;
        
        setUFrequency(self.timerSettings.uFrequency);
      break;
      
      case MSG_TYPE_SET_I_TIMER:
        self.timerSettings.iTimerEnabled = canMsg->Data[0];
        if ( self.timerSettings.iTimerEnabled )
          enableITimer();
        else
          disableITimer();
      break;
      
      case MSG_TYPE_SET_I_TIMER_VALUES:
        for (Int8U counter = 0; counter < 4; counter++)
                longBytes.bytes[counter] = canMsg->Data[counter];
        self.timerSettings.iFrequency = longBytes.L;
        for (Int8U counter = 0; counter < 4; counter++)
                longBytes.bytes[counter] = canMsg->Data[counter+4];
        self.timerSettings.iFilter = longBytes.L;
        
        setIFrequency(self.timerSettings.iFrequency);
      break;
      
      case MSG_TYPE_SET_KF_TIMER:
        self.timerSettings.kfTimerEnabled = canMsg->Data[0];
        if ( self.timerSettings.kfTimerEnabled )
          enableKFTimer();
        else
          disableKFTimer();            
      break;
      
      case MSG_TYPE_SET_KF_TIMER_VALUES:
        for (Int8U counter = 0; counter < 4; counter++)
                longBytes.bytes[counter] = canMsg->Data[counter];
        self.timerSettings.kfFrequency = longBytes.L;
        for (Int8U counter = 0; counter < 4; counter++)
                longBytes.bytes[counter] = canMsg->Data[counter+4];
        self.timerSettings.kfFilter = longBytes.L;
        
        setKFFrequency(self.timerSettings.kfFrequency);
      break;
      
      case MSG_TIME_SET_PWM_PARAMS:
        for (Int8U counter = 0; counter < 2; counter++)
                shortBytes.bytes[counter] = canMsg->Data[counter];
        self.pwmSettings.frequency = shortBytes.s;
        
        for (Int8U counter = 0; counter < 2; counter++)
                shortBytes.bytes[counter] = canMsg->Data[counter+2];
        self.pwmSettings.numberOfPulses = shortBytes.s;
        
        self.pwmSettings.duration = self.pwmSettings.numberOfPulses / self.pwmSettings.frequency * 1000;
        
        self.pwmSettings.chargePercent    = canMsg->Data[4];
        self.pwmSettings.deadtimePercent  = canMsg->Data[5];
        self.pwmSettings.dischargePercent = canMsg->Data[6];
      break;
      
      case MSG_TYPE_AUTODETECT_RANDOM:
        ProcessInitMessage(canMsg);
      break;
      
      case MSG_TYPE_AUTODETECT_PULSE:
        ProcessPulseSendCommand(canMsg);
      break;
      
      case MSG_TYPE_AUTODETECT_ASSIGN:
        ProcessIDAssignCommand(canMsg);
      break;
      
      
      /*#########################################*/
      /*#####    DEBUG MESSAGE PROCESSING    ####*/
      /*#########################################*/
      
      case MSG_TYPE_DEBUG_SET_SCREEN:
        ScreenToShow = canMsg->Data[0];
      break;
                              
      case MSG_TYPE_DEBUG_SET_STATUS:
        packInfo.status[self.id] = canMsg->Data[0];
      break;
      
      case MSG_TYPE_DEBUG_SET_VOLTAGE:
        for (Int8U counter = 0; counter < 4; counter++)
                floatBytes.bytes[counter] = canMsg->Data[counter];
        packInfo.voltage[self.id] = floatBytes.f;
      break;
                  
      case MSG_TYPE_DEBUG_SET_SOC:
        for (Int8U counter = 0; counter < 4; counter++)
                floatBytes.bytes[counter] = canMsg->Data[counter];
        packInfo.soc[self.id] = floatBytes.f;
      break;
      
      case MSG_TYPE_DEBUG_SET_ID:
        setSelfID(canMsg->Data[0], canMsg->Data[1]);
      break;
      
      case MSG_TYPE_DEBUG_FORCE_SEND:
        txTransaction.senderID = self.id;
        txTransaction.receiverID = origin;
        txTransaction.transferRate = 0.2;
        txTransaction.transferTime = 10.0;
        
        ///- Send out request to send to acknowledgeTask
        OSTaskQPost(&AcknowledgeTask,
              (void*) &txTransaction,
              sizeof (void*),
              OS_OPT_POST_FIFO,
              &err);
      break;
      
      
      
      default:
      break;
    }
  }
}


/**
 * @brief LCD update task
 * 
 */
static void  ScreenRefreshTask (void *p_arg)
{
  OS_ERR err;
  OS_TICK cycle_time = 100;
  while (DEF_TRUE) {
    /// -Overwrite the screen with white color
    BufLCDcolorFill(& ToDrawScreen, 0, 0, 132, 132, 0xfff);
    
    /// -Show the current status sprite
    ShowStatusSprite(packInfo.status[self.id]);
    
    switch (ScreenToShow)
    {
    case 0:
      ShowScreen_0();
    break;
    
    case 1:
      ShowScreen_1();      
    break;
    
    case 2:
      ShowScreen_2();      
    break;
    
    case 3:
      ShowScreen_3();      
    break;
    
    case 4:
      ShowScreen_4();
    break;
    
    case 5:
      ShowScreen_5();
    break;
    
    default:
      ShowScreen_1();
    break; 
    }
    
    /// -finally, call the screen update routine
    BufLCDUpdateScreen(&ToDrawScreen, &MirrorScreen);
    OSTimeDlyHMSM(0, 
                  0,
                  0,
                  cycle_time,
                  OS_OPT_TIME_HMSM_STRICT, 
                  &err);
  }
}

/**
 * @brief Pack monitoring task enables or disables acitve balancing depending on difference in pack voltage
 *
 *
 */
static void MonitorPack (void *p_arg)
{
  OS_ERR err;
  OS_TICK timeout_s  = 2;
  OS_TICK timeout_ms = 0;
  while (DEF_TRUE)
  {
    if (self.balancingSettings.monitorEnabled)
    {
      if ( fabs(max(packInfo.voltage, NUMOFNODES) - min(packInfo.voltage, NUMOFNODES)) > self.balancingSettings.startDiff )
      {
        enableActiveBalancing();
      }
      else
      if ( fabs(max(packInfo.voltage, NUMOFNODES) - min(packInfo.voltage, NUMOFNODES)) < self.balancingSettings.stopDiff )
      {
        disableActiveBalancing();
      }
    }
    OSTimeDlyHMSM(0, 0, timeout_s, timeout_ms,
                  OS_OPT_TIME_HMSM_STRICT, 
                  &err);
  }
}

/**
 * @brief Periodically send voltage and SOC broadcasts
 * 
 */
static void  StatusMessageTask (void *p_arg)
{
  OS_ERR err;
  CPU_INT16U cycle_time_s  = 0;
  CPU_INT32U cycle_time_ms = 250;
  static Int8U initSendCounter;
  while (DEF_TRUE)
  {
    if(ID_AUTODETECT)
    {
#ifndef BOARD_TUM
      ScreenToShow = 3;
      autoInit();
#else
      ScreenToShow = 3;
#endif
    }
    else
    {
      if (packInfo.status[self.id]!=UNDEFINED)
      {
        cycle_time_ms = (unsigned int)(self.broadcastSettings.broadcastPeriod*1000) % 1000;
        cycle_time_s  = (unsigned int)(self.broadcastSettings.broadcastPeriod-cycle_time_ms/1000);
        
        if ( self.broadcastSettings.broadcastDebugStatus)
          SendDebugVoltageMessage();
        
        if (initSendCounter < 50)
        {
          SendVoltageMessage();
          initSendCounter++;
        }
        
        if ( sendBroadcastCondition() )
        {
          SendVoltageMessage();
        }
        else
        {
          
        }
      }
      else
      {
        UserSetID();
      }
    }
    OSTimeDlyHMSM(0, 0, cycle_time_s, cycle_time_ms,
                  OS_OPT_TIME_HMSM_STRICT, 
                  &err);
  }
}

static void  VoltageAverageTask (void *p_arg)
{
#ifndef BOARD_TUM
  OS_ERR err;
  float currentVoltage;
  while (DEF_TRUE) 
  {

    if(ID_AUTODETECT)
    {
        currentVoltage = SPI_read_current();
        currentSum -= currentSum/100;
        currentSum += currentVoltage;
        
        packInfo.voltage[self.id] = currentSum/100;
        OSTimeDly(5,OS_OPT_TIME_DLY, &err);
        // send response over can that pulse has been received
        if (enableCurrent)
        {
        if (packInfo.voltage[self.id]>0.05 && autoInitState>=2)
        {
          neighborTrigger=true;
          
          Int16U msg_type = MSG_TYPE_AUTODETECT_RANDOM;	        // TYPE Autodetect
          Int8U msg_target = 0xFF;                              // Broadcast
          Int8U msg_origin = SystemID;                          // origin is SystemID
          sendCANMessage(msg_type, msg_target, msg_origin, 0, NULL);
          
          OSTimeDlyHMSM(0, 0, secWaitAfterNTrigger, 0, 
                        OS_OPT_TIME_HMSM_STRICT, 
                        &err);
        }
      }
    }
    else
    {      
      OSTimeDlyHMSM(0, 0, 0, 5, 
                    OS_OPT_TIME_HMSM_STRICT, 
                    &err);
    }
  }
#else
  OS_ERR      err;
  OS_MSG_SIZE size;
  CPU_TS ts;
  while (DEF_TRUE)
  {
    OSTimeDlyHMSM(0, 0, 0, 20, 
                  OS_OPT_TIME_HMSM_STRICT, 
                  &err);
    if(ID_AUTODETECT)
    {
      UARTRxMsg *UARTMsg;
      Int8U tempID = 0;
      Int8U role = 0;
      char buf[16];
      OS_TICK timeout_ms  = 250;
      
      role = 0;
      for (Int8U i=0; i<10; i++)
      {
        // UART Direction Detection (0xDD) SEND LEFT
        sendLeft(0xDD);
        sendLeft(0XFF);
        // UART Direction Detection (0xDD) SEND RIGHT
        sendRight(0xDD);
        sendRight(0XFF);
        OSTimeDlyHMSM(0, 0, 0, 200, 
                      OS_OPT_TIME_HMSM_STRICT, 
                      &err);
      }

      UARTMsg = OSQPend(&LeftUARTQ,
                        timeout_ms,
                        OS_OPT_PEND_BLOCKING,
                        &size,
                        &ts,
                        &err);
      
      if (err == OS_ERR_TIMEOUT)
        role = 1;
      else if ( (err == OS_ERR_NONE) && (UARTMsg->data[0] == 0xDD) )
        role = 2;
      
      UARTMsg = OSQPend(&RightUARTQ,
                  timeout_ms,
                  OS_OPT_PEND_BLOCKING,
                  &size,
                  &ts,
                  &err);

      if (err == OS_ERR_TIMEOUT)
        role = 3;
      else if ( (err == OS_ERR_NONE) && (UARTMsg->data[0] == 0xDD) && (role != 1) )
        role = 2;
      
      sprintf( buf, "Getting role: %u", role);
      Position(1,1);
      Displ_String(buf);
      
      OSQFlush(&LeftUARTQ, &err);
      OSQFlush(&RightUARTQ, &err);
      
      while (packInfo.status[self.id] == UNDEFINED)
      {
        if (role == 1)
        {
          tempID = 0;
          sendRight(0xAD);
          sendRight(tempID);
          sendRight(0xFF);
          
          sprintf( buf, "Waiting for >>>>");
          Position(1,2);
          Displ_String(buf);
          
          // tempID is sent, wait for confirmation from right
          UARTMsg = OSQPend(&RightUARTQ,
                      timeout_ms,
                      OS_OPT_PEND_BLOCKING,
                      &size,
                      &ts,
                      &err);
          if ( (err == OS_ERR_NONE) && (UARTMsg->data[0] == 0xAA) && (UARTMsg->data[1] == tempID) )
            setSelfID(tempID, UARTMsg->data[2]);
        }
        
        // Cell is either middle or end of chain -> wait for ID from left
        else
        {
          sprintf( buf, "Waiting for <<<<");
          Position(1,2);
          Displ_String(buf);
          
          UARTMsg = OSQPend(&LeftUARTQ,
                            timeout_ms,
                            OS_OPT_PEND_BLOCKING,
                            &size,
                            &ts,
                            &err);
          // tempID received - add one and send to right
          if ( (err == OS_ERR_NONE) && (UARTMsg->data[0] == 0xAD) )
          {
            tempID = UARTMsg->data[1] + 0x01;
            
            if (role == 3)
            {
              sendLeft(0xAA);
              sendLeft(tempID-0x01);
              sendLeft(tempID+1);
              sendLeft(0xFF);
              setSelfID(tempID, tempID+1);
            }
            else
            {
              sendRight(0xAD);
              sendRight(tempID);
              sendRight(0xFF);
              // tempID is sent, wait for confirmation from right
              
              sprintf( buf, "Waiting for >>>>");
              Position(1,2);
              Displ_String(buf);
              
              UARTMsg = OSQPend(&RightUARTQ,
                          timeout_ms,
                          OS_OPT_PEND_BLOCKING,
                          &size,
                          &ts,
                          &err);
              if ( (err == OS_ERR_NONE) && (UARTMsg->data[0] == 0xAA) && (UARTMsg->data[1] == tempID) )
              {
                sendLeft(0xAA);
                sendLeft(tempID-0x01);
                sendLeft(UARTMsg->data[2]);
                sendLeft(0xFF);
                setSelfID(tempID, UARTMsg->data[2]);
              }
            }
          }
        }
      }
      Clear_display();
    }
  }
#endif
}

matrixType yk;          ///< Measured output vector
matrixType uk;          ///< Measured input vecto
kalmanType *KFM;        ///< Pointer to dataset describing Extended Kalman Filter

/**
 * @brief Current sampling task
 * 
 * This task is momentarily used for initializing the Extended Kalman Filter
 */
static void  CurrentSampler(void *p_arg)
{
  OS_ERR err;
  OS_TICK timeout_s  = 0;
  OS_TICK timeout_ms = 500;
      
  packInfo.soc[self.id] = 0.5;
  self.current = 0;
    
  KFM = (kalmanType*) malloc(sizeof(kalmanType));
  kalman_matrix_init(KFM);
  
  float yk_da[1] = {3.0};
  matrix_init(&yk, 1, 1, yk_da);
  
  float uk_da[1] = {0.0};
  matrix_init(&uk, 1, 1, uk_da);
  
  initKFTimer();
  
  RCC_Configuration();
  GPIOConfiguration();
  USART_Configuration();
  
  while (DEF_ON)
  {
    // Delay the task for specified timeout
    OSTimeDlyHMSM(0, 
                  0,
                  timeout_s,
                  timeout_ms,
                  OS_OPT_TIME_HMSM_STRICT, 
                  &err);
    /*
    if (self.timerSettings.kfTimerEnabled)
    {
      yk.pData[0] = self.voltage;
      uk.pData[0] = 0;
      
      iterate_KF(KFM, &yk, &uk);
      packInfo.soc[self.id] = KFM->x.pData[0];
    }
    */
//    get_yk(&KFM->x, &uk, &yk);
//    self.voltage_est = yk.pData[0];
    
    
    
  }
}