#ifndef board_select_h
#define board_select_h

/************************************************************************
/  Define the used board here
/  Possible boards are:
/
/  BOARD_E407
/       Olimex E407 board (red with onboard ethernet)
/
/  BOARD_P407
/       Olimex P407 board (red with onboard display and ethernet)
/
/  BOARD_IAR
/       IAR Evaluation board
/
/  BOARD_TUM
/       Board designed by Swaminathan Narayanaswamy (green, onboard display)
/
************************************************************************/

#define BOARD_TUM
//#define BOARD_IAR
//#define BOARD_P407
//#define BOARD_E407
//#define HW_SPI


/************************************************************************
/  Define the used architecture here:
/  Possible architectures are:
/
/  ARCH_TRANSFORMER
/       Transformer based active cell balancing board (slim, blue board)
/
/  ARCH_INDUCTOR
/       Inductor based active cell balancing board (big, blue board)
************************************************************************/

#define ARCH_TRANSFORMER
//#define ARCH_INDUCTOR


/***********************************************************************
/  Define the following to avoid blocking cells inbetween transfer cells
/
************************************************************************/

#define NOBLOCKERS

#endif