/*
*********************************************************************************************************
*                                              EXAMPLE CODE
*
*                             (c) Copyright 2013; Micrium, Inc.; Weston, FL
*
*                   All rights reserved.  Protected by international copyright laws.
*                   Knowledge of the source code may not be used to write a similar
*                   product.  This file may only be used in accordance with a license
*                   and should not be redistributed in any way.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                           MASTER INCLUDES
*
*                                     ST Microelectronics STM32
*                                              with the
*
*                                           STM3240G-EVAL
*                                         Evaluation Board
*
* Filename      : includes.h
* Version       : V1.00
* Programmer(s) : EHS
*                 DC
*********************************************************************************************************
*/

#ifndef  INCLUDES_PRESENT
#define  INCLUDES_PRESENT


/*
*********************************************************************************************************
*                                         STANDARD LIBRARIES
*********************************************************************************************************
*/

#include  <stdarg.h>
#include  <stdio.h>
#include  <stdlib.h>
#include  <math.h>


/*
*********************************************************************************************************
*                                              LIBRARIES
*********************************************************************************************************
*/

#include  <cpu.h>
#include  <lib_def.h>
#include  <lib_ascii.h>
#include  <lib_math.h>
#include  <lib_mem.h>
#include  <lib_str.h>


/*
*********************************************************************************************************
*                                                 OS
*********************************************************************************************************
*/

#include  <os.h>


/*
*********************************************************************************************************
*                                              APP / BSP
*********************************************************************************************************
*/

#include  <bsp.h>

// Include board specific (needed for typedefs "Int8U, Int16U...")
#include <includes_STM32-P407.h>

// Include platform specific typedefinitions
#include  <typedef.h>

// Include Communcation Functions
#include  <spi.h>
#include  <can.h>
#include  <daisy_chain.h>

// Include Hardware Functions
#include  <pwm.h>
#include  <switch.h>
#include  <button.h>
#include  <LED.h>
#include  <screen_control.h>

#include  <dogm162.h>


// Include Control Functions
#include  <filter.h>
#include  <charge_rx_tx.h>
#include  <balancing_strategy.h>
#include  <control.h>
#include  <auto_assign.h>
#include  <UI_timer.h>
#include  <soc_kalman_init.h>
#include  <soc_model.h>
#include  <soc_kalman.h>

// Include Helper Funcions
#include  <array_functions.h>
#include  <matrix_operations.h>
#include  <rng.h>



/*
*********************************************************************************************************
*                                               SERIAL
*********************************************************************************************************
*/

#if (APP_CFG_SERIAL_EN == DEF_ENABLED)
#include  <app_serial.h>
#endif


/*
*********************************************************************************************************
*                                               LCD
*********************************************************************************************************
*/
#include <arm_comm.h>
#include <BufLCD6610driver.h>

/*
*********************************************************************************************************
*                                               RESSOURCES
*********************************************************************************************************
*/
#include <battery_icons.h>
#include <status_icons.h>

/*
*********************************************************************************************************
*                                            INCLUDES END
*********************************************************************************************************
*/


#endif

