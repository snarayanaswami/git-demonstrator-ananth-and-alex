/**
 *********************************************************************************************************
 *
 * @file    pwm.c
 * @author  Arne Meeuw
 * @version V0.0.1
 * @date    2-January-2015
 * @brief   Initialization and setting of PWM related timers (TIM4 and TIM9)
 * 
 *********************************************************************************************************
 */ 

#include <pwm.h>
#include <switch.h>

extern Int16U PWMPulseCounter;
extern Int16U PWMPulseCounter2;
extern Int16U PWMCount;
extern Int16U PWMWaitPercent;
extern Int8U PWMTimerChannel;
extern Int8U ID_AUTODETECT;
extern nodeInfoType self;
extern packInfoType packInfo;
extern OS_SEM sendChargeSemaphore;

/* Generate two non-overlapping PWM pulses on pins D12 and E5 with settable duty cycles and dead time */
void OutputPWMPulses(Int32U Freq, Int16U NumberOfPulses, Int8U DutyCyclePercentPWM1, Int8U DutyCyclePercentWaitBetweenPWM1and2, Int8U DutyCyclePercentPWM2, Int8U TimerChannel)
{
  PWMTimerChannel = TimerChannel;
  PWMCount = NumberOfPulses;
  // Equation for timer counter setting: (100-(Dutycycle+10))*(42000/Freq)
  PWMWaitPercent=(100-(DutyCyclePercentPWM1+DutyCyclePercentPWM2+DutyCyclePercentWaitBetweenPWM1and2))*(42000/Freq);
  
  PWM_Timer9_C1_E5_Init(Freq,100-DutyCyclePercentPWM2,2);
  PWM_Timer4_C1_D12_Init(Freq,100-DutyCyclePercentPWM1,2);
}

/* Generate PWM pulses for neighbor detection on pin and E5 (Switch M5) with settable duty cycle and dead time */
void OutputInitPWMPulses(Int32U Freq, Int16U NumberOfPulses, Int8U DutyCyclePercentPWM1, Int8U TimerChannel)
{
  PWMWaitPercent=(100-(100))*(42000/Freq);
  
  PWMTimerChannel=1;
  PWMCount=NumberOfPulses;
  PWM_Timer9_C1_E5_Init(Freq,0,0);
  PWM_Timer4_C1_D12_Init(Freq,100-DutyCyclePercentPWM1,2);
}


/* Initializes the timer for PWM1 */
void PWM_Timer4_C1_D12_Init(Int16U freq, Int8U duty, Int8U pwmMode)
{
  TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
  TIM_OCInitTypeDef               TIM_OCInitStructure;
  GPIO_InitTypeDef                GPIO_InitStructure;
  uint16_t                                period;
  uint16_t                                pulse;
  PWMPulseCounter=0;
  PWMPulseCounter2=0;
  
  /* Compute the value for the ARR register to have a period of 40 KHz */
  period = ( 168000000 / (40 * freq) ) - 0;      
  
  /* Compute the CCR1 value to generate a PWN signal with 50% duty cycle */
  pulse = (uint16_t) (((uint32_t) duty * (period - 1)) / 100);
  
  /* GPIOA clock enable */
  if (pwmMode>0)
  {
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
  }
  /* TIM1 clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4 , ENABLE);
  
  /* Output configuration */
  TIM_OCStructInit(&TIM_OCInitStructure);
  if (pwmMode>0)
  {
    if (pwmMode==1)
    {
      TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    }
    else
    {
      TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
    }
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = pulse;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;
    
    if (PWMTimerChannel==1)
      TIM_OC1Init(TIM4, &TIM_OCInitStructure);  
    else
      TIM_OC2Init(TIM4, &TIM_OCInitStructure);  
  }
  /* Timer Base configuration */
  TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
  TIM_TimeBaseStructure.TIM_Prescaler = 19;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = period;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
  TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);
  
  if (pwmMode>0)
  {
    if (PWMTimerChannel==1)
    {
      /* Initialize PD12, Alternative Function, 100Mhz, Output, Push-pull */
      GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
      GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
      GPIO_Init(GPIOD, &GPIO_InitStructure);
      GPIO_PinAFConfig(GPIOD, GPIO_PinSource12, GPIO_AF_TIM4);
      /* Very much needed.  Enable Preload register on CCR1. */
      TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Enable);
    }
    else
    {
      /* Initialize PD13, Alternative Function, 100Mhz, Output, Push-pull */
      GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
      GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
      GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
      GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
      GPIO_Init(GPIOD, &GPIO_InitStructure);
      GPIO_PinAFConfig(GPIOD, GPIO_PinSource13, GPIO_AF_TIM4);
      /* Very much needed.  Enable Preload register on CCR2. */
      TIM_OC2PreloadConfig(TIM4, TIM_OCPreload_Enable);
    }
  }
  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  
  if (PWMTimerChannel==1)
    TIM_ITConfig(TIM4, TIM_IT_CC1, ENABLE);
  else
    TIM_ITConfig(TIM4, TIM_IT_CC2, ENABLE);
  
  BSP_IntVectSet(BSP_INT_ID_TIM4 , &TIM4_IRQHandler);
  BSP_IntEn(BSP_INT_ID_TIM4);
  
  
  /* TIM1 counter enable */
  TIM_Cmd(TIM4, ENABLE);
  
  if (pwmMode>0)
  {
    /* TIM1 Main Output Enable */
    TIM_CtrlPWMOutputs(TIM4, ENABLE);
  }
  else
  {
    /* TIM1 Main Output Enable */
    TIM_CtrlPWMOutputs(TIM4, DISABLE);
  }
}


/* Initializes the timer for PWM2 */
void PWM_Timer9_C1_E5_Init(Int16U freq, Int8U duty, Int8U pwmMode)
{
  TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
  TIM_OCInitTypeDef               TIM_OCInitStructure;
  GPIO_InitTypeDef                GPIO_InitStructure;
  uint16_t                                period;
  uint16_t                                pulse;
  
  /* Compute the value for the ARR register to have a period of 40 KHz */
  period = ( 168000000 / (40 * freq) ) - 0;      
  
  /* Compute the CCR1 value to generate a PWN signal with 50% duty cycle */
  pulse = (uint16_t) (((uint32_t) duty * (period - 1)) / 100);
  
  if (pwmMode>0)
  {
  /* GPIOA clock enable */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
  }
  
  /* TIM1 clock enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM9 , ENABLE);
  
  /* Channel 1 output configuration */
  TIM_OCStructInit(&TIM_OCInitStructure);
  if (pwmMode==1)
  {
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  }
  else
  {
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
  }
  if (pwmMode>0)
  {
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = pulse;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;
    TIM_OC1Init(TIM9, &TIM_OCInitStructure);
  }
  /* Timer Base configuration */
  TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
  TIM_TimeBaseStructure.TIM_Prescaler = 39;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = period;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
  TIM_TimeBaseInit(TIM9, &TIM_TimeBaseStructure);
  if (pwmMode>0)
  {
#ifndef BOARD_TUM
    /* Initialize PE5, Alternative Function, 100Mhz, Output, Push-pull */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
    GPIO_Init(GPIOE, &GPIO_InitStructure);
#endif
    GPIO_PinAFConfig(GPIOE, GPIO_PinSource5, GPIO_AF_TIM9);
    
    /* Very much needed.  Enable Preload register on CCR1. */
    TIM_OC1PreloadConfig(TIM9, TIM_OCPreload_Enable);
  }
  
  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_InitStructure.NVIC_IRQChannel = TIM1_BRK_TIM9_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  TIM_ITConfig(TIM9, TIM_IT_CC1, ENABLE);
  BSP_IntVectSet(BSP_INT_ID_TIM1_BRK_TIM9 , &TIM9_IRQHandler);
  BSP_IntEn(BSP_INT_ID_TIM1_BRK_TIM9);
}


/* ISR for TIM 4 */
void TIM4_IRQHandler(void)
{
  GPIO_InitTypeDef                GPIO_InitStruct;
  
  PWMPulseCounter++;
  
  
  if (PWMPulseCounter==1) 
  { 
    // Equation for counter: (100-(Dutycycle+10))*42
    TIM_SetCounter(TIM9,PWMWaitPercent);
    TIM_Cmd(TIM9, ENABLE);
    if (!ID_AUTODETECT)
    {
      TIM_CtrlPWMOutputs(TIM9, ENABLE);
    }
  }
  
  if (PWMPulseCounter>=(PWMCount+1)) 
  {  
    TIM_CtrlPWMOutputs(TIM9, DISABLE);
    if (!ID_AUTODETECT)
    {
//#ifndef BOARD_TUM
      GPIO_InitStruct.GPIO_Pin = GPIO_Pin_5;
      GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
      GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
      GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
      GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
      GPIO_Init(GPIOE, &GPIO_InitStruct);
      
      
      GPIOE->BSRRH |= GPIO_Pin_5; // set PE5 low
//#endif
    }
  }
  
  if (PWMTimerChannel==1)
    TIM_ClearITPendingBit(TIM4, TIM_IT_CC1);
  else
    TIM_ClearITPendingBit(TIM4, TIM_IT_CC2);
}


/* ISR for TIM 9 */
void TIM9_IRQHandler(void)
{
  OS_ERR err;
  GPIO_InitTypeDef                GPIO_InitStruct;
  
  PWMPulseCounter2++;
  
  if (PWMPulseCounter2==PWMCount) 
  {  
    TIM_CtrlPWMOutputs(TIM4, DISABLE);
    
      if (PWMTimerChannel==1)
      {
        GPIO_InitStruct.GPIO_Pin = GPIO_Pin_12;
        GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
        GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
        GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
        GPIO_Init(GPIOD, &GPIO_InitStruct);
        
        GPIOD->BSRRH |= GPIO_Pin_12; // set PD12 low
      }
      else
      {
        GPIO_InitStruct.GPIO_Pin = GPIO_Pin_13;
        GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
        GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
        GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
        GPIO_Init(GPIOD, &GPIO_InitStruct);
        
        GPIOD->BSRRH |= GPIO_Pin_13; // set PD13 low
      }
    
  }
  if (PWMPulseCounter2>=(PWMCount+1)) 
  {  
    
    if (PWMTimerChannel==1)
      TIM_ITConfig(TIM4, TIM_IT_CC1, DISABLE);
    else
      TIM_ITConfig(TIM4, TIM_IT_CC2, DISABLE);
    TIM_Cmd(TIM4, DISABLE); 
    TIM_Cmd(TIM9, DISABLE); 
    
     // set status to OFF after PWMs have ended if status was not UNDEFINED
    if(!ID_AUTODETECT)
    //SwitchConfig(OFF);
      OSSemPost(&sendChargeSemaphore,
                OS_OPT_POST_1,
                &err);
    
//        if (ID_AUTODETECT)
//        {  
//          TIM_CtrlPWMOutputs(TIM9, DISABLE);
//          
//          GPIO_InitStruct.GPIO_Pin = GPIO_Pin_5;
//          GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
//          GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
//          GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
//          GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
//          GPIO_Init(GPIOE, &GPIO_InitStruct);
//          
//          
//          GPIOE->BSRRH |= GPIO_Pin_5; // set PE5 low
//        }
    
    if (packInfo.status[self.id] != UNDEFINED)
    {
      packInfo.status[self.id] = OFF;
    }
  }
  
  TIM_ClearITPendingBit(TIM9, TIM_IT_CC1);
}