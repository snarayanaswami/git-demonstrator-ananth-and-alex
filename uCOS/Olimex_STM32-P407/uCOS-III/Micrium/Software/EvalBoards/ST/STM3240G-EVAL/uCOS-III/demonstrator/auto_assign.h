#ifndef auto_assign_h
#define auto_assign_h

#include <includes.h>

/**
 * @brief Node in List used for auto-assigning of IDs
 *
 */
struct listNode
{
    Int8U ID;
    struct listNode *next;
};

void autoInit(void);
void ProcessInitMessage(CanRxMsg *canMsg);
bool IDinList(struct listNode * List, Int8U searchID);
void MergeLeftIntoRight(void);
void PulseSendCommand(Int8U cellID);
void IDAssignCommand(Int8U tempID, Int8U realID);

void ProcessPulseSendCommand(CanRxMsg *canMsg);
void ProcessIDAssignCommand(CanRxMsg *canMsg);

void UserSetID(void);

void setSelfID(Int8U selfID, Int8U numnodes);

#endif