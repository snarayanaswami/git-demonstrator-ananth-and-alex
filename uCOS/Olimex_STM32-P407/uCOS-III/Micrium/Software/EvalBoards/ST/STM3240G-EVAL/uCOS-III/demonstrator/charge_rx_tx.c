/**
 *********************************************************************************************************
 *
 * @file    charge_rx_tx.c
 * @author  Arne Meeuw
 * @version V0.0.1
 * @date    2-January-2015
 * @brief   Initialization and start of PWM procedure for sending and receiving charge
 * 
 *********************************************************************************************************
 */ 

#include <charge_rx_tx.h>

extern nodeInfoType self;
extern packInfoType packInfo;

extern Int8U enableCurrent;

/**
 * @brief Send charge in specified direction
 * 
 * @param[in]   receiverID      receiving cell's ID
 * @param[out]  none            
 *
 */
void SendCharge(Int8U status)
{  
#ifndef BOARD_TUM
  switch(status)
  {
    case SEND_DOWN:
    // SEND_DOWN
    OutputPWMPulses(/*Int32U Freq*/ self.pwmSettings.frequency, 
                  /*Int16U NumberOfPulses*/ self.pwmSettings.numberOfPulses, 
                  /*Int8U DutyCyclePercentPWM1*/ self.pwmSettings.duration, 
                  /*Int8U DutyCyclePercentWaitBetweenPWM1and2*/ self.pwmSettings.deadtimePercent, 
                  /*Int8U DutyCyclePercentPWM2*/ self.pwmSettings.dischargePercent,
                  /*Int8U TimerChannel*/ 1);
    break;
    case SEND_UP:
    // SEND_UP
    OutputPWMPulses(/*Int32U Freq*/ self.pwmSettings.frequency, 
                  /*Int16U NumberOfPulses*/ self.pwmSettings.numberOfPulses, 
                  /*Int8U DutyCyclePercentPWM1*/ self.pwmSettings.duration, 
                  /*Int8U DutyCyclePercentWaitBetweenPWM1and2*/ self.pwmSettings.deadtimePercent, 
                  /*Int8U DutyCyclePercentPWM2*/ self.pwmSettings.dischargePercent,
                    /*Int8U TimerChannel*/ 2);
    break;
  }
#else
    OutputPWMPulses(/*Int32U Freq*/ self.pwmSettings.frequency, 
                  /*Int16U NumberOfPulses*/ self.pwmSettings.numberOfPulses, 
                  /*Int8U DutyCyclePercentPWM1*/ self.pwmSettings.duration, 
                  /*Int8U DutyCyclePercentWaitBetweenPWM1and2*/ self.pwmSettings.deadtimePercent, 
                  /*Int8U DutyCyclePercentPWM2*/ self.pwmSettings.dischargePercent,
                    /*Int8U TimerChannel*/ 1);
#endif
}

void SendPulseDown(void)
{
  enableCurrent=false;
  OS_ERR err;
  SwitchConfig(INITSEND);
  // wait 100 ms
  OSTimeDlyHMSM(0, 0, 0, 100, 
                OS_OPT_TIME_HMSM_STRICT, 
                &err);  
  
  OutputInitPWMPulses(/*Int32U Freq*/ self.pwmSettings.frequency, 
                      /*Int16U NumberOfPulses*/ 500, 
                      /*Int8U DutyCyclePercentPWM1*/ 50, 
                      /*Int8U TimerChannel*/ 1);
  OSTimeDlyHMSM(0, 0, 0, 500, 
                OS_OPT_TIME_HMSM_STRICT, 
                &err);  
  SwitchConfig(INITRECEIVE);
  enableCurrent=true;
}

/**
 * @brief Receive charge from specified senderID
 * 
 * @param[in]   senderID      sending cell's ID
 * @param[out]  none     
 *
 */
void ReceiveCharge(void)
{
  //SwitchConfig(packInfo.status[self.id]);
}

/**
 * @brief Set switches to idle mode (<code>OFF</code>) and set cell's status to (<code>OFF</code>)
 *            
 */
void GotoIdleMode(void)
{
  // all switches OFF
  SwitchConfig(OFF);
  // set status to OFF
  packInfo.status[self.id]=OFF;
}

/**
 * @brief Set switches to idle mode (<code>OFF</code>) and set cell's status to (<code>OFF</code>)
 * 
 * @param[in]   *transaction    Pointer to struct containing information about proposed balancing transaction
 * @param[out]  status          Resulting role in balancing transaction
 */
Int8U getRole(Int8U ID, transactionInfoType * transaction)
{
  Int8U status = 0;
   
  /// Check if direction is <code>UP (senderID>receiverID)</code>
  if (transaction->senderID > transaction->receiverID)
  {
    /// Cell is sender -> <code>status = SEND_UP</code>
    if (ID == transaction->senderID)
      status = SEND_UP;
    /// Cell is receiver -> <code>status = RECEIVE_DOWN</code>
    else if (ID == transaction->receiverID)
      status = RECEIVE_DOWN;
    /// Cell is blocked from left -> <code>status = BLOCKED</code>
    else if (ID == transaction->senderID + 1)
      status = BLOCKED;
    /// Cell is blocked from right -> <code>status = BLOCKED</code>
    else if (ID == transaction->receiverID - 1)
      status = BLOCKED;
    /// Cell is LOOP-opener -> <code>status = FWD_START</code>
    else if (ID == transaction->senderID - 1)
      status = FWD_MID;
    /// Cell is forwarder -> <code>status = FWD_MID</code>
    else if ( (ID > transaction->receiverID + 1) && (ID < transaction->senderID - 1) )
      status = FWD_MID;
    /// Cell is LOOP-closer -> <code>status = FWD_END</code>
    else if (ID == transaction->receiverID + 1)
      status = FWD_MID;
    else
    /// Cell is not part of balancing transaction -> <code>status = OFF</code>
      status = OFF;
  }
  /// Direction is <code>DOWN (senderID<receiverID)</code>
  else
  {
    /// Cell is sender -> <code>status = SEND_DOWN</code>
    if (ID == transaction->senderID)
      status = SEND_DOWN;
    /// Cell is receiver -> <code>status = RECEIVE_UP</code>
    else if (ID == transaction->receiverID)
      status = RECEIVE_UP;
    /// Cell is blocked from right -> <code>status = BLOCKED</code>
    else if (ID == transaction->senderID - 1)
      status = BLOCKED;
    /// Cell is blocked from left -> <code>status = BLOCKED</code>
    else if (ID == transaction->receiverID + 1)
      status = BLOCKED;
    /// Cell is LOOP-opener -> <code>status = FWD_START</code>
    else if (ID == transaction->senderID + 1)
      status = FWD_MID;
    /// Cell is forwarder -> <code>status = FWD_MID</code>
    else if ( (ID > transaction->senderID + 1) && (ID < transaction->receiverID - 1) )
      status = FWD_MID;
    /// Cell is LOOP-closer -> <code>status = FWD_END</code>
    else if (ID == transaction->receiverID - 1)
      status = FWD_MID;
    else
    /// Cell is not part of balancing transaction -> <code>status = OFF</code>
      status = OFF;
  }
  
  return status;
}