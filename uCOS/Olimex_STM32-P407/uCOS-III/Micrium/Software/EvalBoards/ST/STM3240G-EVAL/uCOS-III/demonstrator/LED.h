#ifndef LED_h
#define LED_h

#include <includes.h>
#ifndef BOARD_TUM
/* LED RED */
#define LED_RED
#define LED_RED_Port         GPIOE
//#define LED_RED_Pin          17
#define LED_RED_Pin          GPIO_Pin_12
#define LED_RED_Clock        RCC_AHB1Periph_GPIOE

/* LED GREEN */
#define LED_GREEN
#define LED_GREEN_Port       GPIOE
//#define LED_GREEN_Pin        18
#define LED_GREEN_Pin        GPIO_Pin_13
#define LED_GREEN_Clock      RCC_AHB1Periph_GPIOE

/* LED YELLOW */
#define LED_YELLOW
#define LED_YELLOW_Port      GPIOE
//#define LED_YELLOW_Pin       19
#define LED_YELLOW_Pin       GPIO_Pin_14
#define LED_YELLOW_Clock     RCC_AHB1Periph_GPIOE

/* LED ORANGE */
#define LED_ORANGE
#define LED_ORANGE_Port      GPIOE
//#define LED_ORANGE_Pin       20
#define LED_ORANGE_Pin       GPIO_Pin_15
#define LED_ORANGE_Clock     RCC_AHB1Periph_GPIOE

#else

/* LED RED */
#define LED_RED
#define LED_RED_Port         GPIOA
//#define LED_RED_Pin          11
#define LED_RED_Pin          GPIO_Pin_11
#define LED_RED_Clock        RCC_AHB1Periph_GPIOA

/* LED GREEN */
#define LED_GREEN
#define LED_GREEN_Port       GPIOB
//#define LED_GREEN_Pin        7
#define LED_GREEN_Pin        GPIO_Pin_7
#define LED_GREEN_Clock      RCC_AHB1Periph_GPIOB

/* LED YELLOW */
#define LED_YELLOW
#define LED_YELLOW_Port      GPIOE
//#define LED_YELLOW_Pin       14
#define LED_YELLOW_Pin       GPIO_Pin_14
#define LED_YELLOW_Clock     RCC_AHB1Periph_GPIOE

/* LED ORANGE */
#define LED_ORANGE
#define LED_ORANGE_Port      GPIOE
//#define LED_ORANGE_Pin       15
#define LED_ORANGE_Pin       GPIO_Pin_15
#define LED_ORANGE_Clock     RCC_AHB1Periph_GPIOE
#endif


typedef enum ledcolor{ RED, 	/* 0 */
                       GREEN, 	/* 1 */ 
                       YELLOW, 	/* 2 */
                       ORANGE 	/* 3 */
}  LEDCOLOR;

typedef enum ledstatus{ LEDOFF, /* 0 */
                        LEDON 	/* 1 */                       
}  LEDSTATUS;


void BoardLEDInit(void);
void BoardLED(Int8U ledColor, bool isOn);

#endif