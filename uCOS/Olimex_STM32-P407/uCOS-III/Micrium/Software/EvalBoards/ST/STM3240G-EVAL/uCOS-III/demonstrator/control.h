#ifndef control_h
#define control_h

#include <includes.h>
#include <float.h>

//#define MOCK_VOLTAGE


void ControlInit(void);


void SendSOCMessage(void);
void SendDebugSOCMessage(void);
void ProcessSOCMessage(CanRxMsg *canMsg);
void SendVoltageMessage(void);
void SendDebugVoltageMessage(void);
void ProcessVoltageMessage(CanRxMsg *canMsg);


void SendSendRequest(transactionInfoType *RequestMessage);
void ProcessSendRequest(CanRxMsg *canMsg);
void SendSendAcknowledge(transactionInfoType *transactionInfo);
void ProcessSendAcknowledge(CanRxMsg *canMsg);

void SendBlockRequest(Int8U senderID, Int8U receiverID);
void ProcessBlockRequest(CanRxMsg *canMsg);
void BlockSelf(idType senderID, idType receiverID);

void SendUnblockRequest(Int8U senderID, Int8U receiverID);
void ProcessUnblockRequest(CanRxMsg *canMsg);
void UnblockSelf(idType senderID, idType receiverID);

void SendStatusResponse(Int8U targetID, Int8U status, Int8U * blockerID);
void ProcessStatusResponse(CanRxMsg *canMsg);

#endif