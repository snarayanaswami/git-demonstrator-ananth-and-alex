/**
 *********************************************************************************************************
 *
 * @file    soc_kalman_init.c
 * @author  Arne Meeuw
 * @version V0.0.1
 * @date    2-January-2015
 * @brief   Initialization function of 'Extended Kalman Filter'
 * 
 *********************************************************************************************************
 */ 

#include <soc_kalman_init.h>

/**
 * @brief Initialization function for kalman filter
 * 
 * @param[in,out]      *KFM      Pointer to dataset describing Extended Kalman Filter
 * @return              none
 */
void kalman_matrix_init(kalmanType * KFM)
{
  /**
   *
   *       Initialization of Kalman Filter Parameters
   *
   *  |Parameter  | Size (m x n)   |         Name                 |
   *  |:---------:|:--------------:|:----------------------------:|
   *  |    x      |   R^ (n x 1)   | state vector                 |
   *  |    y      |   R^ (q x 1)   | output vector                |
   *  |    u      |   R^ (p x 1)   | input vector                 |
   *  |    A      |   R^ (n x n)   | system matrix                |
   *  |    B      |   R^ (n x p)   | input matrix                 |
   *  |    C      |   R^ (q x n)   | output matrix                |
   *  |    D      |   R^ (q x p)   | feedforward matrix           |
   *  |    Sk     |   R^ (n x n)   | estimation error covariance  |
   *  |    Sv     |   R^ (q x q)   | process noise covariance     |
   *  |    Sw     |   R^ (n x n)   | measurement noise covariance |
   *
   */

  Int16U n = 1;
  Int16U q = 1;
  Int16U p = 1;

  KFM->n = n;   // n -> Size of state vector
  KFM->p = p;   // p -> Size of input vector
  KFM->q = q;   // q -> Size of output vector

  // MATRIX x0 DATA
  KFM->x.numRows = n;
  KFM->x.numCols = 1;
  KFM->x.pData = (float*) malloc(sizeof(float)*n);
  float x011 = 0.90;
  float x0_da[1] =
  {
    x011
  };
  // Copy local function data from stack to heap
  memcpy(KFM->x.pData, x0_da, (n * 1) * sizeof(float));

  // MATRIX u0 DATA
  KFM->uk.numRows = p;
  KFM->uk.numCols = 1;
  KFM->uk.pData = (float*) malloc(sizeof(float)*p);
  float u011 = 0.1;
  float u0_da[1] =
  {
    u011
  };
  // Copy local function data from stack to heap
  memcpy(KFM->uk.pData, u0_da, (p * 1) * sizeof(float));

  // MATRIX Ak DATA
  KFM->Ak.numRows = n;
  KFM->Ak.numCols = n;
  KFM->Ak.pData = (float*) malloc((n * n) * sizeof(float));
  float A011 = 1.0;
  float A0_da[1] =
  {
    A011
  };
  // Copy local function data from stack to heap
  memcpy(KFM->Ak.pData, A0_da, ((n * n) * sizeof(float)));

  // MATRIX Ck DATA
  KFM->Ck.numRows = n;
  KFM->Ck.numCols = q;
  KFM->Ck.pData = (float*) malloc((q * n) * sizeof(float));
  float C011 = 1.0;
  float C0_da[1] =
  {
    C011
  };
  // Copy local function data from stack to heap
  memcpy(KFM->Ck.pData, C0_da, ((q * n) * sizeof(float)));


  // MATRIX Sk DATA
  KFM->Sk.numRows = n;
  KFM->Sk.numCols = n;
  KFM->Sk.pData = (float*) malloc((n * n) * sizeof(float));
  float Sk11 = 0.1;
  float Sk_da[1] =
  {
    Sk11
  };
  // Copy local function data from stack to heap
  memcpy(KFM->Sk.pData, Sk_da, ((n * n) * sizeof(float)));

  // MATRIX Sv DATA
  KFM->Sv.numRows = n;
  KFM->Sv.numCols = n;
  KFM->Sv.pData = (float*) malloc((n * n) * sizeof(float));
  float Sv11 = 0.1;
  float Sv_da[1] =
  {
    Sv11
  };
  // Copy local function data from stack to heap
  memcpy(KFM->Sv.pData, Sv_da, ((n * n) * sizeof(float)));

  // MATRIX sw DATA
  KFM->Sw.numRows = n;
  KFM->Sw.numCols = n;
  KFM->Sw.pData = (float*) malloc((n * n) * sizeof(float));
  float Sw11 = 0.1;
  float Sw_da[1] =
  {
    Sw11
  };
  // Copy local function data from stack to heap
  memcpy(KFM->Sw.pData, Sw_da, ((n * n) * sizeof(float)));
}