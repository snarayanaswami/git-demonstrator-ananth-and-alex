/**
 *********************************************************************************************************
 *
 * @file    can.c
 * @author  Arne Meeuw
 * @version V0.0.1
 * @date    2-January-2015
 * @brief   Initialization and interrupt handling for Controller Area Network
 *  
 *********************************************************************************************************
 */ 

#include  <can.h>

extern nodeInfoType self;       ///< Cell information
extern CanRxMsg RxMessage;      ///< Received CAN-Message
extern OS_TCB ExtendedCANTask;  ///< Used for posting into ProcessExtendedCANMsg's message queue

/**
 * @brief Initilialization of CAN1
 * 
 * @param[in]   kbps    CAN-Speed in kilobit per second
 * @return      none
 */
void CAN1_Init(Int16U kbps)
{     
  CAN_InitTypeDef CAN_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;

#if defined(BOARD_E407) || defined(BOARD_TUM)
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
  GPIO_PinAFConfig(GPIOD, GPIO_PinSource0, GPIO_AF_CAN1); 
  GPIO_PinAFConfig(GPIOD, GPIO_PinSource1, GPIO_AF_CAN1); 


  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOD, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOD, &GPIO_InitStructure);
#endif

#ifdef  BOARD_P407
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_CAN1); 
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_CAN1); 


  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
#endif

#ifdef  BOARD_IAR
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_CAN1); 
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_CAN1); 


  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
#endif

  CAN_InitStructure.CAN_SJW = CAN_SJW_1tq;      // synchronization jump width = 1
  CAN_InitStructure.CAN_BS1 = CAN_BS1_14tq;     //14
  CAN_InitStructure.CAN_BS2 = CAN_BS2_6tq;      //6
  CAN_InitStructure.CAN_Prescaler = 2000/kbps;  // 2000 divided by bitrate is prescaler
                                                //baudrate 1000 kbps
  CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
  CAN_InitStructure.CAN_TTCM = DISABLE;
  CAN_InitStructure.CAN_ABOM = DISABLE;
  CAN_InitStructure.CAN_AWUM = DISABLE;
  CAN_InitStructure.CAN_NART = ENABLE;
  CAN_InitStructure.CAN_RFLM = DISABLE;
  CAN_InitStructure.CAN_TXFP = DISABLE;
  CAN_Init(CAN1, &CAN_InitStructure);


  // Initialize Filter settings to let every message pass
  CAN_FilterInitTypeDef CAN_FilterInitStructure;
  CAN_FilterInitStructure.CAN_FilterNumber = 0;
  CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
  CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
  CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
  CAN_FilterInitStructure.CAN_FilterMaskIdLow  = 0x0000;
  CAN_FilterInitStructure.CAN_FilterIdHigh     = 0x0000;
  CAN_FilterInitStructure.CAN_FilterIdLow      = 0x0000;
  CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_FIFO0;
  CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
  CAN_FilterInit(&CAN_FilterInitStructure);

  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_InitStructure.NVIC_IRQChannel = CAN1_RX0_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  BSP_IntVectSet(BSP_INT_ID_CAN1_RX0, &CAN1_RX0_IRQHandler);
  BSP_IntEn(BSP_INT_ID_CAN1_RX0);

  CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);
}


/**
 * @brief Set CAN-filter
 * 
 * This function sets the hardware CAN-filter to ignore all messages except
 * direct messages to own ID and broadcasts.
 * 
 * @param[in]   ownID   ID to set filtering to
 * 
 */
void CAN1_Set_Filter(Int8U ownID)
{
  CAN_FilterInitTypeDef CAN_FilterInitStructure;
  CAN_FilterInitStructure.CAN_FilterNumber = 0;
  CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
  CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;

  // Target value is important: Set filtermask to target-bits
  // Value needs to be shifted left 3 bits, CANextID <> 29 bits
  Int32U MaskID = 0x0000FF00 << 3;
  CAN_FilterInitStructure.CAN_FilterMaskIdHigh = (MaskID >> 16) & 0xFFFF;
  CAN_FilterInitStructure.CAN_FilterMaskIdLow  = (MaskID      ) & 0xFFFF;

  // Filtersetting for broadcasts (target = 0xFF)
  Int32U FilterID = (0x00000000 | (0xFF << 8)) << 3;
  CAN_FilterInitStructure.CAN_FilterIdHigh     = (FilterID >> 16) & 0xFFFF;
  CAN_FilterInitStructure.CAN_FilterIdLow      = (FilterID      ) & 0xFFFF;

  CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_FIFO0;
  CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
  CAN_FilterInit(&CAN_FilterInitStructure);

  CAN_FilterInitStructure.CAN_FilterNumber = 1;
  
  // Filtersetting for broadcasts (target = ownID)
  FilterID = (0x00000000 | (ownID << 8)) << 3;
  CAN_FilterInitStructure.CAN_FilterIdHigh     = (FilterID >> 16) & 0xFFFF;
  CAN_FilterInitStructure.CAN_FilterIdLow      = (FilterID      ) & 0xFFFF;

  CAN_FilterInit(&CAN_FilterInitStructure);
}

/**
 * @brief CAN interrupt handler
 * 
 * Forwards messages received in queue 0 of CAN1 to message queue of ProcessExtendedMessages-task.
 */
void CAN1_RX0_IRQHandler(void)
{
  OS_ERR err;
  CAN_Receive(CAN1, CAN_FIFO0, &RxMessage);
  // send CAN message to Task-Message-Queue of ProcessExtendedMessage()
  OSTaskQPost(&ExtendedCANTask,
              (void          *)       &RxMessage,
              (OS_MSG_SIZE    )       sizeof (void*),
              (OS_OPT         )       OS_OPT_POST_FIFO,
              (OS_ERR        *)       &err);
}



/**
 * @brief Send extended message
 * 
 * Send an extended message by specifying type, target, origin, length and value array
 * 
 * @param[in]   type    type of message according to MSG_TYPE enum
 * @param[in]   target  ID (0-255) of target cell
 * @param[in]   origin  ID (0-255) of origin cell
 * @param[in]   DLC     Length (i.e. amount of bytes) of message
 * @param[in]   data    Array with data bytes
 *
 */
void sendCANMessage(Int16U type, idType target, idType origin, Int8U DLC, Int8U data[])
{
  CanTxMsg canMessage;			
  canMessage.StdId = 0;
  canMessage.ExtId = (type<<16) | (target<<8) | (origin);
  canMessage.RTR = CAN_RTR_DATA;
  canMessage.IDE = CAN_ID_EXT;
  canMessage.DLC = DLC;

  for (Int8U i = 0; i < DLC; i++)
  {
    canMessage.Data[i] = data[i];
  }
  while(!(CAN1->TSR & CAN_TSR_TME0 || CAN1->TSR & CAN_TSR_TME1 || CAN1->TSR & CAN_TSR_TME2))
  {
  }
  CAN_Transmit(CAN1, &canMessage);
}