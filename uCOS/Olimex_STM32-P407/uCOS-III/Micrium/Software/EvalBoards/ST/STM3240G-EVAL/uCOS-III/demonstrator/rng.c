/**
 *********************************************************************************************************
 *
 * @file    rng.c
 * @author  Arne Meeuw
 * @version V0.0.1
 * @date    2-January-2015
 * @brief   Random number generator
 * 
 *********************************************************************************************************
 */ 

#include <rng.h>

/**
 * @brief Generate random 32bit integer value
 * 
 * @param[in]   none
 * @return      32bit random integer value
 */
Int32U random_int(void)
{
  int returnint;
  /** Wait until one RNG number is ready */
  while(RNG_GetFlagStatus(RNG_FLAG_DRDY)== RESET)
  {

  }
  /** Get a 32bit Random number */
  returnint = RNG_GetRandomNumber();
  return returnint;
}

/**
 * @brief Initialize random number generator
 *  
 */
void RNG_Init(void)
{
/** Enable RNG clock source */
RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_RNG, ENABLE);

/** RNG Peripheral enable */
RNG_Cmd(ENABLE);
}

