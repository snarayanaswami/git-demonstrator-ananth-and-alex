/**
 *********************************************************************************************************
 *
 * @file    switch.c
 * @author  Arne Meeuw
 * @version V0.0.1
 * @date    2-January-2015
 * @brief   Initialization and setting of switches for active balancing
 *  
 *********************************************************************************************************
 */ 

#include <switch.h>

void SwitchInit(void)
{
  GPIO_InitTypeDef                GPIO_InitStruct;
  
#ifdef Switch00
  RCC_AHB1PeriphClockCmd(Switch00Clock, ENABLE);
  GPIO_InitStruct.GPIO_Pin = Switch00Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(Switch00Port, &GPIO_InitStruct);    
#endif
  
#ifdef Switch01
  RCC_AHB1PeriphClockCmd(Switch01Clock, ENABLE);
  GPIO_InitStruct.GPIO_Pin = Switch01Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(Switch01Port, &GPIO_InitStruct);    
#endif
  
#ifdef Switch02
  RCC_AHB1PeriphClockCmd(Switch02Clock, ENABLE);
  GPIO_InitStruct.GPIO_Pin = Switch02Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(Switch02Port, &GPIO_InitStruct);    
#endif    
  
#ifdef Switch03
  RCC_AHB1PeriphClockCmd(Switch03Clock, ENABLE);
  GPIO_InitStruct.GPIO_Pin = Switch03Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(Switch03Port, &GPIO_InitStruct);    
#endif
  
#ifdef Switch04
  RCC_AHB1PeriphClockCmd(Switch04Clock, ENABLE);
  GPIO_InitStruct.GPIO_Pin = Switch04Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(Switch04Port, &GPIO_InitStruct);    
#endif
  
#ifdef Switch05
  RCC_AHB1PeriphClockCmd(Switch05Clock, ENABLE);
  GPIO_InitStruct.GPIO_Pin = Switch05Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(Switch05Port, &GPIO_InitStruct);    
#endif  
  
#ifdef Switch06
  RCC_AHB1PeriphClockCmd(Switch06Clock, ENABLE);
  GPIO_InitStruct.GPIO_Pin = Switch06Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(Switch06Port, &GPIO_InitStruct);    
#endif  
  
#ifdef Switch07
  RCC_AHB1PeriphClockCmd(Switch07Clock, ENABLE);
  GPIO_InitStruct.GPIO_Pin = Switch07Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(Switch07Port, &GPIO_InitStruct);    
#endif  
  
#ifdef Switch08
  RCC_AHB1PeriphClockCmd(Switch08Clock, ENABLE);
  GPIO_InitStruct.GPIO_Pin = Switch08Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(Switch08Port, &GPIO_InitStruct);    
#endif  
  
#ifdef Switch09
  RCC_AHB1PeriphClockCmd(Switch09Clock, ENABLE);
  GPIO_InitStruct.GPIO_Pin = Switch09Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(Switch09Port, &GPIO_InitStruct);    
#endif  
  
#ifdef Switch10
  RCC_AHB1PeriphClockCmd(Switch10Clock, ENABLE);
  GPIO_InitStruct.GPIO_Pin = Switch10Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(Switch10Port, &GPIO_InitStruct);    
#endif  
  
#ifdef Switch11
  RCC_AHB1PeriphClockCmd(Switch11Clock, ENABLE);
  GPIO_InitStruct.GPIO_Pin = Switch11Pin;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(Switch11Port, &GPIO_InitStruct);    
#endif  
}

void SwitchConfig(Int8U mode)
{  
#ifdef ARCH_INDUCTOR
  switch(mode)
  {
  case OFF:
    Switch(bin(0,0,0,0,0,0,0,0,0,0,0,0));  
    BoardLED(YELLOW,LEDON);
    BoardLED(GREEN,LEDOFF);
    BoardLED(RED,LEDOFF);
    BoardLED(ORANGE,LEDOFF);
    break;
  case SEND_DOWN:
    Switch(bin(1,0,1,1,1,1,0,0,0,1,0,0));   
    BoardLED(YELLOW,LEDOFF);
    BoardLED(GREEN,LEDON);
    BoardLED(RED,LEDOFF);
    BoardLED(ORANGE,LEDOFF);
    break;
  case SEND_UP:
    Switch(bin(1,1,0,0,0,1,1,1,0,1,0,0));  
    BoardLED(YELLOW,LEDOFF);
    BoardLED(GREEN,LEDON);
    BoardLED(RED,LEDOFF);
    BoardLED(ORANGE,LEDOFF);
    break;
  case RECEIVE_UP:
    Switch(bin(1,1,0,0,1,1,1,1,1,0,0,0));  
    BoardLED(YELLOW,LEDOFF);
    BoardLED(GREEN,LEDOFF);
    BoardLED(RED,LEDON);
    BoardLED(ORANGE,LEDOFF);
    break;
  case RECEIVE_DOWN:
    Switch(bin(1,1,1,1,1,1,0,0,1,0,0,0)); 
    BoardLED(YELLOW,LEDOFF);
    BoardLED(GREEN,LEDOFF);
    BoardLED(RED,LEDON);
    BoardLED(ORANGE,LEDOFF);
    break;
  case INITSEND:
            // M M M M M M M M M M M M 
            // 1 1 2 2 3 3 4 4 5 6 7 7
            // a b a b a b a b     a b
    Switch(bin(1,0,1,1,1,1,0,0,1,0,1,0));   
    BoardLED(YELLOW,LEDOFF);
    BoardLED(GREEN,LEDON);
    BoardLED(RED,LEDOFF);
    BoardLED(ORANGE,LEDOFF);  
    break;
  case INITRECEIVE:
            // M M M M M M M M M M M M 
            // 1 1 2 2 3 3 4 4 5 6 7 7
            // a b a b a b a b     a b
    Switch(bin(0,0,0,0,0,0,1,1,1,1,0,0));   
    BoardLED(YELLOW,LEDOFF);
    BoardLED(GREEN,LEDOFF);
    BoardLED(RED,LEDON);
    BoardLED(ORANGE,LEDOFF); 
    break;
  case INITDISCHARGE:
            // M M M M M M M M M M M M 
            // 1 1 2 2 3 3 4 4 5 6 7 7
            // a b a b a b a b     a b
    Switch(bin(0,0,0,0,0,0,1,1,0,0,0,0));   
    BoardLED(YELLOW,LEDOFF);
    BoardLED(GREEN,LEDOFF);
    BoardLED(RED,LEDOFF);
    BoardLED(ORANGE,LEDON);  
    break;     
  default:
    Switch(bin(0,0,0,0,0,0,0,0,0,0,0,0));  
    BoardLED(YELLOW,LEDON);
    BoardLED(GREEN,LEDOFF);
    BoardLED(RED,LEDOFF);
    BoardLED(ORANGE,LEDOFF);
    break; 
  }
#else
#ifdef ARCH_TRANSFORMER
  switch(mode)
  {
  case OFF:
            // M M M M M M M M
            // T T B B P S BBBB
            // 1 2 1 2     1 2
    Switch(bin(0,0,0,0,0,0,0,0));  
    BoardLED(YELLOW,LEDON);
    //BoardLED(GREEN,LEDOFF);
    BoardLED(RED,LEDOFF);
    BoardLED(ORANGE,LEDOFF);
    break;
  case SEND_UP:
            // M M M M M M M M
            // T T B B P S BBBB
            // 1 2 1 2     1 2
    Switch(bin(0,0,0,0,0,0,0,0));  
    BoardLED(YELLOW,LEDOFF);
    //BoardLED(GREEN,LEDOFF);
    BoardLED(RED,LEDOFF);
    BoardLED(ORANGE,LEDON);
    break;
  case SEND_DOWN:
            // M M M M M M M M
            // T T B B P S BBBB
            // 1 2 1 2     1 2
    Switch(bin(0,0,0,0,0,0,1,1));  
    BoardLED(YELLOW,LEDOFF);
    //BoardLED(GREEN,LEDOFF);
    BoardLED(RED,LEDOFF);
    BoardLED(ORANGE,LEDON);
    break;
  case RECEIVE_UP:
            // M M M M M M M M
            // T T B B P S BBBB
            // 1 2 1 2     1 2
    Switch(bin(1,1,1,1,0,0,0,0));  
    BoardLED(YELLOW,LEDOFF);
    //BoardLED(GREEN,LEDOFF);
    BoardLED(RED,LEDON);
    BoardLED(ORANGE,LEDOFF);
    break;
  case RECEIVE_DOWN:
            // M M M M M M M M
            // T T B B P S BBBB
            // 1 2 1 2     1 2
    Switch(bin(1,1,1,1,0,0,1,1));  
    BoardLED(YELLOW,LEDOFF);
    //BoardLED(GREEN,LEDOFF);
    BoardLED(RED,LEDON);
    BoardLED(ORANGE,LEDOFF);
    break;  
  case FWD_MID:
            // M M M M M M M M
            // T T B B P S BBBB
            // 1 2 1 2     1 2
    Switch(bin(0,0,0,0,0,0,1,1));  
    BoardLED(YELLOW,LEDON);
    //BoardLED(GREEN,LEDOFF);
    BoardLED(RED,LEDOFF);
    BoardLED(ORANGE,LEDOFF);
    break;
    default:
            // M M M M M M M M
            // T T B B P S BBBB
            // 1 2 1 2     1 2
    Switch(bin(0,0,0,0,0,0,0,0));  
    BoardLED(YELLOW,LEDON);
    //BoardLED(GREEN,LEDOFF);
    BoardLED(RED,LEDOFF);
    BoardLED(ORANGE,LEDOFF);
    break;
  }
#endif
#endif
}

/* switchConfig is a 16 bit mask: LSB is switch 1 MSB would be switch 16 */
/* 0b0000000000101001 means that swiches 5,3, and 0 are ON, all others are OFF  */
/* include the file binary.h where you define a bitmask using the 0b notation */
void Switch(Int16U switchConfig)
{
#ifdef Switch00
  if ((switchConfig & (1<<11))==(1<<11))
  {
    Switch00Port->BSRRL |= Switch00Pin; // set switch output high}
  }
  else
  {
    Switch00Port->BSRRH |= Switch00Pin; // set switch output low
  }
#endif
  
#ifdef Switch01
  if ((switchConfig & (1<<10))==(1<<10))
  {
    Switch01Port->BSRRL |= Switch01Pin; // set switch output high}
  }
  else
  {
    Switch01Port->BSRRH |= Switch01Pin; // set switch output low
  }
#endif
  
#ifdef Switch02
  if ((switchConfig & (1<<9))==(1<<9))
  {
    Switch02Port->BSRRL |= Switch02Pin; // set switch output high}
  }
  else
  {
    Switch02Port->BSRRH |= Switch02Pin; // set switch output low
  }
#endif
  
#ifdef Switch03
  if ((switchConfig & (1<<8))==(1<<8))
  {
    Switch03Port->BSRRL |= Switch03Pin; // set switch output high}
  }
  else
  {
    Switch03Port->BSRRH |= Switch03Pin; // set switch output low
  }
#endif
  
#ifdef Switch04
  if ((switchConfig & (1<<7))==(1<<7))
  {
    Switch04Port->BSRRL |= Switch04Pin; // set switch output high}
  }
  else
  {
    Switch04Port->BSRRH |= Switch04Pin; // set switch output low
  }
#endif
  
#ifdef Switch05
  if ((switchConfig & (1<<6))==(1<<6))
  {
    Switch05Port->BSRRL |= Switch05Pin; // set switch output high}
  }
  else
  {
    Switch05Port->BSRRH |= Switch05Pin; // set switch output low
  }
#endif
  
#ifdef Switch06
  if ((switchConfig & (1<<5))==(1<<5))
  {
    Switch06Port->BSRRL |= Switch06Pin; // set switch output high}
  }
  else
  {
    Switch06Port->BSRRH |= Switch06Pin; // set switch output low
  }
#endif
  
#ifdef Switch07
  if ((switchConfig & (1<<4))==(1<<4))
  {
    Switch07Port->BSRRL |= Switch07Pin; // set switch output high}
  }
  else
  {
    Switch07Port->BSRRH |= Switch07Pin; // set switch output low
  }
#endif
  
#ifdef Switch08
  if ((switchConfig & (1<<3))==(1<<3))
  {
    Switch08Port->BSRRL |= Switch08Pin; // set switch output high}
  }
  else
  {
    Switch08Port->BSRRH |= Switch08Pin; // set switch output low
  }
#endif
  
#ifdef Switch09
  if ((switchConfig & (1<<2))==(1<<2))
  {
    Switch09Port->BSRRL |= Switch09Pin; // set switch output high}
  }
  else
  {
    Switch09Port->BSRRH |= Switch09Pin; // set switch output low
  }
#endif
  
#ifdef Switch10
  if ((switchConfig & (1<<1))==(1<<1))
  {
    Switch10Port->BSRRL |= Switch10Pin; // set switch output high}
  }
  else
  {
    Switch10Port->BSRRH |= Switch10Pin; // set switch output low
  }
#endif
  
#ifdef Switch11
  if ((switchConfig & (1<<0))==(1<<0))
  {
    Switch11Port->BSRRL |= Switch11Pin; // set switch output high}
  }
  else
  {
    Switch11Port->BSRRH |= Switch11Pin; // set switch output low
  }
#endif
  
}
