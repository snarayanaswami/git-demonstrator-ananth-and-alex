#ifndef soc_model_h
#define soc_model_h

#include <includes.h>


void get_xk_plus(matrixType * xk, matrixType * uk, double dt, matrixType * xk_plus);
void get_yk(matrixType * xk, matrixType * uk, matrixType * yk);


void get_Ak(matrixType * xk, matrixType * Ak);
void get_Ck(matrixType * xk, matrixType * Ck);

#endif