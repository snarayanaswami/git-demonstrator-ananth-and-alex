#ifndef DOGM162_h
#define DOGM162_h

#include <includes.h>

// Port Definitions -- change to suit your application
// ------------------------------------------------------------

#define DOGM_D0_PORT      GPIOD
#define DOGM_D0_PIN       GPIO_Pin_2

#define DOGM_D1_PORT      GPIOD
#define DOGM_D1_PIN       GPIO_Pin_3

#define DOGM_D2_PORT      GPIOD
#define DOGM_D2_PIN       GPIO_Pin_4
#define DOGM_D3_PORT      GPIOD
#define DOGM_D3_PIN       GPIO_Pin_4

#define DOGM_D4_PORT      GPIOD
#define DOGM_D4_PIN       GPIO_Pin_6

#define DOGM_D5_PORT      GPIOD
#define DOGM_D5_PIN       GPIO_Pin_7

#define DOGM_D6_PORT      GPIOD
#define DOGM_D6_PIN       GPIO_Pin_8

#define DOGM_D7_PORT      GPIOD
#define DOGM_D7_PIN       GPIO_Pin_15


#define DOGM_CS_PORT     GPIOE
#define DOGM_CS_PIN      GPIO_Pin_12

#define DOGM_RS_PORT      GPIOE
#define DOGM_RS_PIN       GPIO_Pin_2

#define DOGM_PSB_PORT     GPIOE
#define DOGM_PSB_PIN      GPIO_Pin_0

#define DOGM_RESET_PORT   GPIOE
#define DOGM_RESET_PIN    GPIO_Pin_1

///////////////////////////////////////////////////////////////////////////////////////////////////


// Function Declarations
// ------------------------------------------------------------

void Delay(uint32_t nTime);
void initialize_IO(void);
void dogm_set_cs();
void dogm_unset_cs();
void dogm_data_mode();
void dogm_command_mode();
void SPI_send(uint8_t data);
void command(uint8_t dat);
void LCD_INITSPI_EADOGM162(void);
void Displ_String(char *str);
void Displ_Char(Int8U data);
void Position(uint8_t column, uint8_t line);
void Clear_display(void);


#endif
