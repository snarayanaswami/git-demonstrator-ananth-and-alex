#ifndef matrix_operations_h
#define matrix_operations_h

#include <includes.h>

/** @brief Returnvalue of matrix operation */
enum math_error
{
MATH_ERR_SUCCESS             = 0x00, 		///< [0x00] Function exited successfully
MATH_ERR_SIZE_MISMATCH       = 0x01, 		///< [0x01] Matrices are of incompatible sizes
MATH_ERR_SINGULAR            = 0x02,            ///< [0x02] 
};

void matrix_init(matrixType * S, Int16U nRows, Int16U nColumns, float * pData);

Int16U matrix_multi(const matrixType * pSrcA, const matrixType * pSrcB, matrixType * pDst);

Int16U matrix_inverse(const matrixType * pSrc, matrixType * pDst);

Int16U matrix_subtr(const matrixType * pSrcA, const matrixType * pSrcB, matrixType * pDst);

Int16U matrix_add(const matrixType * pSrcA, const matrixType * pSrcB, matrixType * pDst);

Int16U matrix_scale(const matrixType * pSrc, float scale, matrixType * pDst);

Int16U matrix_transpose(const matrixType * pSrc, matrixType * pDst);

Int16U matrix_identity(const Int16U n, matrixType * I);
#endif