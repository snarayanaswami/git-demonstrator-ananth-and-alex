/**
 *********************************************************************************************************
 *
 * @file    soc_kalman.c
 * @author  Arne Meeuw
 * @version V0.0.1
 * @date    2-January-2015
 * @brief   C Implementation of an 'Extended Kalman Filter'
 * 
 *********************************************************************************************************
 */

#include <soc_kalman.h>

extern float K[5];
extern float R;
extern float capa_As;
extern nodeInfoType self;

/**
 * @brief Function to iterate Extended Kalman Filter one step
 * @param[in,out]       *KalmanState    Pointer to dataset describing kalman filter
 * @param[in]           *yk             Pointer to measured output vector of the system
 * @param[in]           *uk             Pointer to measured input vector of the system
 * @return              none
 */
void iterate_KF(kalmanType * KalmanState, matrixType * yk, matrixType * uk)
{
  // Initialize DUMMY deltat <- TODO put in actual delta_t
  double delta_t = 1/self.timerSettings.kfFrequency;

  
  // Initialize local size values from KalmanState
  Int16U n = KalmanState->n;
  Int16U p = KalmanState->p;
  Int16U q = KalmanState->q;

  // Initialize system and output matrix from last KalmanState
  matrixType Ak;
  float Ak_da[(n * n)];   // Available by VLA in C99 standard
  matrix_init(&Ak, n, n, (float*) Ak_da);
  get_Ak(&KalmanState->x, &Ak);
  matrixType Ck;
  float Ck_da[(q * n)];
  matrix_init(&Ck, q, n, (float*) &Ck_da);
  get_Ck(&KalmanState->x, &Ck);

  // Initialize '*k_minus'-values from KalmanState
  matrixType Ak_minus;
  Ak_minus = KalmanState->Ak;
  
  matrixType Sw;
  Sw = KalmanState->Sw;
  matrixType Sv;
  Sv = KalmanState->Sv;
  matrixType Sk_minus_super_plus;
  Sk_minus_super_plus = KalmanState->Sk;

  matrixType xk_minus_super_plus;
  xk_minus_super_plus = KalmanState->x;
  matrixType uk_minus;
  uk_minus = KalmanState->uk;

/**
 *        **Matrix operations for State estimate update:**
 *//*                                                                            
 *        xk_super_minus = f(xk_minus_super_plus, uk_minus, delta_t)           
 *//**                                                                            
  *        \f[\hat x_k^- = f(x_{k-1}^+, u_{k-1}, dt)\f]
 *
 */
  ///- Get xk_super_minus from nonlinear state space model 
  /// \f$[f(x_{k-1}^+, u_{k-1}, dt)]\f$
  matrixType xk_super_minus;
  float xk_s_m_da[(n * 1)];
  matrix_init(&xk_super_minus,  n, 1, (float*) xk_s_m_da);
  get_xk_plus(&xk_minus_super_plus, &uk_minus, delta_t, &xk_super_minus);


/**
 *        **Matrix operations for Error covariance time update:**
 *//*
 *        Sk_super_minus = Ak_minus * Sk_minus_super_plus * Ak_minus_trans + Sw
 *//**
 *        \f[\Sigma_{\tilde x, k}^- = \hat A_{k-1} \Sigma_{\hat x, k-1}^+ \hat A_{k-1}^T + \Sigma_w \f]
 *
 */
  
  ///- Multiply Ak_minus and Sk_minus_super_plus 
  /// \f$[ \hat A_{k-1} \Sigma_{\hat x, k-1}^+]\f$
  matrixType Ak_m_Sk_m_s_p;
  float Ak_m_Sk_m_s_p_da[(n * n)];
  matrix_init(&Ak_m_Sk_m_s_p,  n, n, (float*) Ak_m_Sk_m_s_p_da);
  matrix_multi(&Ak_minus, &Sk_minus_super_plus, &Ak_m_Sk_m_s_p);

  ///- Transpose Ak_minus 
  /// \f$[A_{k-1}^T]\f$
  matrixType Ak_minus_trans;
  float Ak_minus_trans_da[(n * n)];
  matrix_init(&Ak_minus_trans,  n, n, (float*) Ak_minus_trans_da);
  matrix_transpose(&Ak_minus, &Ak_minus_trans);

  ///- Multiply (Ak_minus * Sk_minus_super_plus) and Ak_minus_trans 
  /// \f$[\hat A_{k-1} \Sigma_{\hat x, k-1}^+ \hat A_{k-1}^T]\f$
  matrixType Ak_m_Sk_m_s_p_Ak_m_t;
  float Ak_m_Sk_m_s_p_Ak_m_t_da[(n * n)];
  matrix_init(&Ak_m_Sk_m_s_p_Ak_m_t,  n, n, (float*) Ak_m_Sk_m_s_p_Ak_m_t_da);
  matrix_multi(&Ak_m_Sk_m_s_p, &Ak_minus_trans, &Ak_m_Sk_m_s_p_Ak_m_t);

  ///- Add (Ak_minus * Sk_minus_super_plus * Ak_minus_trans) and Sw 
  /// \f$[\hat A_{k-1} \Sigma_{\hat x, k-1}^+ \hat A_{k-1}^T + \Sigma_w]\f$
  matrixType Sk_super_minus;
  float Sk_s_m_da[(n * n)];
  matrix_init(&Sk_super_minus,  n, n, (float*) Sk_s_m_da);
  matrix_add(&Ak_m_Sk_m_s_p_Ak_m_t, &Sw, &Sk_super_minus);


/**
 *        **Matrix operations for Kalman gain matrix calculation:**
 *//*
 *        Lk = Sk_super_minus * Ck_trans * (Ck * Sk_super_minus * Ck_trans + Sv)^-1
 *//**
 *        \f[L_k = \Sigma_{\tilde x, k}^- \hat C_k^T \cdot [\hat C_k \Sigma_{\tilde x,k}^- \hat C_k^T + \Sigma_v]^{-1}\f]
 */ 
  
  ///- Transpose Ck 
  /// \f$[\hat C_k^T]\f$
  matrixType Ck_trans;
  float Ck_trans_da[(n * q)];
  matrix_init(&Ck_trans,  n, q, (float*) Ck_trans_da);
  matrix_transpose(&Ck, &Ck_trans);

  ///- Multiply Sk_super_minus (n x n) and Ck_trans (n x q) 
  /// \f$[\Sigma_{\tilde x, k}^- \hat C_k^T]\f$
  matrixType Sk_s_m_Ck_t;
  float Sk_s_m_Ck_t_da[(n * q)];
  matrix_init(&Sk_s_m_Ck_t,  n, q, (float*) Sk_s_m_Ck_t_da);
  matrix_multi(&Sk_super_minus, &Ck_trans, &Sk_s_m_Ck_t);

  ///- Multiply Ck and Sk_super_minus 
  /// \f$[\hat C_k \Sigma_{\tilde x,k}^-]\f$
  matrixType Ck_Sk_s_m;
  float Ck_Sk_s_m_da[(q * n)];
  matrix_init(&Ck_Sk_s_m,  q, n, (float*) Ck_Sk_s_m_da);
  matrix_multi(&Ck, &Sk_super_minus, &Ck_Sk_s_m);

  ///- Multiply (Ck * Sk_super_minus) and Ck_trans 
  /// \f$[\hat C_k \Sigma_{\tilde x,k}^- \hat C_k^T]\f$
  matrixType Ck_Sk_s_m_Ck_t;
  float Ck_Sk_s_m_Ck_t_da[(q * q)];
  matrix_init(&Ck_Sk_s_m_Ck_t,  q, q, (float*) Ck_Sk_s_m_Ck_t_da);
  matrix_multi(&Ck_Sk_s_m, &Ck_trans, &Ck_Sk_s_m_Ck_t);

  ///- Add (Ck * Sk_super_minus * Ck_trans) and Sv 
  /// \f$[\hat C_k \Sigma_{\tilde x,k}^- \hat C_k^T + \Sigma_v]\f$
  matrixType Ck_Sk_s_m_Ck_t_Sv;
  float Ck_Sk_s_m_Ck_t_Sv_da[(q * q)];
  matrix_init(&Ck_Sk_s_m_Ck_t_Sv,  q, q, (float*) Ck_Sk_s_m_Ck_t_Sv_da);
  matrix_add(&Ck_Sk_s_m_Ck_t, &Sv, &Ck_Sk_s_m_Ck_t_Sv);

  ///- Inverse (Ck * Sk_super_minus * Ck_trans + Sv) 
  /// \f$[[\hat C_k \Sigma_{\tilde x,k}^- \hat C_k^T + \Sigma_v]^{-1}]\f$
  matrixType Ck_Sk_s_m_Ck_t_Sv_inv;
  float Ck_Sk_s_m_Ck_t_Sv_inv_da[(q * q)];
  matrix_init(&Ck_Sk_s_m_Ck_t_Sv_inv,  q, q, (float*) Ck_Sk_s_m_Ck_t_Sv_inv_da);
  matrix_inverse(&Ck_Sk_s_m_Ck_t_Sv, &Ck_Sk_s_m_Ck_t_Sv_inv);

  ///- Multiply (Sk_super_minus * Ck_trans) and (Ck * Sk_super_minus * Ck_trans + Sv)^-1 
  /// \f$[\Sigma_{\tilde x, k}^- \hat C_k^T \cdot [\hat C_k \Sigma_{\tilde x,k}^- \hat C_k^T + \Sigma_v]^{-1}]\f$
  matrixType Lk;
  float Lk_da[(n * q)];
  matrix_init(&Lk,  n, q, (float*) Lk_da);
  matrix_multi(&Sk_s_m_Ck_t, &Ck_Sk_s_m_Ck_t_Sv_inv, &Lk);


/**
 *        **Matrix operations for State estimate mesaurement update:**
 *//*                                                                            
 *        xk_super_plus = xk_super_minus + Lk * (yk - g(xk_super_minus, uk))      
 *//**
 *        \f[\hat x_k^+ = \hat x_k^- + L_k [y_k - g(\hat x_k^-, u_k)]\f]
 *
 */
  ///- Get model output estimate 
  /// \f$[y_{k,est} = g(\hat x_k^-, u_k)]\f$
  matrixType yk_est;
  float yk_est_da[(q * 1)];
  matrix_init(&yk_est,  q, 1, (float*) yk_est_da);
  get_yk(&xk_super_minus, uk, &yk_est);

  ///- Subtract y_k from yk_est 
  /// \f$[y_k - y_{k,est}]\f$
  matrixType yk_yk_e;
  float yk_yk_e_da[(q * 1)];
  matrix_init(&yk_yk_e, q, 1, (float*) yk_yk_e_da);
  matrix_subtr(&yk_est, yk, &yk_yk_e);

  ///- Multiply (yk - yk_est) and Lk
  /// \f$[(y_k - y_{k,est}) L_k]\f$
  matrixType yk_yk_e_Lk;
  float yk_yk_e_Lk_da[(n * 1)];
  matrix_init(&yk_yk_e_Lk,  n, 1, (float*) yk_yk_e_Lk_da);
  matrix_multi(&yk_yk_e, &Lk, &yk_yk_e_Lk);

  ///- Add ((yk - yk_est) * Lk) and xk_super_minus 
  /// \f$[((y_k - y_{k,est}) L_k)+\hat x_k^-]\f$
  matrixType xk_super_plus;
  float xk_super_plus_da[(n * 1)];
  matrix_init(&xk_super_plus,  n, 1, (float*) xk_super_plus_da);
  matrix_add(&yk_yk_e_Lk, &xk_super_minus, &xk_super_plus);


/**
 *        **Matrix operations for Error covariance measurement update:**
 *//*
 *        Sk_super_plus = (I - Lk * Ck) * Sk_super_minus
 *//**
 *        \f[\Sigma_{\tilde x, k}^+ = (I - L_k \hat C_k) \Sigma_{\tilde x, k}^-\f]
 *
 */
  ///- Multiply Lk and Ck 
  /// \f$[L_k \hat C_k]\f$
  matrixType Lk_Ck;
  float Lk_Ck_da[(n * n)];
  matrix_init(&Lk_Ck,  n, n, (float*) Lk_Ck_da);
  matrix_multi(&Lk, &Ck, &Lk_Ck);

  ///- Get Identity Matrix 
  /// \f$[I]\f$
  matrixType I;
  float I_da[(n * n)];
  matrix_init(&I, n, n, (float*) I_da);
  matrix_identity(1, &I);

  ///- Subtract (Lk * Ck) from I 
  /// \f$[I - L_k \hat C_k]\f$
  matrixType Lk_Ck_I;
  float Lk_Ck_I_da[(n * n)];
  matrix_init(&Lk_Ck_I, n, n, (float*) Lk_Ck_I_da);
  matrix_subtr(&I, &Lk_Ck, &Lk_Ck_I);

  ///- Multiply (I - (Lk * Ck)) and Sk_super_minus 
  /// \f$[(I - L_k \hat C_k) \Sigma_{\tilde x, k}^-]\f$
  matrixType Sk_super_plus;
  float Sk_super_plus_da[(n * n)];
  matrix_init(&Sk_super_plus,  n, n, (float*) Sk_super_plus_da);
  matrix_multi(&Lk_Ck_I, &Sk_super_minus, &Sk_super_plus);

  // RANGE LIMITIATION; TODO -> REPLACE WITH ROBUSTNESS CODE
  if (xk_super_plus.pData[0] < 0.001)
    xk_super_plus.pData[0] = 0.001;
  if (xk_super_plus.pData[0] > 0.999)
    xk_super_plus.pData[0] = 0.999;
  if (xk_super_plus.pData[0] != xk_super_plus.pData[0])
    kalman_matrix_init(KalmanState);
  

  // Assign back '*k'-values to KalmanState
  KalmanState->Ak = Ak;
  KalmanState->Ck = Ck;
  KalmanState->L  = Lk;
  KalmanState->Sk = Sk_super_plus;
  KalmanState->Sv = Sv;
  KalmanState->Sw = Sv;
  KalmanState->uk = (*uk);
  memcpy(KalmanState->x.pData, xk_super_plus.pData, (n * 1) * sizeof(float));
}