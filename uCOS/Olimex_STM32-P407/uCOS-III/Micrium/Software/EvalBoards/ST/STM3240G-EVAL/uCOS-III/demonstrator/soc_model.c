/**
 *********************************************************************************************************
 *
 * @file    soc_model.c
 * @author  Arne Meeuw
 * @version V0.0.1
 * @date    2-January-2015
 * @brief   Battery model descriptive functions
 * 
 * The model is split into two groups.<br>
 * The first group comprises the nonlinear equations.<br>
 * The second group contains the linearized jacobian matrices<br>
 * needed for the Extended Kalman Filter.
 * 
 *********************************************************************************************************
 */ 

#include "soc_model.h"

/// Parameters K[0] - K[5] derived from trained model
//float K[5] = {2.7745, -0.4827, 0.961, -1.439, -0.0486};
float K[5] = {19.5627322, 21.7358585, -38.4256778, 57.6359534, -0.138298032};
/// Parameter R derived from trained model
//float R = 0.0228;
float R = 0.0213770711;
/// Capacity of cell
float capa_As = 1.1 * 3600;

/**
 * @brief Function calculating the next state vector using the non-linear battery model
 * @param[in]           *xk      Pointer to current state vector
 * @param[in]           *uk      Pointer to current input vector
 * @param[in,out]       *xk_plus Pointer to next state vector
 * @return              none
 * 
 * 
 *  Non linear state-space model (state-vector):
 *//*
 *              xk_plus = f(xk, uk)
 *//**
 * \f[x_k+1 = f(x_k, u_k)\f]
 * 
 */
void get_xk_plus(matrixType * xk, matrixType * uk, double dt, matrixType * xk_plus)
{
  /// Size of state vector
  Int16U n = xk->numRows;

  /// Get parameters from state and input vector
  float soc = xk->pData[0];  ///> State of charge
  float ik  = uk->pData[0];  ///> Cell Current
  float xk_plus11 = soc - ik * dt / capa_As;
  float xk_plus_da[1] =
  {
    xk_plus11
  };
  /// Copy local function data from stack to heap
  memcpy(xk_plus->pData, &xk_plus_da, (n * 1) * sizeof(float));
}

/**
 * @brief Function calculating the current output vector using the non-linear battery model
 * @param[in]           *xk      Pointer to current state vector
 * @param[in]           *uk      Pointer to current input vector
 * @param[in,out]       *yk      Pointer to current output vector
 * @return              none
 * 
 *  Non linear state-space model (output-vector):
 *//*
 *                 yk = g(xk, uk)
 *//**          
 * \f[y_k = g(x_k, u_k)\f]
 * 
 */
void get_yk(matrixType * xk, matrixType * uk, matrixType * yk)
{
  /// Size of output vector
  Int16U q = yk->numRows;

  /// Get parameters from state vector
  float soc = xk->pData[0];  ///> State of charge
  float ik  = uk->pData[0];  ///> Cell Current
  float yk11 = K[0] + K[1]/soc + K[2]*soc + K[3] * log(soc) + K[4] * log(1-soc) - R*ik;
  float yk_da[1] =
  {
   yk11
  };
  /// Copy local function data from stack to heap
  memcpy(yk->pData, &yk_da, (q * 1) * sizeof(float));
}

/**
 * @brief Function calculating linearized jacobian system matrix
 * @param[in]           *xk      Pointer to current state vector
 * @param[in,out]       *Ak      Pointer system matrix
 * @return              none
 * 
 *  Linearized jacobian system matrix:
 *//* 
 *  Ak = df(xk, uk)|
 *       ----------|
 *           dxk   |xk=xk_super_plus
 *//**
 * \f[\hat{A_k} = \frac{\partial f(x_k, u_k)}
 *                     {\partial x_k}\bigg|_{x_k=\hat{x}_k^+}\f]
 * 
 */
void get_Ak(matrixType * xk, matrixType * Ak)
{
  /// n -> Size of state vector
  Int16U n = xk->numRows;

  float Ak11 = 1.0;
  // MATRIX Ak DATA
  float Ak_da[1] =
  {
    Ak11
  };
  /// Copy local function data from stack to heap
  memcpy(Ak->pData, &Ak_da, (n * n) * sizeof(float));
}

/**
 * @brief Function calculating linearized jacobian output matrix
 * @param[in]           *xk      Pointer to current state vector
 * @param[in,out]       *Ck      Pointer to output matrix
 * @return              none
 * 
 *  Linearized jacobian output matrix:
 *//* 
 *  Ck = dg(xk, uk)|
 *       ----------|
 *           dxk   |xk=xk_super_minus
 *//**               
 * \f[\hat{C_k} = \frac{\partial g(x_k, u_k)}
 *                       {\partial x_k}\bigg|_{x_k=\hat{x}_k^-}\f]
 *
 */
void get_Ck(matrixType * xk, matrixType * Ck)
{
  /// n -> Size of state vector
  const Int16U n = xk->numRows;
  /// q -> Size of output vector
  const Int16U q = Ck->numRows;
  
  /// Get parameters from state vector
  float soc = xk->pData[0];  ///> State of charge
  float a11 = (K[1]/soc/soc - K[2] + K[3] / soc - K[4]/(1-soc));
  // MATRIX Ck DATA
  float Ck_da[1] =
  {
    a11
  };
  /// Copy local function data from stack to heap
  memcpy(Ck->pData, &Ck_da, (q * n) * sizeof(float));
}