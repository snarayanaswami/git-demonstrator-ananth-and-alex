/**
 *********************************************************************************************************
 *
 * @file    control.c
 * @author  Arne Meeuw
 * @version V0.0.1
 * @date    2-January-2015
 * @brief   Control functions for sending requests, acknowledgements and processing the same.
 * 
 *********************************************************************************************************
 */ 

#include <control.h>

extern nodeInfoType self;                       ///< Cell information
extern packInfoType packInfo;                   ///< Battery pack information
extern Int8U NUMOFNODES;                        ///< Number of nodes in the system
extern Int8U SystemID;                          ///< Cell's own ID

extern OS_Q BlockResponseQ;                     ///< Message Queue used for cell blocking reply management
extern OS_TCB AcknowledgeTask;                  ///< Used for posting into AcknowledgeTask's message queue

extern transactionInfoType rxTransaction;       ///< 
extern transactionInfoType txTransaction;       ///< 

/**
 * @brief Initialize switch positions and status 
 *
 */
void ControlInit(void)
{
  SwitchConfig(OFF);
  packInfo.status[self.id]=UNDEFINED;
  self.isTempMaster=false;
  
#ifdef MOCK_VOLTAGE
  packInfo.voltage[self.id] = 3.8;
#else
  packInfo.voltage[self.id] = SPI_read_voltage();
#endif
  
  for (Int8U counter = 0; counter < NUMOFNODES; counter++)
  {
    packInfo.status[counter]=UNDEFINED; 
  }
}

/** 
 * @brief Send message containing the cells SOC as a 32bit float
 *
 */
void SendSOCMessage(void)
{
  // Union for seperating 32 bit float value into 4 bytes
  union floatUnion_t
  {
    float f;
    Int8U bytes[sizeof(float)];
  } floatBytes;
  floatBytes.f = self.soc;
  
  Int16U msg_type = MSG_TYPE_SOC;	// TYPE SOCBroadcast
  Int8U msg_target = 0xFF;		// Broadcast
  Int8U msg_origin = self.id;		// origin is self.id
  Int8U DLC = 4;                        // length of message is 4 bytes

  packInfo.soc[self.id] = self.soc;
  sendCANMessage(msg_type, msg_target, msg_origin, DLC, floatBytes.bytes);
}

/** 
 * @brief Send debug message containing the cells SOC as a 32bit float
 *
 */
void SendDebugSOCMessage(void)
{
  // Union for seperating 32 bit float value into 4 bytes
  union floatUnion_t
  {
    float f;
    Int8U bytes[sizeof(float)];
  } floatBytes;
  floatBytes.f = self.soc;
  
  Int16U msg_type = MSG_TYPE_DEBUG_SOC;	// TYPE SOCBroadcast
  Int8U msg_target = 0xFF;		// Broadcast
  Int8U msg_origin = self.id;		// origin is self.id
  Int8U DLC = 4;                        // length of message is 4 bytes

  sendCANMessage(msg_type, msg_target, msg_origin, DLC, floatBytes.bytes);
}

/**
 * @brief Process SOC message and save the received values in packInfo array
 * @param[in] *canMsg Pointer to received CAN-message
 */
void ProcessSOCMessage(CanRxMsg *canMsg)
{
  union floatUnion_t
  {
    float f;
    Int8U bytes[sizeof(float)];
  } floatBytes;

  idType senderID = (canMsg->ExtId>>8)&0xFF;

  for (Int8U counter = 0; counter < 4; counter++)
  {
          floatBytes.bytes[counter] = canMsg->Data[counter];
  }
  packInfo.soc[senderID] = floatBytes.f;
}

/**
 * @brief Send message containing the cell's voltage as a 32bit float 
 *
 */
void SendVoltageMessage(void)
{
  union floatUnion_t
  {
    float f;
    Int8U bytes[sizeof(float)];
  } floatBytes;
  floatBytes.f = self.voltage;
  
  Int16U msg_type = MSG_TYPE_VOLTAGE;	// TYPE VoltageBroadcast
  Int8U msg_target = 0xFF;		// Broadcast
  Int8U msg_origin = self.id;		// origin is self.id
  Int8U DLC = 4;                        // length of message is 4 bytes

  packInfo.voltage[self.id] = self.voltage;
  sendCANMessage(msg_type, msg_target, msg_origin, DLC, floatBytes.bytes);
}

/**
 * @brief Send debug message containing the cell's voltage as a 32bit float 
 *
 */
void SendDebugVoltageMessage(void)
{
  union floatUnion_t
  {
    float f;
    Int8U bytes[sizeof(float)];
  } floatBytes;
  floatBytes.f = self.voltage;
  
  Int16U msg_type = MSG_TYPE_DEBUG_VOLTAGE;	// TYPE VoltageBroadcast
  Int8U msg_target = 0xFF;		        // Broadcast
  Int8U msg_origin = self.id;		        // origin is self.id
  Int8U DLC = 4;                                // length of message is 4 bytes

  sendCANMessage(msg_type, msg_target, msg_origin, DLC, floatBytes.bytes);
}

/** 
 * @brief Process voltage message and save the received values in packInfo array
 * @param[in] *canMsg Pointer to received CAN-message
 * 
 */
void ProcessVoltageMessage(CanRxMsg *canMsg)
{
  union floatUnion_t
  {
    float f;
    Int8U bytes[sizeof(float)];
  } floatBytes;
  
  for (Int8U counter = 0; counter < 4; counter++)
  {
          floatBytes.bytes[counter] = canMsg->Data[counter];
  }
  
  // Message origin is 4th byte in ID
  Int8U origin = (canMsg->ExtId)&0xFF;
  
  packInfo.voltage[origin] = floatBytes.f;
}

/** 
 * @brief Send balancing request containing sender-/receiver-pair
 * @param[in] *RequestMessage Pointer to struct containing information about proposed balancing transaction
 *
 */
void SendSendRequest(transactionInfoType *transactionRequest)
{
  Int16U msg_type = MSG_TYPE_SEND_REQ;	                // TYPE BlockRequest
  Int8U msg_target = transactionRequest->senderID;      // target is senderID
  Int8U msg_origin = transactionRequest->receiverID;    // origin is receiverID
  /*
  // Extended Frame:
  canMessage.DLC = 8;
  union floatUnion_t
  {
    float f;
    char bytes[sizeof(float)];
  } floatBytes;
  
  floatBytes.f = transactionRequest->transferRate;
  // Set Message Bytes [0..3] to transferRate
  canMessage.Data[0] = floatBytes.bytes[0];
  canMessage.Data[1] = floatBytes.bytes[1];
  canMessage.Data[2] = floatBytes.bytes[2];
  canMessage.Data[3] = floatBytes.bytes[3];
  
  floatBytes.f = transactionRequest->transferTime;
  // Set Message Bytes [4..7] to transferTime
  canMessage.Data[4] = floatBytes.bytes[0];
  canMessage.Data[5] = floatBytes.bytes[1];
  canMessage.Data[6] = floatBytes.bytes[2];
  canMessage.Data[7] = floatBytes.bytes[3];  
  */
  
  sendCANMessage(msg_type, msg_target, msg_origin, 0, NULL);
}

/** 
 * @brief Process balancing request and forward to acknowledge-strategy task
 * @param[in] *canMsg Pointer to received CAN-message
 * 
 */
void ProcessSendRequest(CanRxMsg *canMsg)
{
  OS_ERR err;
  Int8U msg_target = (canMsg->ExtId>>8)&0xFF;
  Int8U msg_origin = (canMsg->ExtId)&0xFF;
  
  if ( packInfo.status[self.id] == OFF )
  { 
  txTransaction.senderID = msg_target;
  txTransaction.receiverID = msg_origin;
  txTransaction.blockSuccessful = false;


  OSTaskQPost(&AcknowledgeTask,
              (void*) &txTransaction,
              sizeof (void*),
              OS_OPT_POST_FIFO,
              &err);
  }
}

/** 
 * @brief Send acknowledge containing sender-/receiver-pair
 * @param[in] *transactionInfo Pointer to struct containing information about proposed balancing transaction
 * 
 */
void SendSendAcknowledge(transactionInfoType *transactionInfo)
{
  Int16U msg_type = MSG_TYPE_SEND_ACK;	               // TYPE SendAcknowledge
  Int8U msg_target = transactionInfo->receiverID;      // target is receiverID
  Int8U msg_origin = transactionInfo->senderID;        // origin is senderID
  
  /*
  // Extended Frame:
  canMessage.DLC = 8;
  union floatUnion_t
  {
    float f;
    char bytes[sizeof(float)];
  } floatBytes;
  
  floatBytes.f=transactionInfo->transferRate;
  // Set Message Bytes [0..3] to transferRate
  canMessage.Data[0] = floatBytes.bytes[0];
  canMessage.Data[1] = floatBytes.bytes[1];
  canMessage.Data[2] = floatBytes.bytes[2];
  canMessage.Data[3] = floatBytes.bytes[3];
  
  floatBytes.f=transactionInfo->transferTime;
  // Set Message Bytes [4..7] to transferTime
  canMessage.Data[4] = floatBytes.bytes[0];
  canMessage.Data[5] = floatBytes.bytes[1];
  canMessage.Data[6] = floatBytes.bytes[2];
  canMessage.Data[7] = floatBytes.bytes[3];  
  */
  
  sendCANMessage(msg_type, msg_target, msg_origin, 0, NULL);
}

/** 
 * @brief Process acknowledge message, check if node is blocked and start receiving charge
 * @param[in] *canMsg Pointer to received CAN-message
 * 
 */
void ProcessSendAcknowledge(CanRxMsg *canMsg)
{
  Int8U msg_origin = (canMsg->ExtId)&0xFF;
  Int8U senderID   = msg_origin;
  Int8U receiverID = self.id;  
  
  transactionInfoType transaction;
  transaction.receiverID = receiverID;
  transaction.senderID = senderID;
  
  // Testify one last time if mode and acknowledger is legit
  if ( (packInfo.status[self.id] == getRole(self.id, &transaction)) && (isInList(senderID, self.blockerIDs, 2)) )
  {
    if ( (packInfo.status[self.id] == RECEIVE_UP) || (packInfo.status[self.id] == RECEIVE_DOWN) )
    {
      beforeReceive();
      SwitchConfig(packInfo.status[self.id]);
      // Set receiveActive flag
      self.receiveActive = true;
    }
  }
}

/** 
 * @brief Send block-request containing sender-/receiver-pair
 * @param[in] senderID ID of cell who is sending charge in the proposed balancing transaction
 * @param[in] receiverID ID of cell who is receiving charge in the proposed balancing transaction
 * 
 */
void SendBlockRequest(Int8U senderID, Int8U receiverID)
{ 
  Int16U msg_type = MSG_TYPE_BLOCK;	// TYPE BlockRequest
  Int8U msg_target = 0xFF;		// Broadcast
  Int8U msg_origin = self.id;		// origin is self.id
  
  // Block Cell because cell cannot hear it's own messages on the bus
  BlockSelf(senderID, receiverID);
  
  Int8U data[2];
  data[0] = senderID;
  data[1] = receiverID;
  sendCANMessage(msg_type, msg_target, msg_origin, 2, data);
}

/**
 * @brief Process block-request and answer according whether successful or not
 * @param[in] *canMsg Pointer to received CAN-message
 * 
 */
void ProcessBlockRequest(CanRxMsg *canMsg)
{
  OS_ERR err;
  Int8U msg_origin = (canMsg->ExtId)&0xFF;
  Int8U senderID = canMsg->Data[0];
  Int8U receiverID = canMsg->Data[1];
  
  transactionInfoType transaction;
  transaction.receiverID = receiverID;
  transaction.senderID = senderID;
  if (isInParticipantList(self.id, senderID, receiverID))
  {
    ///- Cell is OFF -> take over status in proposed transaction
    if (packInfo.status[self.id] == OFF)
    {
      // set status to according role
      packInfo.status[self.id] = getRole(self.id, &transaction);
      // add blocker to blockerIDs list
      appendBlocker(msg_origin);
      
#ifdef BOARD_TUM
      if ( (packInfo.status[self.id] == FWD_MID) )
      {
        SwitchConfig(packInfo.status[self.id]);
      }
#endif
      
    }
          ///- Cell is BLOCKED and role in proposed transaction is BLOCKED
    else 
    if ( (packInfo.status[self.id] == BLOCKED) && (getRole(self.id, &transaction) == BLOCKED) )
    {
      if (!isInList(msg_origin, self.blockerIDs, 2))
      {
        ///-- Set status to BLOCKED
        packInfo.status[self.id] = BLOCKED;
        // add blocker to blockerIDs list
        appendBlocker(msg_origin); 
      }
    }
    
    // Stray replies over random period of 10 ms to avoid collision 
    // TODO->ELIMINATE!!!
    OSTimeDlyHMSM(0, 
                  0, 
                  0, 
                  random_int()%10, 
                  OS_OPT_TIME_HMSM_STRICT, 
                  &err);
    
    // Send status response containing taken status and blockerIDs
    SendStatusResponse(msg_origin, packInfo.status[self.id], self.blockerIDs);
  }
}

/** 
 * @brief Block own cell since CAN messages are not received by self
 * @param[in] senderID ID of cell who is sending charge in the proposed balancing transaction
 * @param[in] receiverID ID of cell who is receiving charge in the proposed balancing transaction
 * 
 */
void BlockSelf(idType senderID, idType receiverID)
{
  OS_ERR err;

  transactionInfoType transaction;
  transaction.receiverID = receiverID;
  transaction.senderID = senderID;
  
  packInfo.status[self.id] = getRole(self.id, &transaction);
  appendBlocker(self.id);
  
  BlockResponseType *BlockResponse = (BlockResponseType*) malloc(sizeof(BlockResponseType));
  BlockResponse->status     = packInfo.status[self.id];
  BlockResponse->blockerID0 = self.blockerIDs[0];
  BlockResponse->blockerID1 = self.blockerIDs[1];
  BlockResponse->origin     = self.id;
  BlockResponse->target     = self.id;

  OSQPost(&BlockResponseQ, 
        (void*) BlockResponse, 
        sizeof(void*), 
        OS_OPT_POST_FIFO, 
        &err);
}

/**
 * @brief Send block-request containing sender-/receiver-pair
 * @param[in] senderID ID of cell who is sending charge in the proposed balancing transaction
 * @param[in] receiverID ID of cell who is receiving charge in the proposed balancing transaction
 * 
 */
void SendUnblockRequest(Int8U senderID, Int8U receiverID)
{ 
  Int16U msg_type = MSG_TYPE_UNBLOCK;	// TYPE BlockRequest
  Int8U msg_target = 0xFF;		// Broadcast
  Int8U msg_origin = self.id;		// origin is self.id
  OS_ERR err;
  
  // Unblock Cell because cell cannot hear it's own messages on the bus
  UnblockSelf(senderID, receiverID);
  
  Int8U data[2];
  data[0] = senderID;
  data[1] = receiverID;
  sendCANMessage(msg_type, msg_target, msg_origin, 2, data);
  OSTimeDlyHMSM(0, 0, 0, random_int()%10, OS_OPT_TIME_HMSM_STRICT, &err);
  sendCANMessage(msg_type, msg_target, msg_origin, 2, data);
}

/**
 * @brief Process unblock-request and answer according whether successful or not
 * @param[in] *canMsg Pointer to received CAN-message
 * 
 */
void ProcessUnblockRequest(CanRxMsg *canMsg)
{
  Int8U msg_origin = (canMsg->ExtId)&0xFF;
  Int8U senderID = canMsg->Data[0];
  Int8U receiverID = canMsg->Data[1];
  
  transactionInfoType transaction;
  transaction.receiverID = receiverID;
  transaction.senderID = senderID;
  
  // Check if cell is part of proposed transaction
  if (isInParticipantList(self.id, senderID, receiverID))
  {
    if ( isInList(msg_origin, self.blockerIDs, 2) )
    {
      if ( packInfo.status[self.id] == BLOCKED )
      {
        removeBlocker(msg_origin);
        if ( self.blockers == 0 )
        {
          packInfo.status[self.id] = OFF;
        }
        else
        {
          packInfo.status[self.id] = BLOCKED;
        }
      }
      else
      {
      // Check if 
      if (packInfo.status[self.id] == getRole(self.id, &transaction))
      {
        SwitchConfig(OFF);
        packInfo.status[self.id] = OFF;
        flushBlockers();
        if ( (self.id == transaction.receiverID) && self.receiveActive)
        {
          // Receive charge done -> remove receiveFlag and execute afterReceive() function
          self.receiveActive = false;
          afterReceive();
        }
      }
      }
    }
  }
}

/**
 * @brief Block own cell since CAN messages are not received by self 
 * @param[in] senderID ID of cell who is sending charge in the proposed balancing transaction
 * @param[in] receiverID ID of cell who is receiving charge in the proposed balancing transaction
 * 
 */
void UnblockSelf(idType senderID, idType receiverID)
{
  SwitchConfig(OFF);
  packInfo.status[self.id] = OFF;
  flushBlockers();
}

/**
 * @brief Send statusresponse after blockingrequest containing blocker-cell-ID
 * @param[in] targetID Message recipient (i.e. cell who sent the blocking request)
 * @param[in] status Cell's own status
 * 
 */
void SendStatusResponse(Int8U targetID, Int8U status, Int8U * blockerIDs)
{
  Int16U msg_type = MSG_TYPE_STATUS_RESPONSE;	// TYPE StatusResponse
  Int8U msg_target = targetID;			// to targetID
  Int8U msg_origin = self.id;			// origin is self.id

  Int8U data[3];
  data[0] = status;
  data[1] = blockerIDs[0];
  data[2] = blockerIDs[1];
  sendCANMessage(msg_type, msg_target, msg_origin, 3, data);
}

/**
 * @brief Process statusresponse and check whether block-request was successful
 * @param[in] *canMsg Pointer to received CAN-message
 * 
 */
void ProcessStatusResponse(CanRxMsg *canMsg)
{ 
  OS_ERR err;
  BlockResponseType *BlockResponse = (BlockResponseType*) malloc(sizeof(BlockResponseType));
  BlockResponse->status     = canMsg->Data[0];
  BlockResponse->blockerID0 = canMsg->Data[1];
  BlockResponse->blockerID1 = canMsg->Data[2];
  BlockResponse->origin     = (canMsg->ExtId)&0xFF;
  BlockResponse->target     = (canMsg->ExtId>>8)&0xFF;
  
  if ( ((self.id == BlockResponse->blockerID0) || (self.id == BlockResponse->blockerID1) )
      &&(self.id == BlockResponse->target) )
  {
    OSQPost(&BlockResponseQ, 
            (void*) BlockResponse, 
            sizeof(void*), 
            OS_OPT_POST_FIFO, 
            &err);
  }
}

