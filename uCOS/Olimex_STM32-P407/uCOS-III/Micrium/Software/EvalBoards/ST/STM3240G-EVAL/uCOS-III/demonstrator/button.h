#ifndef button_h
#define button_h

void HWButtonInit(void);
void StartStopButtonInit(void);
void EXTI0_IRQHandler(void);
void EXTILine0_Config(void);
void EXTI2_IRQHandler(void);
void EXTILine2_Config(void);

#endif