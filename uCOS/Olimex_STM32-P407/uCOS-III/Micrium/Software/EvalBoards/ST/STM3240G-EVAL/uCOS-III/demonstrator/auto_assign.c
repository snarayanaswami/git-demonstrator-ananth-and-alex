/**
 *********************************************************************************************************
 *
 * @file    auto_assign.c
 * @author  Arne Meeuw
 * @version V0.0.1
 * @date    2-January-2015
 * @brief   Auto assignment of ID based on sending current pulses through battery-pack-chain
 * 
 *********************************************************************************************************
 */ 

#include <auto_assign.h>


extern Int8U ID_AUTODETECT;

extern nodeInfoType self;
extern packInfoType packInfo;
extern Int8U NUMOFNODES;
extern Int8U SystemID;

extern Int8U ScreenToShow;

extern struct listNode *leftRoot;
extern struct listNode *rightRoot;
extern struct listNode *leftEnd;
extern struct listNode *rightEnd;

extern Int8U autoInitState;
extern Int8U neighborTrigger;

extern Int32U LastButtonTS;

Int8U IDList[MAXNUMOFNODES];
Int8U receivedIDs = 0;
Int8U addToRight=true;
Int8U currentPivot=0;
Int8U detectedIDs=0;

extern CanRxMsg RxMessage;                      ///< Received CAN-message

void autoInit(void)
{
  Int8U rnum;
  Int8U tempID;
  Int8U repeat;
  OS_ERR err;
  
  CanTxMsg canMessage;
  Int16U msg_type;
  Int16U msg_target;
  Int16U msg_origin;
  //char buf[32];
  //Int8U counter=0;
  Int8U lastDetected=0;
  Int8U thisCellIsMaster=true;
  Int8U outcounter=0;
  Int8U myRealID=0;
  struct listNode * currentPos=rightRoot;
  
  switch(autoInitState)
  {
  case 0: //generate random ID and broadcast via CAN
    rnum = 1+(random_int()%254);
    //rnum=1;
    tempID = rnum;
    SystemID = tempID;
    OSTimeDlyHMSM(0, 0, 1, random_int()%1000, 
                  OS_OPT_TIME_HMSM_STRICT, 
                  &err);  
    
    msg_type = MSG_TYPE_AUTODETECT_RANDOM;	                // TYPE AutoDetect Random Broadcast
    msg_target = 0xFF;		                                // Broadcast
    msg_origin = tempID;		                        // origin is tempID
    canMessage.StdId = 0;
    canMessage.ExtId = (msg_type<<16) | (msg_target<<8) | (msg_origin);
    canMessage.RTR = CAN_RTR_DATA;
    canMessage.IDE = CAN_ID_EXT;
    canMessage.DLC = 8;
    
    for (Int8U counter = 0; counter < 8; counter++)
    {  
      canMessage.Data[counter] = random_int()%256;
    }
    
    repeat=false;
    
    // check if someone else already has the same ID, if so, try again
    for (Int8U counter = 0; counter < receivedIDs; counter++)
    {
      if (IDList[counter]== tempID) repeat=true;
    }
    
    if (!repeat)
    {
      CAN_Transmit(CAN1, &canMessage);
      OSTimeDlyHMSM(0, 0, 1, 200, 
                    OS_OPT_TIME_HMSM_STRICT, 
                    &err);  
      if (CAN_GetLastErrorCode(CAN1)==CAN_ErrorCode_NoErr) 
        autoInitState = 1;
      //else repeat state 0
    }
    //else repeat state 0
    
    break;
    
  case 1:// determine whether this cell is temporary master as it has the lowest random ID
    
    
    thisCellIsMaster=true;
    
    for (Int8U counter = 0; counter < receivedIDs; counter++)
    {
      if (IDList[counter]< SystemID) thisCellIsMaster=false;
    }
    currentPivot=SystemID;
    if (thisCellIsMaster) {
      self.isTempMaster=true;
      rightEnd->ID=currentPivot;
      rightEnd->next=0;
    }
    
    autoInitState = 2;
    break;
    
  case 2:// if master, create neighbor list
    lastDetected=detectedIDs;
    if(self.isTempMaster)
    {
      NUMOFNODES=receivedIDs+1;
      OSTimeDlyHMSM(0, 0, 0, 500, 
                    OS_OPT_TIME_HMSM_STRICT, 
                    &err);
      
      SendPulseDown();
      
      OSTimeDlyHMSM(0, 0, 1, 0, 
                    OS_OPT_TIME_HMSM_STRICT, 
                    &err);
      
      while (lastDetected<detectedIDs)
        // a right neighbor was identified, go on
      {	
        lastDetected=detectedIDs;
        PulseSendCommand(currentPivot);
        OSTimeDlyHMSM(0, 0, 1, 0, 
                      OS_OPT_TIME_HMSM_STRICT,&err);
      }
      
      // no more cells can be detected on the right
      addToRight=false;
      
      while(detectedIDs < receivedIDs)
      {
        // go on with a random cell from the left
        for (Int8U counter = 0; counter < receivedIDs; counter++)
        {
          if (!IDinList(rightRoot,IDList[counter]))
          {
            currentPivot=IDList[counter];
          }
        }
        detectedIDs++;
        
        do // go on from left to right
        {	
          //lastDetected=detectedIDs;
          leftEnd->ID=currentPivot;
          leftEnd->next=0;
          PulseSendCommand(currentPivot);
          OSTimeDlyHMSM(0, 0, 1, 0, 
                        OS_OPT_TIME_HMSM_STRICT,&err);
        }
        while (!neighborTrigger);
        neighborTrigger=false;
        MergeLeftIntoRight();
      }
      autoInitState = 3;
    }
    else
    {
      SwitchConfig(INITRECEIVE);
      autoInitState = 3;
    }
    
    break;	
  case 3:// if master, create neighbor list and send it to all nodes
    if(self.isTempMaster)
    {
      outcounter=0;
      currentPos=rightRoot;
      
      while (currentPos!=0)
      {
        if (currentPos->ID==SystemID) 
        {
          myRealID=outcounter; 
        }
        else
        {
          IDAssignCommand(currentPos->ID, outcounter);
        }
        outcounter++;
        currentPos=currentPos->next;
        
      }
      setSelfID(myRealID, NUMOFNODES);
    }
    //autoInitState = 4;
    break;	
  }
}

void PulseSendCommand(Int8U cellID)
{	
  CanTxMsg canMessage;
  
  Int16U msg_type = MSG_TYPE_AUTODETECT_PULSE;	        // TYPE PulseSendCommand
  Int8U msg_target = cellID;                            // Target is cellID
  Int8U msg_origin = SystemID;                          // origin is SystemID	
  canMessage.StdId = 0;
  canMessage.ExtId = (msg_type<<16) | (msg_target<<8) | (msg_origin);
  canMessage.RTR = CAN_RTR_DATA;
  canMessage.IDE = CAN_ID_EXT;
  canMessage.DLC = 0;
  
  CAN_Transmit(CAN1, &canMessage);
}

void ProcessPulseSendCommand(CanRxMsg *canMsg)
{
  Int8U targetID = (canMsg->ExtId>>8)&0xFF;
  if (autoInitState == 3)
  {
    // send Pulse if tempMaster told this cell to do so
    if (SystemID == targetID)
      SendPulseDown();				
  }
}

void IDAssignCommand(Int8U tempID, Int8U realID)
{	
  OS_ERR err;
  CanTxMsg canMessage;
  
  Int16U msg_type = MSG_TYPE_AUTODETECT_ASSIGN;	        // TYPE IDAssignCommand
  Int8U msg_target = tempID;                            // Target is tempID
  Int8U msg_origin = SystemID;                          // origin is SystemID	
  canMessage.StdId = 0;
  canMessage.ExtId = (msg_type<<16) | (msg_target<<8) | (msg_origin);
  canMessage.RTR = CAN_RTR_DATA;
  canMessage.IDE = CAN_ID_EXT;
  canMessage.DLC = 2;
  
  canMessage.Data[0] = realID;
  canMessage.Data[1] = NUMOFNODES; 
  
  CAN_Transmit(CAN1, &canMessage);
  
  OSTimeDlyHMSM(0, 0, 0, 200, 
                    OS_OPT_TIME_HMSM_STRICT,&err);
}

void ProcessIDAssignCommand(CanRxMsg *canMsg)
{
  Int8U targetID = (canMsg->ExtId>>8) & 0xFF;
  
  if (autoInitState == 3)
  {
    // replace tempID with assigned ID
    if (SystemID == targetID) 
    {
      setSelfID(canMsg->Data[0], canMsg->Data[1]);
    }
  }
}

void ProcessInitMessage(CanRxMsg *canMsg)
{
  Int8U origin = (canMsg->ExtId)&0xFF;

  if(autoInitState<1)
  {
    IDList[receivedIDs] = origin;
    receivedIDs++;
  }
  
  if( (autoInitState == 2) && self.isTempMaster )
  {		
    if(addToRight)
    {
      if ( (!IDinList(leftRoot,origin)) && (!IDinList(rightRoot,origin)) )
      {
        rightEnd->ID=currentPivot;
        rightEnd->next=(struct listNode *) malloc( sizeof(struct listNode) );
        rightEnd=rightEnd->next;
        rightEnd->next=0;
        rightEnd->ID=origin;
        currentPivot=origin;
        detectedIDs++;
      }
    }
    else
    {
      if(origin == rightRoot->ID) // pulse sent to root of Right, therefore we have to see if we are done
      {
        neighborTrigger=true;
      }
      else // continue detection until rightRoot is hit
      {
        if ( (!IDinList(leftRoot,origin)) && (!IDinList(rightRoot,origin)) )
        {
          leftEnd->ID=currentPivot;
          leftEnd->next=(struct listNode *) malloc( sizeof(struct listNode) );
          leftEnd=leftEnd->next;
          leftEnd->next=0;
          leftEnd->ID=origin;
          currentPivot=origin;
          detectedIDs++;
        }
      }
    }
  }
  return;
}

bool IDinList(struct listNode * List, Int8U searchID)
{
  if (List->ID==searchID)
    return true;	
  List=List->next;
  while (List!=0)
  {
    if (List->ID==searchID)
      return true;	
    else List=List->next;
  }
  return false;
}

void MergeLeftIntoRight(void)
{
  leftEnd->next=rightRoot;
  rightRoot=leftRoot;
  leftRoot=(struct listNode *) malloc( sizeof(struct listNode) );
  leftEnd=leftRoot;
  leftRoot->next=0;
  leftRoot->ID=0;
  return;
}

void UserSetID(void)
{
  OS_ERR err;
  Int32U now=OSTimeGet(&err);
  // check that 5s have passed since last keypress and we are still UNDEFINED
  if ((packInfo.status[self.id] == UNDEFINED) && ((now - LastButtonTS) > 5000))
  {
    self.id = SystemID;
    packInfo.status[self.id] = OFF;
    ScreenToShow = 1;
  }
}

void setSelfID(Int8U selfID, Int8U numnodes)
{
  SwitchConfig(OFF);
  self.id = selfID;
  SystemID = self.id;
  CAN1_Set_Filter(self.id);
  packInfo.status[self.id] = OFF;
  NUMOFNODES = numnodes;
  ScreenToShow = 1;
  ID_AUTODETECT = false;
  smartCellStart();
}