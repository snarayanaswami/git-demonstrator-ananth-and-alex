#ifndef UI_timer_h
#define UI_timer_h

#include <includes.h>

void initITimer(void);
void TIMER5_IRQHandler(void);
void setIFrequency(Int32U frequency);
void enableITimer(void);
void disableITimer(void);

void initUTimer(void);
void TIMER6_IRQHandler(void);
void setUFrequency(Int32U frequency);
void enableUTimer(void);
void disableUTimer(void);

void initKFTimer(void);
void TIMER7_IRQHandler(void);
void setKFFrequency(Int32U frequency);
void enableKFTimer(void);
void disableKFTimer(void);

#endif