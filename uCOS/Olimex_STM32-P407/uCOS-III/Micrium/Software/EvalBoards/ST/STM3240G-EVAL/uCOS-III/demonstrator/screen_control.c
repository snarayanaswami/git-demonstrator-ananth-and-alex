/**
 *********************************************************************************************************
 *
 * @file    screen_control.c
 * @author  Arne Meeuw
 * @version V0.0.1
 * @date    2-January-2015
 * @brief   Control of display and screens to show
 * 
 *********************************************************************************************************
 */ 

#include <screen_control.h>

extern packInfoType packInfo;
extern nodeInfoType self;
extern Int8U NUMOFNODES;
extern Int8U SystemID;

extern Int8U ScreenToShow;

extern Int8U ID_AUTODETECT;

#define VHISTITEMS 132
extern float voltageHistory[];
extern Int16S voltageHistoryOffset;

#define SHISTITEMS 132
extern float socHistory[];
extern Int16S socHistoryOffset;

#define CHISTITEMS 132
extern float currentHistory[];
extern Int16S currentHistoryOffset;


extern Int8U neighborTrigger;
extern Int8U detectedIDs;
extern Int8U currentPivot;
extern Int8U receivedIDs;
extern Int8U IDList[MAXNUMOFNODES];
extern Int8U autoInitState;
extern Int8U addToRight;
extern struct listNode *rightRoot;
extern struct listNode *leftRoot;

Int32U timer_counter;

char buf[32];

void ShowStatusSprite(Int8U status)
{
  Int8U cellPosX=50;
  Int8U cellPosY=5;
  if (ScreenToShow == 1)
  {
  switch(status) 
  {
    case OFF:
#ifndef BOARD_TUM
      BufLCDshowSpriteBMP12bit( &ToDrawScreen, cellPosX, cellPosY, BATTERY_OFF, 0, 32, 32, &status_icons);
#else
      Position(1,1);
      Displ_Char(0x00);
      Position(16,1);
      Displ_Char(0x00);
      Position(1,2);
      Displ_Char(0x00);
      Position(16,2);
      Displ_Char(0x00);
#endif
      break;
      
    case SEND_UP:
#ifndef BOARD_TUM
      BufLCDshowSpriteBMP12bit( &ToDrawScreen, cellPosX, cellPosY, BATTERY_HIGH, 0, 32, 32, &battery_icons);
      BufLCDshowSpriteBMP12bit( &ToDrawScreen, cellPosX-32, cellPosY, ARROW_LEFT, 0, 32, 32, &battery_icons);
#else
      Position(1,1);
      Displ_Char(0x7F);
      Position(1,2);
      Displ_Char(0x7F);
#endif
      break;
      
    case SEND_DOWN:
#ifndef BOARD_TUM
      BufLCDshowSpriteBMP12bit( &ToDrawScreen, cellPosX, cellPosY, BATTERY_HIGH, 0, 32, 32, &battery_icons);
      BufLCDshowSpriteBMP12bit( &ToDrawScreen, cellPosX+32, cellPosY, ARROW_RIGHT, 0, 32, 32, &battery_icons);
#else
      Position(16,1);
      Displ_Char(0x7E);
      Position(16,2);
      Displ_Char(0x7E);
#endif
      break;
      
    case RECEIVE_UP:
#ifndef BOARD_TUM
      BufLCDshowSpriteBMP12bit( &ToDrawScreen, cellPosX, cellPosY, BATTERY_LOW, 0, 32, 32, &battery_icons);
      BufLCDshowSpriteBMP12bit( &ToDrawScreen, cellPosX-32, cellPosY, ARROW_RIGHT, 0, 32, 32, &battery_icons);
#else
      Position(1,1);
      Displ_Char(0x7E);
      Position(1,2);
      Displ_Char(0x7E);
#endif
      break;
      
    case RECEIVE_DOWN:
#ifndef BOARD_TUM
      BufLCDshowSpriteBMP12bit( &ToDrawScreen, cellPosX, cellPosY, BATTERY_LOW, 0, 32, 32, &battery_icons);
      BufLCDshowSpriteBMP12bit( &ToDrawScreen, cellPosX+32, cellPosY, ARROW_LEFT, 0, 32, 32, &battery_icons);
#else
      Position(16,1);
      Displ_Char(0x7F);
      Position(16,2);
      Displ_Char(0x7F);
#endif
      break;
      
    case BLOCKED:
#ifndef BOARD_TUM
      BufLCDshowSpriteBMP12bit( &ToDrawScreen, cellPosX, cellPosY, BATTERY_OFF, 0, 32, 32, &status_icons);
      BufLCDshowSpriteBMP12bit( &ToDrawScreen, cellPosX+32, cellPosY, ICON_BLOCKED, 0, 32, 32, &status_icons);
      BufLCDshowSpriteBMP12bit( &ToDrawScreen, cellPosX-32, cellPosY, ICON_BLOCKED, 0, 32, 32, &status_icons);
#else
      Position(1,1);
      Displ_Char(0xDB);
      Position(16,1);
      Displ_Char(0xDB);
      Position(1,2);
      Displ_Char(0xDB);
      Position(16,2);
      Displ_Char(0xDB);
#endif
      break;
      
     case BLOCKED_R:
#ifndef BOARD_TUM
      BufLCDshowSpriteBMP12bit( &ToDrawScreen, cellPosX, cellPosY, BATTERY_OFF, 0, 32, 32, &status_icons);
      BufLCDshowSpriteBMP12bit( &ToDrawScreen, cellPosX+32, cellPosY, ICON_BLOCKED, 0, 32, 32, &status_icons);
#else
      Position(16,1);
      Displ_Char(0xDB);
      Position(16,2);
      Displ_Char(0xDB);
#endif
      break;
      
     case BLOCKED_L:
#ifndef BOARD_TUM
      BufLCDshowSpriteBMP12bit( &ToDrawScreen, cellPosX, cellPosY, BATTERY_OFF, 0, 32, 32, &status_icons);
      BufLCDshowSpriteBMP12bit( &ToDrawScreen, cellPosX-32, cellPosY, ICON_BLOCKED, 0, 32, 32, &status_icons);
#else
      Position(1,1);
      Displ_Char(0xDB);
      Position(1,2);
      Displ_Char(0xDB);
#endif
      break;
      
    case FWD_MID:
#ifndef BOARD_TUM
      BufLCDshowSpriteBMP12bit( &ToDrawScreen, cellPosX, cellPosY, ICON_FWD_MID, 0, 32, 32, &status_icons);
#else
      Position(1,1);
      Displ_Char(0x3D);
      Position(16,1);
      Displ_Char(0x3D);
      Position(1,2);
      Displ_Char(0x3D);
      Position(16,2);
      Displ_Char(0x3D);
#endif
      break;
      
    case FWD_END:
#ifndef BOARD_TUM
      BufLCDshowSpriteBMP12bit( &ToDrawScreen, cellPosX, cellPosY, ICON_FWD_END, 0, 32, 32, &status_icons);
#else
      Position(1,1);
      Displ_Char(0x3D);
      Position(16,1);
      Displ_Char(0x3D);
      Position(1,2);
      Displ_Char(0x3D);
      Position(16,2);
      Displ_Char(0x3D);
#endif
      break;
    }
  }
}
/**
 * @brief Show a diagram (value over time axis) with given height
 * 
 * @param[in]   yPosition       Vertical position where to beginn drawing the diagram
 * @param[in]   height          Vertical size of diagram
 * @param[in]   *History        Pointer to data array
 * @param[in]   length          Number of elements in data array
 * @param[in]   offset          Offset for displaying current value in diagram
 * @param[in]   unitSymbol      Symbol to show vor displaying unit values
 * 
 */
void ShowDiagram(Int8U yPosition, Int8U height, float * History, Int8U length, Int8U offset, char unitSymbol)
{
  float minRange=0.01;
  
  char buf[32];
  float vMax=-100;
  float vMin=100;
  Int8U yDrawPos=0;
  Int8U lastyDrawPos=0;
  Int8U counter=0;
  Int8U start=0;
  Int8U end=0;

  float vRange;
  float vRangeShow;
  float diff;

  //calculate min and max value in history array
  for (counter=0;counter<length;counter++)
  {
    float value=History[counter];
    if (value!=-100)
    {
      if ( value<vMin) vMin = value;
      if ( value>vMax) vMax = value;
    }
  }

  vRange=vMax-vMin;
  vRangeShow=vRange;
  
  diff = minRange-vRangeShow;
  if (diff > 0)
  {
    vMax=vMax+(0.5*diff);
    vMin=vMin-(0.5*diff);
    vRange=vMax-vMin;
  }
  
  for (counter=0;counter<length;counter++)
  {
    if (History[(offset + counter + 1) % length]!=-100)
    {
      lastyDrawPos=(Int8U) (yPosition+(((vMax-History[(offset + counter) % length])/vRange)*height));
      yDrawPos=(Int8U) (yPosition+(((vMax-History[(offset + counter + 1) % length])/vRange)*height));
      
      if (counter>0)
      {
        if (lastyDrawPos<=yDrawPos)
        {
          start=lastyDrawPos;
          end=yDrawPos;
        }
        else
        {
          start=yDrawPos;
          end=lastyDrawPos;
        }
        BufLCDcolorFill(&ToDrawScreen, length-1-counter, start, 1, end-start+1, 0x00f);
      }
      else
        BufLCDsetPixel(&ToDrawScreen, length-1-counter, yDrawPos, 0x00f);  
    }
  }
  sprintf( buf, "%0.3f%c", vMax, unitSymbol);
  BufLCDprintStringBG( &ToDrawScreen, buf, 1, yPosition-10, 0x000, 0xfff);
  BufLCDcolorFill(&ToDrawScreen, 0, yPosition, 10, 1, 0xc00);
  sprintf( buf, "%0.3f%c", vMin, unitSymbol);
  BufLCDprintStringBG( &ToDrawScreen, buf, 1, yPosition+height+2, 0x000, 0xfff);
  BufLCDcolorFill(&ToDrawScreen, 0, yPosition+height, 10, 1, 0x0c0);
}

/**
 * @brief Show screen 0 (Debug)
 * 
 * Screen shows:
 * - <code>self.ID, packInfo.status[self.id], SystemID</code>
 * - <code>packInfo.status[0] .. packInfo.status[5]</code>
 * - <code>self.voltage, globDiffVoltage</code>
 *
 */
void ShowScreen_0(void) // DEBUG SCREEN
{
  sprintf( buf, "DEBUG INFO");
  BufLCDprintStringBG( &ToDrawScreen, buf, 10, 60, 0x000, 0xfff);

  sprintf( buf, "ID: %u, St: %u, Bu: %u", self.id, packInfo.status[self.id], SystemID);
  BufLCDprintStringBG( &ToDrawScreen, buf, 10, 80, 0x000, 0xfff);
  sprintf( buf, "IM: %u PS: %u %u %u %u", packInfo.status[0], packInfo.status[1], packInfo.status[2], packInfo.status[3], packInfo.status[4]);
  //sprintf( buf, "IM: %u PS: %u %u %u", self.isMaster, packInfo.status[0], packInfo.status[1], packInfo.status[2]);
  BufLCDprintStringBG( &ToDrawScreen, buf, 10, 90, 0x000, 0xfff);

  sprintf( buf, "V : %0.3f, D: %0.3f", self.voltage, globDiffVoltage);
  BufLCDprintStringBG( &ToDrawScreen, buf, 10, 100, 0x000, 0xfff);
}

/**
 * @brief Show screen 1 (Demo screen)
 * 
 * Screen shows:
 * - <code>self.ID</code>
 * - <code>self.voltage/code>
 * - <code>Voltage diagram</code>
 *
 */

void ShowScreen_1(void) // Standardscreen
{
#ifndef BOARD_TUM
  sprintf( buf, "%u", self.id);
  BufLCDprintStringBGZoom( &ToDrawScreen, buf, 2, 4, 0x000, 0xfff);  
  
  sprintf( buf, "%0.2f V", self.voltage);
  
  BufLCDprintStringBGZoom( &ToDrawScreen, buf, 30, 45, 0x000, 0xfff);  
  
  ShowDiagram(80, 40, voltageHistory, VHISTITEMS, voltageHistoryOffset, 'V');
#else
                
  sprintf( buf, " Cell %u / %u ", self.id+1, NUMOFNODES);
  Position(2,1);
  Displ_String(buf);
              //"................" <- 16 characters
  sprintf( buf, " Batt: %1.2f V ", self.voltage);
  Position(2,2);
  Displ_String(buf);
#endif
}

/**
 * @brief Show screen 2 (Voltage & SOC diagram)
 * 
 * Screen shows:
 * - <code>self.ID</code>
 * - <code>self.voltage</code>
 * - <code>Voltage diagram</code>
 * - <code>packInfo.soc[self.id]</code>
 * - <code>SOC diagram</code>
 *
 */
void ShowScreen_2(void) // Standardscreen
{
  sprintf( buf, "%u", self.id);
  BufLCDprintStringBGZoom( &ToDrawScreen, buf, 2, 4, 0x000, 0xfff);  

  sprintf( buf, "%0.2fV", self.voltage);
  BufLCDprintStringBGZoom( &ToDrawScreen, buf, 55, 4, 0x000, 0xfff);  
  ShowDiagram(35, 30, voltageHistory, VHISTITEMS, voltageHistoryOffset, 'V');
  
  sprintf( buf, "%0.1f%%", packInfo.soc[self.id]*100);
  BufLCDprintStringBGZoom( &ToDrawScreen, buf, 55, 70, 0x000, 0xfff);  
  ShowDiagram(90, 30, socHistory, SHISTITEMS, socHistoryOffset, '%');
}

/**
 * @brief Show screen 3 (Autodetect debug)
 * 
 * Screen shows:
 * - <code>SystemID, detectedIDs</code>
 * - <code>self.isTempMaster, currentPivot</code>
 * - <code>IDList</code>
 * - <code>autoInitState, receivedIDs, addToRight</code>
 *
 */
void ShowScreen_3(void) // AutoDetect debug screen
{
#ifndef BOARD_TUM
  sprintf( buf, "AUTO DETECT INFO:");
  BufLCDprintStringBG( &ToDrawScreen, buf, 10, 60, 0x000, 0xfff);
  
  sprintf( buf, "TID:%u, DIDs:%u", SystemID, detectedIDs);
  BufLCDprintStringBG( &ToDrawScreen, buf, 10, 80, 0x000, 0xfff);
  
  sprintf( buf, "TM: %u, CP: %u", self.isTempMaster, currentPivot);
  BufLCDprintStringBG( &ToDrawScreen, buf, 10, 110, 0x000, 0xfff);
  
  for (Int8U counter = 0; counter < receivedIDs; counter++)
  {
    sprintf( buf, "%u", IDList[counter]);
    BufLCDprintStringBG( &ToDrawScreen, buf, 10+(counter * 3*8), 90, 0x000, 0xfff);
  }
  sprintf( buf, "AIS:%u RIDs:%u DR:%u", autoInitState, receivedIDs, addToRight);
  BufLCDprintStringBG( &ToDrawScreen, buf, 10, 100, 0x000, 0xfff);
  
  if(self.isTempMaster)
  {
    Int8U outcounter=0;
    struct listNode * currentPos=rightRoot;
    
    while (currentPos!=0)
    {
      sprintf( buf, "%u", currentPos->ID);
      BufLCDprintStringBG( &ToDrawScreen, buf, 10+(outcounter * 3*8), 40, 0x000, 0xfff);
      outcounter++;
      currentPos=currentPos->next;
    }
    outcounter=0;
    currentPos=leftRoot;
    
    while (currentPos!=0)
    {
      sprintf( buf, "%u", currentPos->ID);
      BufLCDprintStringBG( &ToDrawScreen, buf, 10+(outcounter * 3*8), 30, 0x000, 0xfff);
      outcounter++;
      currentPos=currentPos->next;
    }
  }
#else

#endif
}

/**
 * @brief Show screen 4 (Various values debug)
 * 
 * Screen shows:
 * - <code>packInfo.status[self.id]</code>
 * - <code>self.blockerID</code>
 * - <code>self.id</code>
 * - <code>packInfo.soc[self.id]</code>
 * - <code>self.voltage</code>
 * - <code>self.voltage_est</code>
 * - <code>self.current</code>
 *
 */
void ShowScreen_4(void) // Various values debug-screen
{
  sprintf( buf, "Status: %u", packInfo.status[self.id]);
  BufLCDprintStringBG( &ToDrawScreen, buf, 10, 10, 0x000, 0xfff);
 
  sprintf( buf, "BlockerIDs: %u, %u", self.blockerIDs[0], self.blockerIDs[1]);
  BufLCDprintStringBG( &ToDrawScreen, buf, 10, 25, 0x000, 0xfff);
  
  sprintf( buf, "CellID: %u", self.id);
  BufLCDprintStringBG( &ToDrawScreen, buf, 10, 40, 0x000, 0xfff);
  
  sprintf( buf, "NUMOFNODES: %u", NUMOFNODES);
  BufLCDprintStringBG( &ToDrawScreen, buf, 10, 55, 0x000, 0xfff);
  
  sprintf( buf, "SOC: %0.2f%%", packInfo.soc[self.id]*100);
  BufLCDprintStringBG( &ToDrawScreen, buf, 10, 70, 0x000, 0xfff);      
  
  sprintf( buf, "Voltage: %0.2fV", self.voltage);
  BufLCDprintStringBG( &ToDrawScreen, buf, 10, 85, 0x000, 0xfff);  
  
  sprintf( buf, "~Voltage: %0.2fV", self.voltage_est);
  BufLCDprintStringBG( &ToDrawScreen, buf, 10, 100, 0x000, 0xfff);    
  
  sprintf( buf, "Current: %0.2f", self.current);
  BufLCDprintStringBG( &ToDrawScreen, buf, 10, 115, 0x000, 0xfff);     
}

void ShowScreen_5(void)
{
  ShowDiagram(15, 20, voltageHistory, VHISTITEMS, voltageHistoryOffset, 'V');
  
  ShowDiagram(60, 20, currentHistory, CHISTITEMS, currentHistoryOffset, 'A');
  
  ShowDiagram(105, 15, socHistory, SHISTITEMS, socHistoryOffset, '%');
}