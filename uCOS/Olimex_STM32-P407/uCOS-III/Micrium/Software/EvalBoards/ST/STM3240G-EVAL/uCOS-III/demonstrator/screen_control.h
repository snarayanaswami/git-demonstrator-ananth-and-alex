#ifndef screen_control_h
#define screen_control_h


#include <includes.h>
#include <float.h>

void ShowStatusSprite(Int8U status);
void ShowVoltageDiagram(Int8U yPosition, Int8U height);

void ShowScreen_0(void);
void ShowScreen_1(void);
void ShowScreen_2(void);
void ShowScreen_3(void);
void ShowScreen_4(void);
void ShowScreen_5(void);

#endif