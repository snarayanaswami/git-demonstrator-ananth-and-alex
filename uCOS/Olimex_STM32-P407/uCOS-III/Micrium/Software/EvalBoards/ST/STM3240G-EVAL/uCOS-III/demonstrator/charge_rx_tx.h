#ifndef charge_rx_tx_h
#define charge_rx_tx_h

#include <includes.h>

void SendCharge(Int8U status);
void SendPulseDown(void);
void ReceiveCharge(void);
void GotoIdleMode(void);
Int8U getRole(Int8U ID, transactionInfoType * transaction);

#endif