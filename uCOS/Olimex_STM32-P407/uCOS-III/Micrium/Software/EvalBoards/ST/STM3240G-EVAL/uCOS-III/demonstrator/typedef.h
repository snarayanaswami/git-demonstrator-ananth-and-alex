/**
 *********************************************************************************************************
 *
 * @file    typedef.h
 * @author  Arne Meeuw
 * @version V0.0.1
 * @date    2-January-2015
 * @brief   Definitions of datatypes
 * 
 *********************************************************************************************************
 */ 

#ifndef typedef_h
#define typedef_h

#include <includes.h>

#define MAXNUMOFNODES 10

/** @brief Typedefinition for matrices */
typedef struct matrixType
{
  Int16U numRows;               ///< number of rows of the matrix
  Int16U numCols;               ///< number of columns of the matrix
  float *pData;                 ///< points to the data of the matrix
}matrixType;

/** @brief Typedefinition for datastructure used in kalman filter */
typedef struct {
  Int16U n;                     ///< state vector order
  Int16U q;                     ///< output vector order
  Int16U p;                     ///< input vector order
  matrixType Sw;                ///< process noise covariance
  matrixType Sv;                ///< measurement noise covariance
  matrixType Sk;                ///< estimation error covariance
  matrixType x;                 ///< state vector
  matrixType L;                 ///< kalman gain
  matrixType Ak;                ///< Jacobian Matrix A
  matrixType Ck;                ///< Jacobian Matrix C
  matrixType uk;                ///< Input memory matrix
} kalmanType;


typedef float voltageType;      ///< Cell voltage
typedef double currentType;     ///< Cell current
typedef double socType;         ///< Cell SOC
typedef Int8U idType;           ///< Cell ID (0 - 255)
typedef Int8U statusType;       ///< Cell Status (see enum 'statusvalue')
typedef Int8U modeType;         ///< Cell mode (see enum 'modevalue')

typedef statusType statusArrayType[MAXNUMOFNODES];
typedef voltageType voltageArrayType[MAXNUMOFNODES];
typedef socType socArrayType[MAXNUMOFNODES];
typedef modeType modeArrayType[MAXNUMOFNODES];

extern float globDiffVoltage;

/** @brief Typedef describing a proposed -transaction */
typedef struct
{
  idType senderID;              ///< ID of cell who is sending charge
  idType receiverID;            ///< ID of cell who is receiving charge
  bool blockSuccessful;         ///<
  float transferTime;           ///< Duration of balancing transaction
  float transferRate;           ///< Current of balancing transaction
}transactionInfoType;

/** @brief Typedef for responses to blockrequests containing the blocker-cell ID */
typedef struct
{
  idType origin;                ///< ID of origin
  idType target;                ///< ID of target
  idType status;                ///< Origin's status
  idType blockerID0;            ///< ID of blocker0
  idType blockerID1;            ///< ID of blocker1
}BlockResponseType;

/** 
 * @brief Typedef for broadcasting settings
 *
 */
typedef struct
{
  bool  minBroadcast;                   ///< 
  bool  maxBroadcast;                   ///< 
  bool  allBroadcast;                   ///< 
  float broadcastPeriod;                ///< 
  bool  limitBroadcast;                 ///< 
  float limitBroadcastBound;            ///< 
  bool  broadcastAfterTransaction;      ///< 
  bool  broadcastDebugStatus;           ///< 
}broadcastSettingsType;

/** 
 * @brief Typedef for UIKF timer settings
 *
 */
typedef struct
{
  bool   uTimerEnabled;         ///< 
  Int32U uFrequency;            ///< 
  Int32U uFilter;               ///< 
  bool   iTimerEnabled;         ///< 
  Int32U iFrequency;            ///< 
  Int32U iFilter;               ///< 
  bool   kfTimerEnabled;        ///< 
  Int32U kfFrequency;           ///< 
  Int32U kfFilter;              ///< 
}timerSettingsType;

/** 
 * @brief Typedef for PWM timer settings
 *
 */
typedef struct
{
  Int16U frequency;          ///< 
  Int16U numberOfPulses;     ///< 
  Int16U duration;           ///< 
  Int8U  chargePercent;      ///< 
  Int8U  deadtimePercent;    ///< 
  Int8U  dischargePercent;   ///<  
}pwmSettingsType;

/**
 * @brief Typedef for balancing settings
 *
 */
typedef struct
{
  bool    OCVbalancing;         ///< 
  float   OCVlatency;           ///< 
  OS_TICK lastTransaction;      ///< 
  bool    balancingEnabled;     ///< Status if balancing is active
  Int8U   balancingStrategy;    ///< Cell's pursued balancing strategy
  bool    monitorEnabled;       ///< Status if monitoring is active
  float   startDiff;            ///< Upper bound of monitor (start balancing)
  float   stopDiff;             ///< Lower bound of monitor (stop balancing)
}balancingSettingsType;

/** @brief  Typedef for self-description of cell-parameters */
typedef struct
{
  idType id;                    ///< Cell ID (0 - 255)
  
  voltageType voltage_est;      ///< Cell voltage estimated by model
  currentType current;          ///< Cell current
  socType soc;                  ///< Cell state of charge
  voltageType voltage;          ///< Actual cell voltage (high update rate)
  
  idType blockerIDs[2];         ///< Blocker IDs [(0 - 255), (0 - 255)]
  idType blockers;              ///< Amount of blockers
  
  bool receiveActive;           ///< Flag if receiving is active
  bool isTempMaster;            ///< Needed for ID AUTO-ASSIGNING
  
  broadcastSettingsType broadcastSettings;
  balancingSettingsType balancingSettings;
  timerSettingsType     timerSettings;
  pwmSettingsType       pwmSettings;
}nodeInfoType;

/** @brief Typedef containing information about the batterypack */
typedef struct
{
  socArrayType     soc;         ///< Array containing the pack cell's state of charge
  voltageArrayType voltage;     ///< Array containing the pack cell's voltage
  statusArrayType  status;      ///< Array containing the pack cell's status (switch status)
}packInfoType;

typedef struct
{
  Int8U data[32];
  Int8U length;
}UARTRxMsg;

/** 
 * @brief Cell Status
 *
 * Status containing switch (MOSFET) settings
 *
 */
enum statusvalue{ 
OFF                           = 0x00,                   ///<  [0] All switches off
SEND_UP                       = 0x01,                   ///<  [1] receiverID < senderID
SEND_DOWN                     = 0x02,                   ///<  [2] receiverID > senderID
RECEIVE_UP                    = 0x03,                   ///<  [3] receiverID > senderID
RECEIVE_DOWN                  = 0x04,                   ///<  [4] receiverID < senderID
FWD_START                     = 0x05,                   ///<  [5] Open forwarding loop
FWD_MID                       = 0x06,                   ///<  [6] Forward charge
FWD_END                       = 0x07,                   ///<  [7] Close forwarding loop
UNDEFINED                     = 0x08,                   ///<  [5] undefined setting
INITSEND                      = 0x09,                   ///<  [8] Send pulse for ID autodetect
INITRECEIVE                   = 0x0A,                   ///< [10] Receive pulse for ID autodetect
INITDISCHARGE                 = 0x0C,                   ///< [12] 

BLOCKED_R                     = 0x0B,                   ///< [0x0B] Switches off, blocked from right
BLOCKED_L                     = 0xB0,                   ///< [0xB0] Switches off, blocked from left
BLOCKED                       = 0xBB,                   ///< [0xBB] Switches off, blocked from both sides
PROCESSING                    = 0xCC                    ///< [0xCC] Switches off, processing of request
};

/** 
 * @brief Balancingstrategy naming enumeration
 *
 */
enum strategy{
BAL_NONE                      = 0x00,
BAL_MINIMUM                   = 0x01,
BAL_MAXIMUM                   = 0x02,
BAL_MINMAX                    = 0x03,
BAL_BELOW_AVERAGE             = 0x04
};


/**
 * @brief CAN message types defined by extended 29 bit CAN identifier
 *
 * A message consists of a 29-bit identifier followed by up to 8 data bytes.
 * - Bits **0 to 2** are not used and therefore *0*
 * - Bits **3 to 16** determine *typification* as described in this enum.
 * - Bits **17 to 24** determine *target-ID* of the message (i.e. recipient)
 * - Bits **25 to 32** determine *origin-ID* of the message (i.e. despatcher)
 * 
 * |Bits:|[3 .. 16]|[17 .. 24]|[25 .. 32]|[0       ..      64]|
 * |:---:|:-------:|:--------:|:--------:|:------------------:|
 * |Function:|[type]|[target]|[origin]|[data(0) .. data(7)]|
 * 
 */
enum msg_type
{
MSG_TYPE_NONE                 = 0x0000,                 ///< [0x0000][][]	No type

// BLOCK/UNBLOCK/RESPONSE      "0x000x" `               
MSG_TYPE_UNBLOCK              = 0x0001,                 ///< [0x0002][0xFF][self.id] [senderID][receiverID]
MSG_TYPE_BLOCK 	              = 0x0002,                 ///< [0x0001][0xFF][self.id] [senderID][receiverID]
MSG_TYPE_STATUS_RESPONSE      = 0x0003,                 ///< [0x0003][targetID][self.id] [status] [blockerID0] [blockerID1]

// PARAMETER BROADCASTS        "0x001x"                 
MSG_TYPE_SOC 		      = 0x0010,                 ///< [0x0010][0xFF][self.id] [SoC(0)][SoC(1)][SoC(2)][SoC(3)]
MSG_TYPE_VOLTAGE              = 0x0011,                 ///< [0x0011][0xFF][self.id] [Voltage(0)][Voltage(1)][Voltage(2)][Voltage(3)]
MSG_TYPE_TEMPERATURE	      = 0x0012,                 ///< [0x0012][0xFF][self.id] [Temperature(0)][Temperature(1)][Temperature(2)][Temperature(3)]

// REQUESTS / ACKNOWLEDGES     "0x002x"
MSG_TYPE_SEND_REQ             = 0x0020,                 ///< [0x0020][senderID][self.id]
MSG_TYPE_SEND_ACK             = 0x0021,                 ///< [0x0021][receiverID][self.id]
MSG_TYPE_EXT_SEND_REQ         = 0x0022,                 ///< [0x0022][senderID][self.id]   [rate(0)][rate(1)][rate(2)][rate(3)] [time(0)][time(1)][time(2)][time(3)]
MSG_TYPE_EXT_SEND_ACK         = 0x0023,                 ///< [0x0023][receiverID][self.id] [rate(0)][rate(1)][rate(2)][rate(3)] [time(0)][time(1)][time(2)][time(3)]

// SETTING COMMANDS            "0x00xx"                 
MSG_TYPE_SET_BALANCING        = 0x0030,                 ///< [0x0030][targetID][self.id] [balancingEnabled]
MSG_TYPE_SET_STRATEGY         = 0x0031,                 ///< [0x0031][targetID][self.id] [balancingStrategy]

MSG_TYPE_SET_MONITOR          = 0x0032,                 ///< [0x0032][targetID][self.id] [monitorEnabled]
MSG_TYPE_SET_MONITOR_BOUND    = 0x0033,                 ///< [0x0033][targetID][self.id] [startDiff(0)][startDiff(1)][startDiff(2)][startDiff(3)] [stopDiff(0)][stopDiff(1)][stopDiff(2)][stopDiff(3)]

MSG_TYPE_SET_BC_SETTINGS      = 0x0034,                 ///< [0x0034][targetID][self.id] [minBroadcast] [maxBroadcast] [allBroadcast] [broadcastAfterTransaction] [limitBroadcast]  [broadcastDebugStatus]
MSG_TYPE_SET_BC_PERIOD        = 0x0035,                 ///< [0x0035][targetID][0xFF]  [PERIOD(0)][PERIOD(1)][PERIOD(2)][PERIOD(3)]
MSG_TYPE_SET_BC_BOUND         = 0x0036,                 ///< [0x0036][targetID][0xFF]  [BOUND(0)][BOUND(1)][BOUND(2)][BOUND(3)]

MSG_TYPE_SET_OCV_BALANCING    = 0x0037,                 ///< [0x0037][targetID][0xFF]  [OCVbalancing]
MSG_TYPE_SET_OCV_LATENCY      = 0x0038,                 ///< [0x0038][targetID][0xFF]  [LATENCY(0)][LATENCY(1)][LATENCY(2)][LATENCY(3)]

MSG_TYPE_SET_U_TIMER          = 0x0039,                 ///< [0x0039][targetID][0xFF]  [uTimerEnabled]
MSG_TYPE_SET_U_TIMER_VALUES   = 0x0040,                 ///< [0x0040][targetID][0xFF]  [FREQ(0)][FREQ(1)][FREQ(2)][FREQ(3)] [FILTER(0)][FILTER(1)][FILTER(2)][FILTER(3)]
MSG_TYPE_SET_I_TIMER          = 0x0041,                 ///< [0x0041][targetID][0xFF]  [iTimerEnabled]
MSG_TYPE_SET_I_TIMER_VALUES   = 0x0042,                 ///< [0x0042][targetID][0xFF]  [FREQ(0)][FREQ(1)][FREQ(2)][FREQ(3)] [FILTER(0)][FILTER(1)][FILTER(2)][FILTER(3)]
MSG_TYPE_SET_KF_TIMER         = 0x0043,                 ///< [0x0043][targetID][0xFF]  [kfTimerEnabled]
MSG_TYPE_SET_KF_TIMER_VALUES  = 0x0044,                 ///< [0x0044][targetID][0xFF]  [FREQ(0)][FREQ(1)][FREQ(2)][FREQ(3)] [FILTER(0)][FILTER(1)][FILTER(2)][FILTER(3)]

MSG_TYPE_SET_PWM_FREQUENCY    = 0x0045,                 ///< [0x0044][targetID][0xFF]  [FREQ(0)][FREQ(1)][FREQ(2)][FREQ(3)]
MSG_TYPE_SET_PWM_NO_PULSES    = 0x0046,                 ///< [0x0044][targetID][0xFF]  [PULSES(0)][PULSES(1)][PULSES(2)][PULSES(3)]
MSG_TYPE_SET_PWM_CHG_PRCNT    = 0x0047,                 ///< [0x0044][targetID][0xFF]  [PRCNT(0)][PRCNT(1)][PRCNT(2)][PRCNT(3)]
MSG_TYPE_SET_PWM_DT_PRCNT     = 0x0048,                 ///< [0x0044][targetID][0xFF]  [PRCNT(0)][PRCNT(1)][PRCNT(2)][PRCNT(3)]
MSG_TYPE_SET_PWM_DCHG_PRCNT   = 0x0049,                 ///< [0x0044][targetID][0xFF]  [PRCNT(0)][PRCNT(1)][PRCNT(2)][PRCNT(3)]

MSG_TIME_SET_PWM_PARAMS       = 0x0050,                 ///< [0x0044][targetID][0xFF]  [FREQ(0)][FREQ(1)][PULSES(0)][PULSES(1)][PRCNT_CHG(0)][PRCNT_DT(0)][PRCNT_DCHG(0)]



// AUTO DETECT MESSAGES        "0x00Ax"
MSG_TYPE_AUTODETECT_RANDOM    = 0x00A0,                 ///< [0x00A0][0xFF][randomID] [randomByte(0) .. randomByte(7)]
MSG_TYPE_AUTODETECT_PULSE     = 0x00A1,                 ///< [0x00A1][targetID][tempMasterID]
MSG_TYPE_AUTODETECT_ASSIGN    = 0x00A2,                 ///< [0x00A2][targetID][origin] [realID]


// DEBUG SECTION               "0x0Dxx"
MSG_TYPE_DEBUG_SOC            = 0x0D00,                 ///< [0x0D00][0xFF][self.id] [SoC(0)][SoC(1)][SoC(2)][SoC(3)]
MSG_TYPE_DEBUG_VOLTAGE        = 0x0D01,                 ///< [0x0D01][0xFF][self.id] [Voltage(0)][Voltage(1)][Voltage(2)][Voltage(3)]

MSG_TYPE_DEBUG_SET_SCREEN     = 0x0D10,                 ///< [0x0D10][targetID][0xFF] [ScreenToShow]
MSG_TYPE_DEBUG_SET_STATUS     = 0x0D11,                 ///< [0x0D11][targetID][0xFF] [status]
MSG_TYPE_DEBUG_SET_SOC        = 0x0D12,                 ///< [0x0D12][targetID][0xFF] [SoC(0)][SoC(1)][SoC(2)][SoC(3)]
MSG_TYPE_DEBUG_SET_VOLTAGE    = 0x0D13,                 ///< [0x0D13][targetID][0xFF] [Voltage(0)][Voltage(1)][Voltage(2)][Voltage(3)]
MSG_TYPE_DEBUG_SET_CURRENT    = 0x0D14,                 ///< [0x0D14][targetID][0xFF] [Current(0)][Current(1)][Current(2)][Current(3)]
MSG_TYPE_DEBUG_SET_ID         = 0x0D15,                 ///< [0x0D15][targetID][0xFF] [ID] [NUMOFNODES]
MSG_TYPE_DEBUG_FORCE_SEND     = 0x0D16,                 ///< [0x0D16][targetID][receiverID] 

MSG_TYPE_UNDEFINED 	      = 0x1FFF                  ///< [0x1FFF][][]       Not defined
};

#endif