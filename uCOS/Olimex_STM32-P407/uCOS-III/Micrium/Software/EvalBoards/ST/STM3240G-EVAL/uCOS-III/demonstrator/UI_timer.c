/**
 *********************************************************************************************************
 *
 * @file    UI_timer.c
 * @author  Arne Meeuw
 * @version V0.0.1
 * @date    2-January-2015
 * @brief   Hardware timer management used for sampling of current, 
 *          voltage and iteration of Extended Kalman Filter
 * 
 *********************************************************************************************************
 */ 

#include <UI_timer.h>
   

#define CPU_FREQ          168000000      ///< CPU frequency in Hz
#define prescaler         1680           ///< Prescaler for timer initialization
Int16U iSamplingFreq    = 6000;          //< Sampling frequency of current sampler
Int16U uSamplingFreq    = 600;           //< Sampling frequency of voltage sampler
Int16U kfSamplingFreq   = 10;           //< Update frequency of kalman filter

extern nodeInfoType self;               ///< Cell information
extern packInfoType packInfo;           ///< Battery pack information

#define VHISTITEMS 132
extern float voltageHistory[];
extern Int16U voltageTimeScaler;
extern Int16S voltageHistoryOffset;

#define SHISTITEMS 132
extern float socHistory[];
extern Int16U socTimeScaler;
extern Int16S socHistoryOffset;

#define CHISTITEMS 132
extern float currentHistory[];
extern Int16U currentTimeScaler;
extern Int16S currentHistoryOffset;

currentType currentSumme;

/**
 * @brief TIMER5 initialization sequence 
 * 
 * Timer used for high frequency current sampling.
 * 
 */
void initITimer(void)
{  
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);
  /// Set Timer configuration
  TIM_TimeBaseInitTypeDef TIM_USER_CFG;
  TIM_USER_CFG.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_USER_CFG.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_USER_CFG.TIM_Period = (CPU_FREQ / prescaler / iSamplingFreq)-1;
  TIM_USER_CFG.TIM_Prescaler = prescaler;
  TIM_USER_CFG.TIM_RepetitionCounter = 0;  
  TIM_TimeBaseInit(TIM5, &TIM_USER_CFG);
  
  /// Set BoardSupportPack functions remapping the interrupt vector
  BSP_IntVectSet(BSP_INT_ID_TIM5, &TIMER5_IRQHandler);
  BSP_IntEn(BSP_INT_ID_TIM5);
  
  TIM_ITConfig(TIM5, TIM_IT_Update, ENABLE);
}

/** 
 *@brief TIMER5 interrupt handler used for current sampling 
 *
 */
void TIMER5_IRQHandler(){
  OS_ERR err;
  static Int16U ScalerCounter;
  static currentType currentSum;
  currentType nowCurrent;
  
  /// Check if interrupt is set
  if( (TIM5->SR & TIM_IT_Update)==1)
  {
    /// Disable timer update and clear interrupt flag
    TIM_ITConfig(TIM5, TIM_IT_Update, DISABLE);
    TIM_ClearFlag(TIM5, TIM_FLAG_Update);
    TIM_ClearITPendingBit(TIM5, TIM_IT_Update);
    
    
    //self.current = (sin((float)OSTimeGet(&err)/1000) + 1);
    
    nowCurrent = read_current();
    currentSum -= currentSum/self.timerSettings.iFilter;
    currentSum += nowCurrent;
    
    self.current = currentSum/self.timerSettings.iFilter;
    
    currentSumme += self.current / iSamplingFreq;
    
    if (ScalerCounter % currentTimeScaler == 0)
    {
      currentHistory[currentHistoryOffset]=(float)self.current;
      currentHistoryOffset--;
      ScalerCounter = 1;
      if (currentHistoryOffset<0) currentHistoryOffset=CHISTITEMS-1;
    }
    else
      ScalerCounter++;
    
    /// Enable timer update
    TIM_ITConfig(TIM5, TIM_IT_Update, ENABLE);                      
  }
}

/** 
 * @brief TIMER5 frequency set function
 *
 */
void setIFrequency(Int32U frequency)
{
  disableITimer();
  iSamplingFreq = frequency;
  initITimer();
  enableITimer();
}

/** 
 * @brief TIMER5 enable function 
 *
 */
void enableITimer(void)
{
  TIM_Cmd(TIM5, ENABLE);
}

/** 
 * @brief TIMER5 disable function 
 * 
 */
void disableITimer(void)
{
  TIM_Cmd(TIM5, DISABLE);
}


/**
 * @brief TIMER6 initialization sequence 
 * 
 * Timer used for high frequency voltage sampling.
 *
 */
void initUTimer(void)
{  
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);
  /// Set Timer configuration
  TIM_TimeBaseInitTypeDef TIM_USER_CFG;
  TIM_USER_CFG.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_USER_CFG.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_USER_CFG.TIM_Period = (CPU_FREQ / prescaler / uSamplingFreq)-1;
  TIM_USER_CFG.TIM_Prescaler = prescaler;
  TIM_USER_CFG.TIM_RepetitionCounter = 0;  
  TIM_TimeBaseInit(TIM6, &TIM_USER_CFG);
  
  /// Set BoardSupportPack functions remapping the interrupt vector
  BSP_IntVectSet(BSP_INT_ID_TIM6_DAC, &TIMER6_IRQHandler);
  BSP_IntEn(BSP_INT_ID_TIM6_DAC);
  
  TIM_ITConfig(TIM6, TIM_IT_Update, ENABLE);
}

/**
 * @brief TIMER6 interrupt handler used for voltage sampling 
 * 
 */
void TIMER6_IRQHandler(){
  static Int16U ScalerCounter;
  static voltageType voltageSum;
  voltageType currentVoltage;
  
  /// Check if interrupt is set
  if( (TIM6->SR & TIM_IT_Update)==1)
  {
    /// Disable timer update and clear interrupt flag
    TIM_ITConfig(TIM6, TIM_IT_Update, DISABLE);
    TIM_ClearFlag(TIM6, TIM_FLAG_Update);
    TIM_ClearITPendingBit(TIM6, TIM_IT_Update);

    
//    packInfo.voltage[self.id] = fir(SPI_read_voltage());
    
    currentVoltage = SPI_read_voltage();
    voltageSum -= voltageSum/self.timerSettings.uFilter;
    voltageSum += currentVoltage;
    
    self.voltage = voltageSum/self.timerSettings.uFilter;
   
    if (ScalerCounter % voltageTimeScaler  == 0)
    {
      voltageHistory[voltageHistoryOffset]=(float)self.voltage;
      voltageHistoryOffset--;
      ScalerCounter = 1;
      if (voltageHistoryOffset<0) voltageHistoryOffset=VHISTITEMS-1;
    }
    else
      ScalerCounter++;
    
    /// Enable timer update
    TIM_ITConfig(TIM6, TIM_IT_Update, ENABLE);                      
  }
}

/** 
 * @brief TIMER6 frequency set function
 *
 */
void setUFrequency(Int32U frequency)
{
  disableUTimer();
  uSamplingFreq = frequency;
  initUTimer();
  enableUTimer();
}

/** 
 * @brief TIMER6 enable function 
 *
 */
void enableUTimer(void)
{
  TIM_Cmd(TIM6, ENABLE);
}

/** 
 * @brief TIMER6 disable function 
 *
 */
void disableUTimer(void)
{
  TIM_Cmd(TIM6, DISABLE);
}



/** 
 * @brief TIMER7 initialization sequence 
 *
 * Timer used for high frequency Kalman Filter updatng.
 * 
 */
void initKFTimer(void)
{  
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);
  /// Set Timer configuration
  TIM_TimeBaseInitTypeDef TIM_USER_CFG;
  TIM_USER_CFG.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_USER_CFG.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_USER_CFG.TIM_Period = (CPU_FREQ / prescaler / kfSamplingFreq)-1;
  TIM_USER_CFG.TIM_Prescaler = prescaler;
  TIM_USER_CFG.TIM_RepetitionCounter = 0;
  TIM_TimeBaseInit(TIM7, &TIM_USER_CFG);
  
  /// Set BoardSupportPack functions remapping the interrupt vector
  BSP_IntVectSet(BSP_INT_ID_TIM7, &TIMER7_IRQHandler);
  BSP_IntEn(BSP_INT_ID_TIM7);
  
  TIM_ITConfig(TIM7, TIM_IT_Update, ENABLE);
}


extern matrixType yk;   ///< Measured output vector
extern matrixType uk;   ///< Measured input vector
extern kalmanType *KFM; ///< Pointer to dataset describing Extended Kalman Filter

/** 
 * @brief TIMER7 interrupt handler used for Extended Kalman Filter iteration
 * 
 */
void TIMER7_IRQHandler(){
  static Int16U ScalerCounter;
  
  /// Check if interrupt is set
  if( (TIM7->SR & TIM_IT_Update)==1)
  {
    /// Disable timer update and clear interrupt flag
    TIM_ITConfig(TIM7, TIM_IT_Update, DISABLE);
    TIM_ClearFlag(TIM7, TIM_FLAG_Update);
    TIM_ClearITPendingBit(TIM7, TIM_IT_Update);

    yk.pData[0] = self.voltage;
    //uk.pData[0] = self.current;
    uk.pData[0] = currentSumme/kfSamplingFreq;
    currentSumme = 0;
    
    iterate_KF(KFM, &yk, &uk);
    packInfo.soc[self.id] = KFM->x.pData[0];
    
    get_yk(&KFM->x, &uk, &yk);
    self.voltage_est = yk.pData[0];
    

    if (ScalerCounter % socTimeScaler == 0)
    {
      socHistory[socHistoryOffset]=(float)packInfo.soc[self.id]*100;
      socHistoryOffset--;
      ScalerCounter = 1;
      if (socHistoryOffset<0) socHistoryOffset=SHISTITEMS-1;
    }
    else
      ScalerCounter++;
    
    /// Enable timer update
    TIM_ITConfig(TIM7, TIM_IT_Update, ENABLE);                      
  }
}

/** 
 * @brief TIMER7 frequency set function
 *
 */
void setKFFrequency(Int32U frequency)
{
  disableKFTimer();
  kfSamplingFreq = frequency;
  initKFTimer();
  enableKFTimer();
}

/** 
 * @brief TIMER7 enable function 
 *
 */
void enableKFTimer(void)
{
  TIM_Cmd(TIM7, ENABLE);
}

/** 
 * @brief TIMER7 disable function 
 *
 */
void disableKFTimer(void)
{
  TIM_Cmd(TIM7, DISABLE);
}