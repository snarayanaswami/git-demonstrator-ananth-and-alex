
Status: uCOS-III is working under IAR. LED is modifed by Cui Jin, and LED is working. However, USART3 (RS232_2) is not working for output of serial port console. 

1. Just Open the project file "\Micrium\Software\EvalBoards\ST\STM3240G-EVAL\uCOS-III\IAR\uCOS-III.eww" in IAR.
2. Go to project "Option" -> "Debugging", make sure you select "J-Link" to download the image
3. press F7 to make
4. then you can download to see LED is blinking. The default app is included into main() function in app.c
   You can enable serial port by enable the macro inside of app.cfg

Will let you know if the serial port is working after debugging.