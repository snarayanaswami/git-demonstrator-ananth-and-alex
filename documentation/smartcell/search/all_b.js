var searchData=
[
  ['n',['n',['../structkalman_type.html#a2023c998f18e8a2c35dcfdfb418daf97',1,'kalmanType']]],
  ['nodeinfotype',['nodeInfoType',['../structnode_info_type.html',1,'']]],
  ['none',['NONE',['../typedef_8h.html#a35822c953540e9ad193309b6d4f65011ac157bdf0b85a40d2619cbc8bc1ae5fe2',1,'typedef.h']]],
  ['numcols',['numCols',['../structmatrix_type.html#ad567568bcb60d4ca70d7dda7824c6523',1,'matrixType']]],
  ['numofnodes',['NUMOFNODES',['../app_8c.html#a641db1dcff1cda71fff093124abe45c5',1,'NUMOFNODES():&#160;app.c'],['../balancing__strategy_8c.html#a641db1dcff1cda71fff093124abe45c5',1,'NUMOFNODES():&#160;app.c'],['../control_8c.html#a641db1dcff1cda71fff093124abe45c5',1,'NUMOFNODES():&#160;app.c'],['../screen__control_8c.html#a641db1dcff1cda71fff093124abe45c5',1,'NUMOFNODES():&#160;app.c']]],
  ['numrows',['numRows',['../structmatrix_type.html#a849896707929d26c20778956c7740e10',1,'matrixType']]]
];
