var searchData=
[
  ['matrix_5fadd',['matrix_add',['../matrix__operations_8c.html#adbabae1fc388ade28312f118444118fa',1,'matrix_operations.c']]],
  ['matrix_5fidentity',['matrix_identity',['../matrix__operations_8c.html#ad892b837c4ca4fc03d1ea73ec459206f',1,'matrix_operations.c']]],
  ['matrix_5finit',['matrix_init',['../matrix__operations_8c.html#a9108d4755beb80496b8fef067e30c332',1,'matrix_operations.c']]],
  ['matrix_5finverse',['matrix_inverse',['../matrix__operations_8c.html#aaa63b195fa568db42ac6f8b8c831d47e',1,'matrix_operations.c']]],
  ['matrix_5fmulti',['matrix_multi',['../matrix__operations_8c.html#a6b28e801d548374615eaefdc2409a164',1,'matrix_operations.c']]],
  ['matrix_5fscale',['matrix_scale',['../matrix__operations_8c.html#a2478b27835c9ccdfe6267a5978cd7d87',1,'matrix_operations.c']]],
  ['matrix_5fsubtr',['matrix_subtr',['../matrix__operations_8c.html#ab68f6cd24216db6c007f0eefd75c7fe2',1,'matrix_operations.c']]],
  ['matrix_5ftranspose',['matrix_transpose',['../matrix__operations_8c.html#a920e5d667035bfd35ea434c7e02d9ecc',1,'matrix_operations.c']]],
  ['max',['max',['../array__functions_8c.html#aed903f56c7d3d661e8d3168ecf98c132',1,'array_functions.c']]],
  ['min',['min',['../array__functions_8c.html#a1fbc45c00f2409f4fd317d62414aab19',1,'array_functions.c']]],
  ['morechargeonleft',['moreChargeOnLeft',['../array__functions_8c.html#a9a7746b436cdce03d8eec88803b70e94',1,'array_functions.c']]],
  ['morevoltageonleft',['moreVoltageOnLeft',['../array__functions_8c.html#ae1aa24e639101b28032289580942a4ed',1,'array_functions.c']]]
];
