var searchData=
[
  ['r',['R',['../soc__kalman_8c.html#a4c4b08e61e92b3da1ed512f4e006f34a',1,'R():&#160;soc_model.c'],['../soc__model_8c.html#a4c4b08e61e92b3da1ed512f4e006f34a',1,'R():&#160;soc_model.c']]],
  ['random_5fint',['random_int',['../rng_8c.html#a558d20bc99c0f4993914c8942772ab50',1,'rng.c']]],
  ['receive_5fdown',['RECEIVE_DOWN',['../typedef_8h.html#a9ebcb624bc7a430ea86515bde42b0604ac83beb11afdbee537b6b8f2fe9a17072',1,'typedef.h']]],
  ['receive_5fup',['RECEIVE_UP',['../typedef_8h.html#a9ebcb624bc7a430ea86515bde42b0604afa034b68b44a280d651a9e8c8cdf8c6f',1,'typedef.h']]],
  ['receiveactive',['receiveActive',['../structnode_info_type.html#aa271dcdbbdd9e5c35a5feffd0c902b23',1,'nodeInfoType']]],
  ['receivecharge',['ReceiveCharge',['../charge__rx__tx_8c.html#a0da27abd89b83c772ddbe8d009f46518',1,'charge_rx_tx.c']]],
  ['receiverid',['receiverID',['../structtransaction_info_type.html#a49345cc885a9a286fd21783fafa761dd',1,'transactionInfoType']]],
  ['removeblocker',['removeBlocker',['../array__functions_8c.html#abcf74dde612e97033ae44e87b54805bb',1,'array_functions.c']]],
  ['requestchargecondition',['requestChargeCondition',['../balancing__strategy_8c.html#a9ca7a000fc75ae81c9532126fdab4a44',1,'balancing_strategy.c']]],
  ['requeststrategy',['RequestStrategy',['../app_8c.html#aa7ca066ef02b1264ac8ded69dddb481a',1,'app.c']]],
  ['requesttask',['RequestTask',['../app_8c.html#ad1a685a27520f83598dc9998fed322e5',1,'app.c']]],
  ['requesttaskstack',['RequestTaskStack',['../app_8c.html#a7e66eb97f9755036cd2a01061d9cf584',1,'app.c']]],
  ['rng_2ec',['rng.c',['../rng_8c.html',1,'']]],
  ['rng_5finit',['RNG_Init',['../rng_8c.html#ab774557a4dbbd02ae4ce1b218a3967a4',1,'rng.c']]]
];
