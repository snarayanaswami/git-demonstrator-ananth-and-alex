var searchData=
[
  ['vhistitems',['VHISTITEMS',['../app_8c.html#a64f4d3cd6b265a40569226122343fe1f',1,'app.c']]],
  ['voltage',['voltage',['../structpack_info_type.html#ab92972ddd10f15a9af069e2fc2f11cba',1,'packInfoType']]],
  ['voltage_5fest',['voltage_est',['../structnode_info_type.html#a387297a898bb743b3bf8ab77522f056c',1,'nodeInfoType']]],
  ['voltageaveragetaskstk',['VoltageAverageTaskStk',['../app_8c.html#ad4c3931decdf1c584a4fd0600633a501',1,'app.c']]],
  ['voltageavgtask',['VoltageAvgTask',['../app_8c.html#a21caa4fe6f39ea77f4e0f118e543d866',1,'app.c']]],
  ['voltagehistory',['voltageHistory',['../app_8c.html#a96df979e2bb2eeb3cdb6ec0749d4fe81',1,'voltageHistory():&#160;app.c'],['../screen__control_8c.html#a51334c6102333538a0506410c2671fdb',1,'voltageHistory():&#160;app.c'],['../_u_i__timer_8c.html#a51334c6102333538a0506410c2671fdb',1,'voltageHistory():&#160;app.c']]],
  ['voltagehistoryoffset',['voltageHistoryOffset',['../app_8c.html#afce5f7b9a18d9bcc1267925025b1866f',1,'voltageHistoryOffset():&#160;app.c'],['../screen__control_8c.html#afce5f7b9a18d9bcc1267925025b1866f',1,'voltageHistoryOffset():&#160;app.c'],['../_u_i__timer_8c.html#afce5f7b9a18d9bcc1267925025b1866f',1,'voltageHistoryOffset():&#160;app.c']]],
  ['voltagetype',['voltageType',['../typedef_8h.html#a64c2a84d0d8d4da894a6a3dd1f0e6e77',1,'typedef.h']]]
];
